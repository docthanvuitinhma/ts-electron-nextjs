/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    /** @see https://nextjs.org/docs/advanced-features/output-file-tracing#automatically-copying-traced-files-experimental */
    output: 'standalone',
    distDir: 'build',
    experimental: {
        images: {
            allowFutureImage: true
        }
    },
    amp: {
        canonicalBase: 'src'
    },
    eslint: {
        "react/display-name": "off"
    }
}

module.exports = nextConfig
