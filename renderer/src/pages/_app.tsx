import '../styles/globals.scss'
import type {AppProps} from 'next/app'
import Head from "next/head";
import {RecoilRoot} from "recoil";
import {ConfigContextProvider} from "../client/presentation/contexts/ConfigContext";
import {Fragment, ReactNode, useCallback, useEffect, useState} from "react";
import {Provider as InversifyProvider} from "inversify-react";
import {container} from "../client/config/InversifyConfig";
import {BrowserRouter} from "react-router-dom";
import {App} from "../client/const/App";
import {TNextAppData} from "../client/const/Types";
import {EDData} from "../client/core/encrypt/EDData";
import {SessionContextProvider} from "../client/presentation/contexts/SessionContext";
import {InitTracking} from "../client/presentation/contexts/InitTracking";
import "plyr-react/plyr.css";

const MyApp = ({Component, pageProps}: AppProps) => {
    const [render, setRender] = useState<ReactNode>()
    const _pageProps: any = pageProps;

    useEffect(() => {
        const data: TNextAppData = {};

        if (pageProps && pageProps.hasOwnProperty('header')) {
            data.header = EDData.decrypt((pageProps as any).header);
        }

        const isInitTracking = process.env.NEXT_PUBLIC_INIT_TRACKING && (process.env.NEXT_PUBLIC_INIT_TRACKING == "true")

        setRender(
            <Fragment>
                <BrowserRouter>
                    <InversifyProvider container={container}>
                        <RecoilRoot>
                            <SessionContextProvider data={data}>
                                <ConfigContextProvider>
                                    {
                                        isInitTracking
                                            ? <InitTracking>
                                                <Component {...pageProps} />
                                            </InitTracking>
                                            : <Component {...pageProps} />
                                    }
                                </ConfigContextProvider>
                            </SessionContextProvider>
                        </RecoilRoot>
                    </InversifyProvider>
                </BrowserRouter>
            </Fragment>
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const head = useCallback(() => {
        return (
            <Head>
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
                <link rel="manifest" href="/site.webmanifest"/>
                <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5"/>
                <meta name="msapplication-TileColor" content="#da532c"/>
                <meta name="theme-color" content="#ffffff"/>
                <title>{_pageProps.title ?? App.appTitle}</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1"/>
            </Head>
        )
    }, [_pageProps.title])

    return (
        <>
            {
                head()
            }
            {
                render ?? null
            }
        </>
    )
}

export default MyApp
