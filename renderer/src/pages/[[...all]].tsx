import {GetServerSideProps, NextPage} from "next";
import App from "../client/presentation/App";

type _TParams = {
    id: string,
}

const AllPage: NextPage = (props: any) => {
    return <App {...props}/>
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const {id} = context.params as _TParams;

    console.log('id', id)

    return {
        props: {
            id: context.params
        }
    }
}

export default AllPage;
