import React, {useEffect, useState} from 'react'
import {CCard, CCardBody, CCardGroup, CCol, CContainer, CRow} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {cilLockLocked, cilUser} from '@duonghainbs/coreui-icons'
import {Button, Form, Input, Modal, Typography} from "antd";
import {useTranslation} from "react-i18next";
import {useSessionContext} from "../../contexts/SessionContext";
import {SendingStatus} from "../../../const/Events";
import {LoginAction} from "../../../recoil/auth/login/LoginAction";
import {Color} from "../../../const/Color";
import {useNavigate} from "react-router";
import {Navigate} from "react-router-dom";
import {RouteConfig} from "../../../config/RouteConfig";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {LoginOutlined} from "@ant-design/icons";
import {MachineAction} from "../../../recoil/machine/MachineAction";
import {RoleAction} from "../../../recoil/role/RoleAction";
import {PermissionAction} from "../../../recoil/permission/PermissionAction";
import {UserAction} from "../../../recoil/user/UserAction";
import {noti} from "../../functions/Notification";
import ErrorItemFC from '../../widgets/ErrorItemFC';

type _TFormFinish = {
    email: string
    password: string
}

let timerEmail: NodeJS.Timeout
let timerPassword: NodeJS.Timeout

const Login = () => {
    const navigate = useNavigate()
    const {t} = useTranslation()
    const [session] = useSessionContext()
    const {
        vm,
        onLogIn,
        onClearState
    } = LoginAction()
    const {onClearMachineState} = MachineAction()
    const {onClearRoleState} = RoleAction()
    const {onClearPermissionState} = PermissionAction()
    const {onClearUserState} = UserAction()

    const [emailError, setEmailError] = useState('')
    const [passwordError, setPasswordError] = useState('')

    useEffect(() => {
        console.log('%cMount Screen: LoginScreen', Color.ConsoleInfo)
        onClearMachineState()
        onClearUserState()
        onClearRoleState()
        onClearPermissionState()
        return () => {
            onClearState()

            console.log('%cUnmount Screen: LoginScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (vm.status === SendingStatus.success) {
            noti('success', 'Đăng nhập thành công')
            navigate(session.redirectPath, {
                replace: true
            })
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.status])

    const onFinish = (values: _TFormFinish) => {
        onLogIn({
            email: values.email,
            password: values.password
        })
    }
    const isLoading = vm.status === SendingStatus.loading
    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach((e) => {
            if (e.name[0] === "email") {
                console.log('email')
                // setClientUsernameError(e.errors[0] ? e.errors[0] : '')
            }

            if (e.name[0] === "password") {
                console.log('pass')
                // setClientPasswordError(e.errors[0] ? e.errors[0] : '')
            }
        })
    }

    const debounceEmail = (func: Function, timeOut = 900) => {

        return (...arg: string[]) => {

            clearTimeout(timerEmail)

            timerEmail = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debouncePassword = (func: Function, timeOut = 900) => {

        return (...arg: string[]) => {

            clearTimeout(timerPassword)

            timerPassword = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach((e: any) => {
            if (e.name[0] === "email") {
                console.log('email')
                const process = debounceEmail(() => {
                    if (e.errors[0]) {
                        setEmailError(e.errors[0])
                    }
                })

                process()
            } else if (e.name[0] === "password") {
                console.log('password')
                const process = debouncePassword(() => {
                    if (e.errors[0]) {
                        setPasswordError(e.errors[0])
                    }
                })

                process()
            }
        })
    }

    useEffect(() => {
        if (vm.error && vm.status === SendingStatus.error) {
            if (vm.error.hasOwnProperty('email')) {
                setEmailError(vm.error.email)
            }

            if (vm.error.hasOwnProperty('password')) {
                setPasswordError(vm.error.password)
            }
        }
    }, [vm.error, vm.status])

    // redirect if logged
    if (session.isAuthenticated) {
        return <Navigate to={RouteConfig.DASHBOARD}/>
    }

    const resetError = (state: string, setState: Function) => {
        if (state) {
            setState('')
        }
    }

    return (
        <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol md={8}>
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <Typography.Title
                                        level={2}
                                        className={"mb-0"}
                                    >
                                        {t("text.login")}
                                    </Typography.Title>
                                    <Typography.Text type={"secondary"}>
                                        {t('text.loginSubTitle')}
                                    </Typography.Text>
                                    <Form
                                        className={"mt-3"}
                                        name={"login"}
                                        onFinish={onFinish}
                                        onFinishFailed={onFinishFailed}
                                        onFieldsChange={onFieldsChange}
                                    >
                                        <div className={"flex flex-col gap-2"}>
                                            <Form.Item
                                                key={'email'}
                                                name={"email"}
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Vui lòng nhập email'
                                                    },
                                                    {
                                                        type: "email",
                                                        message: 'Email sai định dạng'
                                                    },
                                                    {
                                                        min: 3,
                                                        message: t("error.usernameBetween", {
                                                            min: 3,
                                                            max: 30,
                                                        })
                                                    },
                                                    {
                                                        max: 30,
                                                        message: t("error.usernameBetween", {
                                                            min: 3,
                                                            max: 30,
                                                        })
                                                    }
                                                ]}
                                                validateStatus={emailError ? 'error' : ''}
                                                help={emailError ? emailError : ''}
                                            >
                                                <Input
                                                    placeholder={t("text.username")}
                                                    addonBefore={<CIcon icon={cilUser}/>}
                                                    onKeyUp={() => resetError(emailError, setEmailError)}
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                className={"form-item-main"}
                                                key={'password'}
                                                name={"password"}
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: t("error.passwordRequired")
                                                    },
                                                    {
                                                        min: 6,
                                                        message: t("error.passwordBetween", {
                                                            min: 6,
                                                            max: 30,
                                                        })
                                                    },
                                                    {
                                                        max: 30,
                                                        message: t("error.passwordBetween", {
                                                            min: 6,
                                                            max: 30,
                                                        })
                                                    }
                                                ]}
                                                validateStatus={passwordError ? 'error' : ''}
                                                help={passwordError ? passwordError : ''}
                                            >
                                                <Input.Password
                                                    style={{
                                                        textAlign: "start"
                                                    }}
                                                    placeholder={t('text.password')}
                                                    addonBefore={<CIcon icon={cilLockLocked}/>}
                                                    onKeyUp={() => resetError(passwordError, setPasswordError)}
                                                    // iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                                                />
                                            </Form.Item>
                                        </div>
                                        <CRow className={"mt-2"}>
                                            <CCol xs={6}>
                                                <Button
                                                    htmlType={"submit"}
                                                    type={"primary"}
                                                    size={"large"}
                                                    loading={isLoading}
                                                    icon={<LoginOutlined/>}
                                                >
                                                    {t('text.login')}
                                                </Button>
                                            </CCol>
                                            <CCol xs={6} className="text-right">
                                                <Button
                                                    type="link"
                                                    onClick={() => {
                                                        Modal.info({
                                                            title: t('text.notify'),
                                                            content: "Vui lòng liên hệ với quản trị viên",
                                                            okText: t('button.close'),
                                                            okType: "default"
                                                        });
                                                    }}
                                                >
                                                    {t('text.forgotPassword')}
                                                </Button>
                                            </CCol>
                                        </CRow>
                                    </Form>
                                    <ErrorItemFC
                                        status={vm.status}
                                        typeView={'modal'}
                                    >
                                    </ErrorItemFC>
                                </CCardBody>
                            </CCard>
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
        </div>
    )
}

export default Login
