import React, {useEffect, useMemo, useState} from "react";
import {useNavigate} from "react-router";
import {Button, Card, Checkbox, Form, Input} from "antd";
import {TUserV0} from "../../../models/UserModel";
import {Validation} from "../../../core/Validation";
import {Color} from "../../../const/Color";
import {AddUserAction} from "../../../recoil/user/AddUserAction";
import {SendingStatus} from "../../../const/Events";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {noti} from "../../functions/Notification";

let timerName: NodeJS.Timeout
let timerEmail: NodeJS.Timeout
let timerPass: NodeJS.Timeout

const AddUser = () => {
    const navigator = useNavigate()
    const [nameError, setNameError] = useState('')
    const [emailError, setEmailError] = useState('')
    const [passwordError, setPasswordError] = useState('')
    const {
        vm,
        onGetRoles,
        onAddUser,
        onClearUserState
    } = AddUserAction()
    const [form] = Form.useForm();
    useEffect(() => {

        console.log('%cMount Screen: Add User', Color.ConsoleInfo)
        onGetRoles()

        return () => {
            onClearUserState()
            console.log('%cUnmount Screen: Add User', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (vm.addStatus === SendingStatus.success) {
            noti("success", 'Thêm mới người dùng thành công!')
            navigator('/user')
        } else console.log('failed')
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.addStatus])

    const debounceName = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerName)

            timerName = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceEmail = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerEmail)

            timerEmail = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debouncePass = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerPass)

            timerPass = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const onFinish = (data: TUserV0) => {
        const postData = {
            name: data.name,
            email: data.email,
            password: data.password,
            role_id: data.role
        }

        onAddUser(postData)
    }

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach(e => {
            switch (e.name[0]) {
                case 'name':
                    setNameError(e.errors[0])
                    break
                case 'email':
                    setEmailError(e.errors[0])
                    break
                case 'password':
                    setPasswordError(e.errors[0])
                    break
            }
        })
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach(e => {
            if (e.name === 'name') {
                const process = debounceName(() => {
                    if (e.errors) {
                        setNameError(e.errors.toString)
                    }
                })

                process()
            } else if (e.name === 'email') {
                const process = debounceEmail(() => {
                    if (e.errors) {
                        setEmailError(e.errors.toString)
                    }
                })

                process()
            } else if (e.name === 'password') {
                const process = debouncePass(() => {
                    if (e.errors) {
                        setPasswordError(e.errors[0])
                    }
                })

                process()
            }
        })
    }

    useEffect(() => {
        if (vm.addStatus === SendingStatus.error && vm.error) {
            if (vm.error.hasOwnProperty('name')) {
                setNameError(vm.error.name)
            }
            if (vm.error.hasOwnProperty('email')) {
                setEmailError(vm.error.email)
            }
            if (vm.error.hasOwnProperty('password')) {
                setPasswordError(vm.error.password)
            }
        }
    }, [vm.addStatus, vm.error])

    const options = useMemo(() => (
        vm.res?.map(item => (
            {
                label: item.displayName,
                value: item.id ? item.id : 0
            }
        ))

    ), [vm.res])

    return (
        <Card>
            <Form
                labelCol={{span: 8}}
                wrapperCol={{span: 14}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                onFieldsChange={onFieldsChange}

            >
                <Form.Item
                    label={'Họ và tên'}
                    name={'name'}
                    rules={[
                        {required: true},
                        {min: 3},
                        {max: 100}
                    ]}
                    validateStatus={nameError ? 'error' : ''}
                    help={nameError ? nameError : ''}
                >
                    <Input
                        placeholder={'Họ và tên'}
                        onKeyUp={() => setNameError('')}
                    />
                </Form.Item>

                <Form.Item
                    label={'Email'}
                    name={'email'}
                    rules={[
                        {required: true},
                        {type: "email"},
                        {pattern: Validation.FORMAT_EMAIL}
                    ]}
                    validateStatus={emailError ? 'error' : ''}
                    help={emailError ? emailError : ''}
                >
                    <Input
                        placeholder={'Email'}
                        onKeyUp={() => setEmailError('')}
                    />
                </Form.Item>

                <Form.Item
                    label={'Tạo mật khẩu'}
                    name={'password'}
                    rules={[
                        {required: true},
                        {pattern: Validation.FORMAT_PASSWORD},
                        {max: 100}
                    ]}
                    validateStatus={passwordError ? 'error' : ''}
                    help={passwordError ? passwordError : ''}
                >
                    <Input
                        placeholder={'Mật khẩu'}
                        onKeyUp={() => setPasswordError('')}
                    />
                </Form.Item>

                <Form.Item
                    label={'Vai trò'}
                    name={'role'}
                    rules={[
                        {required: true}
                    ]}
                >
                    <Checkbox.Group
                        options={
                            options
                                ? options
                                : []
                        }
                    />
                </Form.Item>
                <Form.Item className="flex justify-center">
                    <Button
                        htmlType={"submit"}
                        type={"primary"}
                        disabled={
                            !form.isFieldsTouched(true) ||
                            !!form.getFieldsError().filter(({ errors }) => errors.length).length
                        }
                    >Lưu</Button>
                </Form.Item>
            </Form>

        </Card>
    )
}

export default AddUser
