import {Avatar, Button, Card, Dropdown, Menu, Modal, Tag, Tooltip, Typography} from "antd";
import {ExclamationCircleOutlined, MoreOutlined, ClockCircleOutlined, MailOutlined, UserOutlined} from "@ant-design/icons";
import {Utils} from "../../../core/Utils";
import {App} from "../../../const/App";
import {useNavigate} from "react-router";
import {useSetRecoilState} from "recoil";
import {InfoState} from "../../../recoil/user/UpdateUserState";
import {AiOutlineEdit} from "react-icons/ai";
import {MdOutlineDisabledVisible} from "react-icons/md";

export const UserCard = ({...props}) => {
    const navigate = useNavigate()
    const SetRecoil = useSetRecoilState(InfoState)
    const onHandelEdit = () => {
        const obj1 = {name: props.item?.name, createdAt: props.item?.createdAt, email: props.item?.email, role: props.item?.role, id: +(props.item.userId)}
        SetRecoil(obj1)
        navigate(`/user/edit/${props?.item?.userId}`)
    }
    const onClick = (key: string) => {
        switch (key) {
            case 'delete':
                console.log('delete')
                Modal.confirm({
                    title: 'Vô hiệu hóa tài khoản?',
                    icon: <ExclamationCircleOutlined/>,
                    content: 'Bạn muốn vô hiệu hóa tài khoản?',
                    onOk() {
                        return new Promise((resolve, reject) => {
                            setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                        }).catch(() => console.log('Oops errors!'));
                    },
                    onCancel() {
                    },
                })
                break
            case 'edit':
                const obj1 = {name: props.item?.name, createdAt: props.item?.createdAt, email: props.item?.email, role: props.item?.role, id: +(props.item.userId)}
                SetRecoil(obj1)
                navigate(`/user/edit/${props?.item?.userId}`)
                break
        }
    }

    return (
        <Card>
            <div className={'flex justify-between'}>
                <Card.Meta
                    avatar={<Avatar src="https://joeschmoe.io/api/v1/random"/>}
                    title={
                        <div className={'flex items-center gap-2 text-xl'}>
                            <Tooltip title={'Tên người dùng'}>
                                <Typography.Text>{props.item.name}</Typography.Text>
                            </Tooltip>
                        </div>
                    }
                    description={
                        <div className={'flex items-center gap-2'}>
                            <Tooltip title={'Ngày tạo'}>
                                <Typography.Text className={'text-gray-400 flex justify-center items-center'}>
                                    <ClockCircleOutlined className="mr-2"/> {Utils.createdAtFormatted(props.item.createdAt, App.FormatToMoment)}
                                </Typography.Text>
                            </Tooltip>
                        </div>
                    }
                />

                <Dropdown
                    overlay={
                        <Menu
                            items={[
                                {
                                    key: 'edit',
                                    label: 'Chỉnh sửa',
                                },
                                {
                                    key: 'delete',
                                    label: <Typography className={'text-red-500'}>Xóa</Typography>,
                                },
                            ]}
                            onClick={e => onClick(e.key)}
                        />
                    }
                >
                    <MoreOutlined/>
                </Dropdown>
            </div>
            <hr/>
            <div className="flex justify-between items-center text-[15px]">
                <div className="font-bold text-[15px] flex justify-center items-center">
                    <MailOutlined className="mr-2"/> Email :
                </div>
                <div className="text-[15px] hover:cursor-not-allowed italic">
                    {props.item.email}
                </div>
            </div>
            <div className="flex justify-between text-center my-4">
                {props.item.role?.length === 0
                    ? ''
                    : <>
                        <div className=" font-bold text-[15px] flex justify-center items-center">
                            <UserOutlined className="mr-2"/>
                            Vai trò :
                        </div>
                        <div>
                            {
                                props.item.role?.map((item: any) => {
                                    let color = ""
                                    if (item.name === "admin") {
                                        color = "green"
                                    }
                                    if (item.name === "content") {
                                        color = "blue"
                                    }
                                    if (item.name === "test") {
                                        color = "gray"
                                    }

                                    return (
                                        <Tag key={item.id} color={color}>{item.name}</Tag>
                                    )
                                })

                            }
                        </div>
                    </>
                }
            </div>
            <hr/>
            <div className="flex justify-between">
                <Button className="bg-blue-800" onClick={() => onHandelEdit()}><AiOutlineEdit className="text-white"/></Button>
                <Button danger={true}><MdOutlineDisabledVisible/></Button>
            </div>
        </Card>
    )
}