import {ColumnsType} from "antd/es/table";
import {Button, Empty, Modal, Pagination, Space, Spin, Table, Tag, Tooltip, Typography} from "antd";
import {Utils} from "../../../core/Utils";
import {App} from "../../../const/App";
import {Key, TableRowSelection} from "antd/es/table/interface";
import React, {useEffect, useLayoutEffect, useState} from "react";
import {Color} from "../../../const/Color";
import {useLocation, useNavigate} from "react-router";
import {Query} from "../../functions/Query";
import {UserAction} from "../../../recoil/user/UserAction";
import {MasterLayoutCtxWrapper} from "../../layouts/MasterLayout";
import {RouteConfig} from "../../../config/RouteConfig";
import {EditOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import {SendingStatus} from "../../../const/Events";
import {UserCard} from "./UserCard";
import {useTranslation} from "react-i18next";
import {useSetRecoilState} from "recoil";
import {InfoState} from "../../../recoil/user/UpdateUserState";
import {AddUserAction} from "../../../recoil/user/AddUserAction";
import {UpdateUserAction} from "../../../recoil/user/UpdateUserAction";
import {FaUserPlus} from "react-icons/fa";
import {MdOutlineDisabledVisible} from "react-icons/md";


interface DataType {
    key: string
    name: string
    email: string
    role: any
    createdAt: string
    action: {
        user_id: string
    }
}

const UserScreen = () => {
    const [selected, setSelected] = useState<Key[]>([])
    const location = useLocation();
    const {t} = useTranslation();
    const navigator = useNavigate();
    const [loading, setLoading] = useState(false);
    const SetRecoil = useSetRecoilState(InfoState);
    const {onClearUpdateState} = UpdateUserAction();
    const {onGetRoles} = AddUserAction();
    const {
        vm,
        onGetAllUser,
        onClearUserState
    } = UserAction();
    const {queryToObject} = Query();
    const query = queryToObject();

    useEffect(() => {
        console.log('%cMount Screen: Users', Color.ConsoleInfo)
        return () => {
            onClearUserState()
            console.log('%cMount Screen: Users', Color.ConsoleInfo)
            onClearUpdateState()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useLayoutEffect(() => {
        onClearUserState()
        onClearUpdateState()
        setLoading(true)
        onGetAllUser(query)
        onGetRoles()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location]);

    useEffect(() => {
        if (vm.status === SendingStatus.success) {
            setLoading(false)
        }
    }, [vm.status]);

    const onHandelEdit = (id: any) => {
        onGetRoles()
        const obj = vm.res?.find(item => item.userId === +(id.user_id))
        const obj1 = {name: obj?.name, createdAt: obj?.createdAt, email: obj?.email, role: obj?.role, id: +(id.user_id)}
        SetRecoil(obj1)
        navigator(`/user/edit/${id.user_id}`)
    }

    const columns: ColumnsType<DataType> = [
        {
            title: `${t("machine.name")}`,
            dataIndex: 'name',
            key: 'name',
            render: value => (
                <Typography.Text strong>
                    {value}
                </Typography.Text>
            )
        },
        {
            title: `${t("user.email")}`,
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: `${t("user.role")}`,
            dataIndex: 'role',
            key: 'role',
            render: value =>
                value.length === 0
                    ? ''
                    : <div className={'flex flex-wrap gap-2'}>
                        {
                            value.map((item: any, index: any) => {

                                let color = item.name === 'admin'
                                    ? 'blue'
                                    : ""

                                if (item.name === "content") {
                                    color = 'green'
                                }
                                if (item.name === "test") {
                                    color = 'gray'
                                }
                                return <Tag key={index} color={color} className={'m-0'}>{item.name ?? "Chưa cấp quyền"}</Tag>
                            })
                        }
                    </div>

        },
        {
            title: `${t("machine.created_at")}`,
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: value => (
                <Typography.Paragraph italic>
                    {Utils.createdAtFormatted(value, App.FormatToMoment)}
                </Typography.Paragraph>
            )
        },
        {

            title: `${t("machine.action")}`,
            dataIndex: 'action',
            key: 'action',
            render: (value) => (
                <div className={"flex flex-row"}>
                    <Tooltip title={`${t("machine.edit")}`}>
                        <Button
                            type={"primary"}
                            icon={<EditOutlined/>}
                            onClick={() => onHandelEdit(value)}
                        />
                    </Tooltip>
                </div>

            )

        },
    ]

    const data: DataType[] = vm.res?.map<DataType>(item => (

        {
            key: item.userId ? item.userId.toString() : '',
            name: item.name ?? "",
            email: item.email,
            role: item.role || [{id: 7, name: "Chưa cấp quyền"}],
            createdAt: item.createdAt ?? "",
            action: {
                user_id: item.userId ? item.userId.toString() : '',
            }
        }
    )) ?? []

    const rowSelection: TableRowSelection<DataType> = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelected(selectedRowKeys)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
    }

    const handleInactive = () => {
        Modal.confirm({
            title: 'Vô hiệu hóa tài khoản?',
            icon: <ExclamationCircleOutlined/>,
            content: 'Bạn muốn vô hiệu hóa tài khoản?',
            onOk() {
                return new Promise((resolve, reject) => {
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {
            },
        })
    }
    const onAdd = () => {
        navigator(RouteConfig.ADD_USER)
    }
    const tool = () => (
        <div>
                <Tooltip title={t("user.create_new")}>
                    <Button
                        type={"primary"}
                        onClick={onAdd}
                        className="text-white bg-green-600 hover:bg-green-300"
                        icon={ <FaUserPlus/>}
                    />
                </Tooltip>
            <Tooltip title={t("user.disable")} placement="topRight" >
                <Button
                    type={"primary"}
                    danger
                    onClick={handleInactive}
                    disabled={selected.length <= 0}
                    className="text-white mx-2"
                    icon={<MdOutlineDisabledVisible/>}
                />
            </Tooltip>

        </div>
    )

    return (
        <MasterLayoutCtxWrapper
            tool={tool()}
        >
            {
                vm.res?.length === 0 && vm.status === SendingStatus.success
                    ? <Empty/>
                    : <Spin
                        spinning={loading}
                        size="large"
                    >
                        <>
                            <div className={'hidden sm:block'}>
                                <Table
                                    columns={columns}
                                    dataSource={data}
                                    rowSelection={{...rowSelection}}
                                    pagination={{
                                        pageSize: 20,
                                        position: []
                                    }}
                                />
                            </div>
                            <div className={'sm:hidden'}>
                                <Space
                                    direction={"vertical"}
                                    size="middle"
                                    style={{display: 'flex'}}
                                >
                                    {
                                        vm.res?.map(item => {
                                            return <UserCard item={item} key={item.userId}/>
                                        })
                                    }
                                </Space>
                            </div>
                        </>
                    </Spin>
            }

            <div className={'flex justify-center my-3'}>
                <Pagination
                    onChange={page => {
                        setLoading(true)
                        navigator(`${location.pathname}?page=${page}`)
                    }}
                    responsive
                    pageSize={20}
                    current={vm.meta?.currentPage}
                    total={vm.meta?.totalCount}
                    hideOnSinglePage
                />
            </div>
        </MasterLayoutCtxWrapper>
    )
}

export default UserScreen
