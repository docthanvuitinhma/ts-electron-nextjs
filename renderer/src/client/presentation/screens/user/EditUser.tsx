import {Button, Form, Input, Modal} from 'antd';
import React, {useEffect, useMemo, useState} from 'react';
import {useNavigate, useParams} from "react-router";
import {UpdateUserAction} from "../../../recoil/user/UpdateUserAction";
import {useRecoilValue} from "recoil";
import {InfoState} from "../../../recoil/user/UpdateUserState";
import {SendingStatus} from "../../../const/Events";
import {noti} from "../../functions/Notification";
import {Checkbox} from 'antd';
import {useLocation} from "react-router-dom";
import {UserAction} from "../../../recoil/user/UserAction";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {useTranslation} from "react-i18next";


type SizeType = Parameters<typeof Form>[0]['size'];
let timerName: NodeJS.Timeout

const EditUser: React.FC = () => {
    const {vm, onGetRoles, onUpdateUser} = UpdateUserAction();
    const location = useLocation();
    const {onGetAllUser} = UserAction();
    useEffect(() => {
        if (vm.updateStatus === SendingStatus.success) {
            noti("success", 'Cập nhật người dùng thành công!')
            navigate('/user')
        } else
            console.log("Failed")
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.updateStatus])

    useEffect(() => {
        onGetRoles()

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    useMemo(() =>
            onGetAllUser()

        // eslint-disable-next-line react-hooks/exhaustive-deps
        , []);
    const [, setComponentSize] = useState<SizeType | 'default'>('default');
    const {id} = useParams();
    const navigate = useNavigate();
    const [form] = Form.useForm();
    const {t} = useTranslation();
    const [, setNameError] = useState('')
    const onFormLayoutChange = ({size}: { size: SizeType }) => {
        setComponentSize(size);
    };
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [modalText, setModalText] = useState('Huỷ bỏ thao tác ?');
    const [isShow, setIsShow] = useState(false);
    const handleClose = () => {
        setIsShow(false)
    }
    const handleOk = () => {
        setModalText('Vui lòng chờ trong giây lát...');
        setConfirmLoading(true);
        setTimeout(() => {
            setIsShow(false);
            setConfirmLoading(false);
            navigate('/user')
        }, 2000);
    };

    const onHandelFinish = (data: any) => {
        const updateData = {
            name: data.name,
            email: data.email,
            password: data.password,
            role_id: data.role
        }
        onUpdateUser(id, updateData)
    }

    const data = useRecoilValue(InfoState)
    const options = useMemo(() => (
        vm.res?.map(item => (
            {
                label: item?.displayName,
                value: item?.id ? item.id : 0
            }
        ))

    ), [vm.res])

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach(e => {
            switch (e.name[0]) {
                case 'name':
                    setNameError(e.errors[0])
                    break
            }
        })
    }
    const debounceName = (func: Function, timeOut = 1000) => {
        return (...arg: string[]) => {
            clearTimeout(timerName)
            timerName = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }
    const onFieldsChange = (data: FieldData[]) => {
        data.forEach((e: any) => {
            if (e.name[0] === 'name') {
                const process = debounceName(() => {
                    if (e.errors[0]) {
                        setNameError(e.errors[0] ? e.errors[0] : '')
                    }
                })
                process()
            }
        })
    }
    return (
        <Form
            labelCol={{span: 4}}
            wrapperCol={{span: 14}}
            className="mt-12"
            onFinish={onHandelFinish}
            onValuesChange={onFormLayoutChange}
            onFinishFailed={onFinishFailed}
            onFieldsChange={onFieldsChange}
            fields={
                [
                    {
                        name: 'name',
                        value: data.name
                    },
                    {
                        name: 'email',
                        value: data.email
                    },
                ]
            }
        >
            <Form.Item
                label="Name"
                name="name"
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Email"
                name="email"
            >
                <Input disabled={true}/>
            </Form.Item>
            <Form.Item
                label="Create At"
                name="created_at"
                initialValue={data.createdAt || ['chưa có dữ liệu']}
            >
                <Input disabled={true}/>
            </Form.Item>
            <Form.Item
                label="Role"
                name="role"
                initialValue={data.role?.map(item => item?.id)}
            >
                <Checkbox.Group options={options}/>
            </Form.Item>
            <Form.Item className="w-full">
                <div className="flex w-full justify-center">
                    <Button
                        shape={"default"}
                        type={"primary"}
                        htmlType={"submit"}
                        disabled={
                            !form.isFieldsTouched(true) ||
                            !!form.getFieldsError().filter(({errors}) => errors.length).length
                        }
                    >
                        Lưu
                    </Button>
                    <Button
                        className="mx-4"
                        shape={"default"}
                        type="default"
                        onClick={() => setIsShow(true)}
                    >
                        Huỷ
                    </Button>
                </div>
            </Form.Item>
            <Modal
                open={isShow}
                onCancel={handleClose}
                cancelText={t('button.close')}
                onOk={handleOk}
                confirmLoading={confirmLoading}
            >
                <p>{modalText}</p>
            </Modal>
        </Form>
    );
};

export default EditUser;
