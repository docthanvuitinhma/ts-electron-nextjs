import React, {useCallback, useEffect, useRef, useState} from "react";
import {useLocation, useNavigate, useParams} from "react-router";
import {WiCelsius, WiHumidity, WiThermometer} from "react-icons/wi";
import {Button, Card, Divider, Empty, Pagination, Spin, Tag, Tooltip, Typography} from "antd";
import LightGallery from "lightgallery/react";
import lgZoom from "lightgallery/plugins/zoom";
import lgFullscreen from "lightgallery/plugins/fullscreen";
import {Color} from "../../../const/Color";
import {Query} from "../../functions/Query";
import {SendingStatus} from "../../../const/Events";
import {ImageAction} from "../../../recoil/image/ImageAction";
import CIcon from "@coreui/icons-react";
import {cilClock} from "@duonghainbs/coreui-icons/ts/free/cil-clock";
import ScrollToTop from "react-scroll-to-top";
import {IoImages} from "react-icons/all";
import {VideoCameraOutlined} from "@ant-design/icons";
import {MasterLayoutCtxWrapper} from "../../layouts/MasterLayout";

type TImage = {
    id: number,
    size: string,
    src: string,
    thumb: string,
    subHtml: string
}

const ImageScreen = () => {
    const location = useLocation()
    const navigator = useNavigate()
    const {id} = useParams()
    const [loading, setLoading] = useState(false)
    const {queryToObject} = Query()
    const {
        vm,
        onGetImages,
        onClearState
    } = ImageAction()
    const obj = queryToObject()

    useEffect(() => {
        console.log('%cMount Screen: Image', Color.ConsoleInfo)

        return () => {
            onClearState()
            console.log('%cUnmount Screen: Image', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        onGetImages(id || "", obj)
        setLoading(true)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    useEffect(() => {
        if (vm.status === SendingStatus.success) {
            setLoading(false)
        }

    }, [vm.status])

    const lightGallery = useRef<any>(null)

    const [items, setItems] = useState<TImage[]>([
        {
            id: 1,
            size: '1400-933',
            src: 'w',
            thumb: 'w',
            subHtml: `<div class="lightGallery-captions">
                <h4>Photo by <a href="https://unsplash.com/@dawn">Dan</a></h4>
                <p>Published on November 13, 2018</p>
            </div>`,
        }
    ])

    useEffect(() => {
        const images: TImage[] | undefined = vm.res?.map(item => (
            {
                id: item.order ? item.order : 0,
                size: '1400-933',
                src: item.link ? item.link : '',
                thumb: item.link ? item.link : '',
                subHtml: `<div class="lightGallery-captions">
                            <h4>Photo on ${item.dateShot},  ${item.name} - ${item.size}</h4>
                            <div class="fixed top-[-2476%] right-4 justify-between mt-0.5 font-bold text-[12px] ">
                            <div>  Nhiệt độ : ${item.info.air_temp} °C </div>
                            <div>  Độ ẩm: ${item.info.air_humid} % </div>
                            </div>
                        </div>`
            }
        ))

        if (images !== undefined) {
            setItems(images)
        }

    }, [vm.res])

    const openGallery = (value: number) => {
        lightGallery.current.openGallery(value);
        console.log('có show ảnh')
    };

    const onInit = useCallback((detail: any) => {
        if (detail) {
            lightGallery.current = detail.instance;
        }

    }, []);
    const openWindow = () => {
        window.open("render-video");
    };
    const tool = () => (
        <div>
            <Tooltip trigger={"hover"} title={"Render video"}>
            <Button
                shape={"circle"}
                onClick={openWindow}
                className="text-white bg-[#1a8983] hover:bg-[#1a8979]"
                icon ={<VideoCameraOutlined/>}
            >
            </Button>
            </Tooltip>
        </div>
    )


    return(
        <MasterLayoutCtxWrapper
            tool={tool()}
        >
            {vm.res?.length === 0 && vm.status === SendingStatus.success
                ? <Empty/>
                : (
                    <Spin
                        spinning={loading}
                        size="large">
                        <div>
                            <div className="flex bg-white mb-4 items-center text-center rounded-[2px]">
                                <div className="w-1 bg-orange-400 h-10"></div>
                                <div className="text-[15px] w-fit flex items-center justify-center h-8 m-2 font-bold">
                                    <IoImages className="mr-2 text-blue-400"/> Tổng số hình ảnh : {vm.meta?.totalCount} ảnh
                                </div>
                            </div>
                            <div className={'grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-2'}>
                                {
                                    vm.res?.map((item, index) => (
                                        <Spin key={index} spinning={loading} size={"large"}>
                                            <Card
                                                key={item.order}
                                                bordered={false}
                                                loading={loading}
                                                hoverable
                                                cover={
                                                    // eslint-disable-next-line @next/next/no-img-element
                                                    <>
                                                        <img
                                                            alt={item.name}
                                                            src={item.link}
                                                        />
                                                        <div className="w-full bg-orange-400 h-[8px]"></div>
                                                    </>

                                                }
                                                onClick={() => openGallery(index)}
                                            >
                                                <Card.Meta
                                                    title={<Tooltip className="text-[#1a8983] font-bold text-[18px]" title={'Tên'}>#{item.name}</Tooltip>}
                                                    description={
                                                        <Tooltip
                                                            className="font-bold text-[#4f4f4f]"
                                                            title={'Thời gian chụp'}>
                                                            <CIcon
                                                                icon={cilClock}
                                                                className="mr-2"
                                                            />{item.dateShot}
                                                        </Tooltip>}
                                                />
                                                <Divider/>
                                                <div className={'flex justify-between items-center text-center'}>
                                                    <Tooltip title={'Nhiệt độ'}>
                                                        <Typography.Text
                                                            className="text-[17px]"
                                                        >
                                                            <WiThermometer className={'text-2xl text-red-400'}/>
                                                            {item.info?.air_temp}
                                                            <WiCelsius className={'text-[34px]'}/>
                                                        </Typography.Text>
                                                    </Tooltip>
                                                    <Tooltip title={'Độ ẩm'} className="text-[17px]">
                                                        <Typography.Text>
                                                            <WiHumidity className={'text-2xl text-blue-400'}/>
                                                            {item.info?.air_humid} %
                                                        </Typography.Text>
                                                    </Tooltip>
                                                </div>
                                            </Card>
                                        </Spin>
                                    ))
                                }
                            </div>

                            <div className={'flex justify-center my-3'}>
                                <Pagination
                                    responsive
                                    hideOnSinglePage
                                    showQuickJumper
                                    pageSize={20}
                                    total={vm.meta?.totalCount}
                                    current={vm.meta?.currentPage}
                                    onChange={page => {
                                        navigator(`${location.pathname}?page=${page}`)
                                    }}
                                />
                            </div>

                            <LightGallery
                                elementClassNames={'custom-classname'}
                                onInit={onInit}
                                plugins={[lgZoom, lgFullscreen]}
                                dynamic
                                dynamicEl={items}
                                mode={"lg-fade"}
                            />
                        </div>
                        <ScrollToTop smooth/>
                    </Spin>

                )
            }
        </MasterLayoutCtxWrapper>
        )


}

export default ImageScreen
