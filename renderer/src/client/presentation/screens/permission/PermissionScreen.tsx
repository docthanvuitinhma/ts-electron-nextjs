import {ColumnsType} from "antd/es/table";
import {Button, Empty, Modal, Space, Table} from "antd";
import React, {useEffect, useState} from "react";
import {PermissionAction} from "../../../recoil/permission/PermissionAction";
import {Key, TableRowSelection} from "antd/es/table/interface";
import {Color} from "../../../const/Color";
import {MasterLayoutCtxWrapper} from "../../layouts/MasterLayout";
import {useLocation, useNavigate} from "react-router";
import {RouteConfig} from "../../../config/RouteConfig";
import {DeleteOutlined, ExclamationCircleOutlined, PlusOutlined} from "@ant-design/icons";
import {Query} from "../../functions/Query";
import {SendingStatus} from "../../../const/Events";

interface DataType {
    key: string
    pKey: string
    displayName: string
}

const PermissionScreen = () => {
    const navigator = useNavigate()
    const location = useLocation()
    const [selected, setSelected] = useState<Key[]>([])
    const {queryToObject} = Query()
    const obj = queryToObject()
    const {
        vm,
        onGetPermissions,
        onClearPermissionState
    } = PermissionAction()

    useEffect(() => {
        console.log('%cMount Screen: Permission screen', Color.ConsoleInfo)

        return () => {
            onClearPermissionState()
            console.log('%cUnmount Screen: Permission screen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        onGetPermissions(obj)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const columns: ColumnsType<DataType> = [
        {
            title: 'Display name',
            dataIndex: 'displayName',
            key: 'displayName'
        },
        {
            title: 'Key',
            dataIndex: 'pKey',
            key: 'pKey'
        },
    ]

    const data: DataType[] | undefined = vm.res?.map(item => (

        {
            key: item.id ? item.id.toString() : '',
            pKey: item.key ?? '',
            displayName: item.displayName ?? ''
        }

    ))

    const rowSelection: TableRowSelection<DataType> = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelected(selectedRowKeys)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
    }

    const onAdd = () => {
        navigator(RouteConfig.ADD_PERMISSION)
    }

    const handleInactive = () => {
        Modal.confirm({
            title: 'Vô hiệu hóa quyền?',
            icon: <ExclamationCircleOutlined/>,
            content: 'Bạn muốn vô hiệu hóa quyền?',
            onOk() {
                return new Promise((resolve, reject) => {
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {
            },
        })
    }

    const tool = () => (
        <div className={'hidden sm:block'}>
            <Space>
                <Button
                    type={"primary"}
                    onClick={onAdd}
                >
                    Thêm mới
                </Button>
                <Button
                    danger
                    onClick={handleInactive}
                    disabled={selected.length <= 0}
                >
                    Vô hiệu hóa
                </Button>
            </Space>
        </div>
    )

    const changePage = (page: number) => {
        navigator(`${location.pathname}?page=${page}`)
    }

    return (
        <MasterLayoutCtxWrapper
            tool={tool()}
        >
            {
                vm.res?.length === 0 && vm.status === SendingStatus.success
                    ? <Empty/>
                    : <Table
                        columns={columns}
                        dataSource={data}
                        rowSelection={{...rowSelection}}
                        pagination={{
                            pageSize: 20,
                            position: ["bottomCenter"],
                            hideOnSinglePage: true,
                            onChange: page => changePage(page)
                        }}
                    />
            }

            <div className={'fixed bottom-5 right-5 sm:hidden'}>
                <Space direction={"vertical"}>
                    <Button
                        shape={"circle"}
                        type={"primary"}
                        icon={<PlusOutlined/>}
                        onClick={onAdd}
                    />
                    <Button
                        shape={"circle"}
                        danger
                        onClick={handleInactive}
                        disabled={selected.length <= 0}
                        icon={<DeleteOutlined/>}
                    />
                </Space>
            </div>
        </MasterLayoutCtxWrapper>
    )
}

export default PermissionScreen