import {Button, Card, Form, Input} from "antd";
import {useEffect, useState} from "react";
import {Validation} from "../../../core/Validation";
import {AddPermissionAction} from "../../../recoil/permission/AddPermissionAction";
import {Color} from "../../../const/Color";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {TPermissionV0} from "../../../models/PermissionModel";
import {SendingStatus} from "../../../const/Events";
import {useNavigate} from "react-router";
import {noti} from "../../functions/Notification";
import {RouteConfig} from "../../../config/RouteConfig";

let timer: NodeJS.Timeout

const AddPermission = () => {
    const navigator = useNavigate()
    const [key, setKey] = useState('')
    const [displayNameError, setDisplayNameError] = useState('')
    const [keyError, setKeyError] = useState('')
    const {
        vm,
        onAddPermission,
        onClearState
    } = AddPermissionAction()


    useEffect(() => {
        console.log('%cMount Screen: Add permission', Color.ConsoleInfo)

        return () => {
            onClearState()

            console.log('%cUnmount Screen: Add permission', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (vm.status === SendingStatus.success) {
            noti('success', 'Thêm mới quyền thành công')
            navigator(RouteConfig.PERMISSION)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.status])

    useEffect(() => {
        if (vm.status === SendingStatus.error && vm.error) {
            if (vm.error.hasOwnProperty('key')) {
                setKeyError(vm.error.key)
            }
            if (vm.error.hasOwnProperty('display_name')) {
                setDisplayNameError(vm.error.display_name)
            }
        }
    }, [vm.status, vm.error])

    const debounce = (func: Function, milliseconds?: number) => {
        const time = milliseconds || 900

        return (...arg: string[]) => {
            clearTimeout(timer)

            timer = setTimeout(() => {
                func.apply(this, arg)
            }, time)
        }
    }

    const createKey = (value: string) => {
        const key = value.split(' ').join('-')
        setKey(key)
    }

    const onFinish = (data: TPermissionV0) => {
        onAddPermission({
            key: data.key,
            display_name: data.displayName
        })
    }

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach(e => {
            switch (e.name[0]) {
                case 'key':
                    setKeyError(e.errors[0])
                    break
                case 'displayName':
                    setDisplayNameError(e.errors[0])
                    break
            }
        })
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach(e => {
            // @ts-ignore
            if (e.name[0] === 'displayName') {
                const process = debounce(() => {
                    // @ts-ignore
                    if (e.errors[0]) {
                        // @ts-ignore
                        setDisplayNameError(e.errors[0])
                    }
                })

                process()
            }
            // @ts-ignore
            if (e.name[0] === 'key' && e.errors[0]) {
                // @ts-ignore
                setKeyError(e.errors[0])
            }
        })
    }

    return (
        <Card>
            <Form
                labelCol={{span: 8}}
                wrapperCol={{span: 14}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                onFieldsChange={onFieldsChange}
                fields={[
                    {
                        name: 'key',
                        value: key
                    }
                ]}
            >
                <Form.Item
                    name={'displayName'}
                    label={'Display name'}
                    rules={[
                        {required: true},
                        {pattern: Validation.FORMAT_NAME}
                    ]}
                    validateStatus={displayNameError ? 'error' : ''}
                    help={displayNameError ? displayNameError : ''}
                >
                    <Input
                        placeholder={'Display name'}
                        onBlur={e => {
                            createKey(e.target.value)
                            setKeyError('')
                        }}
                        onKeyUp={() => setDisplayNameError('')}
                    />
                </Form.Item>

                <Form.Item
                    label={'Key'}
                    name={'key'}
                    rules={[
                        {required: true}
                    ]}
                    initialValue={key}
                    validateStatus={keyError ? 'error' : ''}
                    help={keyError ? keyError : ''}
                >
                    <Input
                        disabled
                        placeholder={'Key'}
                    />
                </Form.Item>

                <Button type={"primary"} htmlType={"submit"}>Lưu</Button>
            </Form>
        </Card>
    )
}

export default AddPermission