import React, {useEffect} from "react";
import {useNavigate, useParams} from "react-router";
import {Card, Divider, Empty, Tooltip} from "antd";
import {Color} from "../../../const/Color";
import Plyr from "plyr-react";
import {SendingStatus} from "../../../const/Events";
import {VideoAction} from "../../../recoil/video/VideoAction";
import {DownloadOutlined, VideoCameraOutlined} from "@ant-design/icons";

const VideoScreen = () => {
    const {id} = useParams()
    console.log("param", id)
    const {
        vm,
        onGetVideos,
        onClearVideoState
    } = VideoAction()

    useEffect(() => {

        console.log('%cMount Screen: Video', Color.ConsoleInfo)
        onGetVideos(id || "")
        return () => {

            onClearVideoState()
            console.log('%cUnmount Screen: Video', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const cardVideos = () => (
        vm.res?.map((item, index) => (
            <Card
                key={index}
                actions={[
                    // eslint-disable-next-line react/jsx-no-target-blank
                    <div className="flex">
                        <a
                            key={index}
                            className={'text-black no-underline'}
                            href={item.url}
                            target={'_blank'}
                        >
                            <Tooltip title="Tải xuống video">
                                <DownloadOutlined/>
                            </Tooltip>
                        </a>
                    </div>


                ]}
                cover={
                    <>
                        <div
                            className={''}
                            id={'video'}
                        >
                            <Plyr
                                onClick={event => console.log(event)}
                                source={{
                                    type: "video",
                                    sources: [
                                        {
                                            src: item.url ?? ''
                                        }
                                    ]
                                }}
                            />
                        </div>
                        <div className="w-full bg-orange-400 h-[8px]"></div>
                    </>

                }
            >
                <Card.Meta
                    title={item.name}
                    description={item.size}
                />
            </Card>
        ))
    )

    return vm.res?.length === 0 && vm.status === SendingStatus.success
        ? <Empty/>
        : (
            <div className={'grid grid-cols-1 md:grid-cols-2 2xl:grid-cols-3 gap-2'}>
                {
                    cardVideos()
                }
            </div>
        )
}

export default VideoScreen
