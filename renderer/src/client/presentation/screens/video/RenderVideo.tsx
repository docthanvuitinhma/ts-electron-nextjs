import {useParams} from "react-router";
import {useEffect, useState} from "react";
import {ImageAction} from "../../../recoil/image/ImageAction";
import {Query} from "../../functions/Query";
import {Button, Spin} from "antd";
import {noti} from "../../functions/Notification";

class AppConfig {
    pathSaveImages: string = 'project/image'

    static createPathSaveImages = (name: string): string => {
        return `projectDirectory/image/${name}`;
    }
}

const RenderVideo = () => {
    const {v4: uuidv4} = require('uuid');
    const ipc = require('electron').ipcRenderer;
    const process = require('child_process')
    const $ = require('jquery')
    const randomString = require('random-string');
    const fs = require('fs')
    let format = 'mp3'
    let dir = './media';
    const {queryToObject} = Query()
    const obj = queryToObject()
    const params = useParams();
    console.log()
    const {vm, onGetImages} = ImageAction()
    const [showNoti, setShowNoti] = useState(false);
    useEffect(() => {
        onGetImages(params.id, obj)
    }, [])
    const link_image = vm.res?.map(item => item.link)
    const handleClick = () => {
        format = $("#format option:selected").text();
        ipc.send('open-file-dialog-for-file')
    }
    const selectFolder = () => {
        ipc.send('select-folder-to-save', link_image)
    }
    $("#format").change(function () {
        format = $("#format option:selected").text();
    })

    ipc.on('selected-file', function (event, paths) {
        console.log(event)
        let randomId = randomString()
        setShowNoti(true)
        console.log('Full path: ', paths)
        process.exec(`ffmpeg -framerate 10 -i "${paths}/%05d.jpg" -s "6000x4000" "D:/code/dev/rd-electronjs-atl-desktop/media/VideoRender/video6k.mp4"`, function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            setShowNoti(false)
            $(`#${randomId}`).detach()
            // Notification.requestPermission().then(function (result) {
            //         const myNotification = new Notification("Render Video Success", {
            //             body: "Your file was successfully!"
            //         });
            //     }
            // )
                if (error !== null) {
                    console.log('exec error: ' + error);
                    noti("error", "Có lỗi xảy ra , vui lòng thử lại")
                    setShowNoti(false)
                } else {
                    noti('success', 'Render video success')
                }
        }
        )
    });

    return (
        <div>
            <h1 className="text-center">Render Video</h1>
            <div className="form-group mt-2">
                <label htmlFor="format">Chọn định dạng:</label>
                <select className="form-control" id="format">
                    <option>mp4</option>
                    <option></option>
                    <option></option>
                </select>
            </div>
            <div className="flex w-full">
                <button
                    className="btn btn-danger text-white  mt-6"
                    id="upload"
                    onClick={handleClick}
                >Render Videos
                </button>
            </div>
            {
                showNoti &&
                <div className='alert alert-success mt-4'><Spin /> Video của bạn đang trong quá trình render, vui lòng đợi trong giây lát ...</div>
            }

        </div>
    )
}
export default RenderVideo
