import React, {useCallback, useEffect, useState} from "react";
import {Chart as ChartJS, ArcElement, Tooltip, Legend} from "chart.js";
import {Doughnut} from "react-chartjs-2";
import {DiskUsageAction} from "../../../recoil/disk/DiskUsageAction";
import {useTranslation} from "react-i18next";
import {Space} from "antd";
import DashBoardCard from "./DashBoardCard";
ChartJS.register(ArcElement, Tooltip, Legend);


interface DataType {
    key: string
    name: string
    total: number
    available: number
    used: number
    unit: string
    percent: number
}

const Dashboard = () => {
    useEffect(() => {
        onGetListDisk()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const {t} = useTranslation()
    const [activeIndex, setActiveIndex] = useState<string | number>(0);
    const {vm, onGetListDisk} = DiskUsageAction();
    const data1: DataType[] = vm.diskusage?.map<DataType>(item => (
        {
            key: item.name.toString(),
            name: item.name,
            total: item.total,
            available: item.available,
            used: item.used,
            unit: item.unit.toString(),
            percent: item.percent
        }
    )) ?? []

    const data = {
        labels: [`${t("dashboard.used")}`, `${t("dashboard.available")}`, `${t("dashboard.unit")} :GB`],
        datasets: [
            {
                label: "# of Votes",
                data: [data1[0]?.used, data1[0]?.available],
                backgroundColor: [
                    "rgba(255, 99, 132)",
                    "rgba(54, 162, 235)",
                    "rgba(255, 206, 86)",
                ],
                borderColor: [
                    "rgba(255, 99, 132, 1)",
                    "rgba(54, 162, 235, 1)",
                    "rgba(255, 206, 86, 1)",
                ],
                hoverBorderWidth: 20
            }
        ]
    };
    const data0 = {
        labels: [`${t("dashboard.used")}`, `${t("dashboard.available")}`, `${t("dashboard.unit")} :GB`],
        datasets: [
            {
                label: "# of Votes",
                data: [data1[1]?.used, data1[1]?.available],
                backgroundColor: [
                    "rgba(255, 99, 132)",
                    "rgba(54, 162, 235)",
                    "rgba(255, 206, 86)",
                ],
                borderColor: [
                    "rgba(255, 99, 132, 1)",
                    "rgba(54, 162, 235, 1)",
                    "rgba(255, 206, 86, 1)",
                ],
                hoverBorderWidth: 20
            }
        ]
    };
    const data2 = {
        labels: [`${t("dashboard.used")}`, `${t("dashboard.available")}`, `${t("dashboard.unit")} :GB`],
        datasets: [
            {
                label: "# of Votes",
                data: [data1[2]?.used, data1[2]?.available],
                backgroundColor: [
                    "rgba(255, 99, 132)",
                    "rgba(54, 162, 235)",
                    "rgba(255, 206, 86)",
                ],
                borderColor: [
                    "rgba(255, 99, 132, 1)",
                    "rgba(54, 162, 235, 1)",
                    "rgba(255, 206, 86, 1)",
                ],
                hoverBorderWidth: 20
            }
        ]
    };
    const data3 = {
        labels: [`${t("dashboard.used")}`, `${t("dashboard.available")}`, `${t("dashboard.unit")} :GB`],
        datasets: [
            {
                label: "GB",
                data: [data1[3]?.used, data1[3]?.available],
                backgroundColor: [
                    "rgba(255, 99, 132)",
                    "rgba(54, 162, 235)",
                ],
                borderColor: [
                    "rgba(255, 99, 132, 1)",
                    "rgba(54, 162, 235, 1)",
                ],
                hoverBorderWidth: 20
            }
        ]
    };
    const onPieEnter = useCallback(
        (_: any, index: string) => {
            setActiveIndex(index);
            console.log(activeIndex);
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [setActiveIndex]
    );
    return (
        <div>
            <div className=" hidden sm:flex " >
                <div className="block">
                    <Doughnut data={data} onMouseEnter={() => onPieEnter}/>
                    <div className=" flex uppercase w-full justify-center font-bold mt-6 ">{data1[0]?.name} </div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.total")}: <span className="font-bold"> {data1[0]?.total} {data1[0]?.unit}</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.used")} : <span className="font-bold"> {data1[0]?.percent} %</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.available")} : <span className="font-bold"> {data1[0]?.available} {data1[0]?.unit} </span></div>
                </div>
                <div className="block">
                    <Doughnut data={data0} onMouseEnter={() => onPieEnter}/>
                    <div className=" uppercase flex justify-center font-bold mt-6 ">{data1[1]?.name} </div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.total")} : <span className="font-bold"> {data1[1]?.total} {data1[1]?.unit}</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.used")}: <span className="font-bold"> {data1[1]?.percent} %</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.available")}: <span className="font-bold"> {data1[1]?.available} {data1[1]?.unit} </span></div>
                </div>
                <div className="block">
                    <Doughnut data={data2} onMouseEnter={() => onPieEnter}/>
                    <div className=" uppercase flex justify-center font-bold mt-6 ">{data1[2]?.name}  </div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.total")} : <span className="font-bold"> {data1[2]?.total} {data1[2]?.unit}</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.used")} : <span className="font-bold"> {data1[2]?.percent} %</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.available")} : <span className="font-bold"> {data1[2]?.available} {data1[2]?.unit}</span></div>
                </div>
                <div className="block">
                    <Doughnut data={data3} onMouseEnter={() => onPieEnter}/>
                    <div className=" uppercase flex justify-center font-bold mt-6 ">{data1[3]?.name}</div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.total")}: <span className="font-bold"> {data1[3]?.total} {data1[3]?.unit}</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.used")} : <span className="font-bold"> {data1[3]?.percent} %</span></div>
                    <div className=" flex uppercase w-full justify-center">{t("dashboard.available")} : <span className="font-bold"> {data1[3]?.available} {data1[3]?.unit}</span></div>
                </div>
            </div>
            <div className={'block sm:hidden'}>
                <Space
                    direction={"vertical"}
                    size="middle"
                    style={{display: 'flex'}}
                >
                    {
                        vm.diskusage?.map((item,index) => {
                            return <DashBoardCard item={item} key={index}/>
                        })
                    }
                </Space>
            </div>
        </div>

    );
}
export default Dashboard
