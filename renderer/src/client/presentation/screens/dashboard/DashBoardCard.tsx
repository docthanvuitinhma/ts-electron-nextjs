import {Avatar, Card, Tooltip, Typography} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";

const  DashBoardCard  = ({...props}) => {
    const {t} = useTranslation()
    return(
        <Card className="shadow-lg shadow-cyan-500/50 mb-2">
            <Card.Meta
                avatar={<Avatar src="https://joeschmoe.io/api/v1/random"/>}
                title={
                    <div className='items-center '>
                        <Tooltip title={"Tên ổ đĩa"}>
                            <Typography.Text className="text-xl font-bold">{props.item.name}</Typography.Text>
                        </Tooltip>
                    </div>
                }
            />
            <hr/>
            <div className="text-[18px]">
                <div className="flex justify-between items-center text-center">
                    <div >{t("dashboard.total")}:</div>
                    <div className="font-bold flex">
                        <span> {props.item?.total}</span>
                        <span className="ml-2"> {props.item?.unit}</span>
                    </div>
                </div>
                <div className="flex justify-between my-4">
                    <div className="w-full ">{t("dashboard.used") }:</div>
                    <span className="flex font-bold "> {props.item?.percent}</span>
                    <span className="flex ml-2 font-bold ">%</span>
                </div>
                <div className="flex justify-between">
                    <div>{t("dashboard.available")}:</div>
                    <div className="flex font-bold">
                        <span> {props.item?.available} </span>
                        <span className="ml-2"> {props.item?.unit}</span>
                    </div>
                </div>
            </div>

        </Card>
    )
}
export default DashBoardCard