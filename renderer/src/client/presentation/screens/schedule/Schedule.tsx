import {Button, Card, Col, DatePicker, Empty, Pagination, Row, Select, Space, Spin} from "antd";
import {ScheduleAction} from "../../../recoil/schedule/ScheduleAction";
import React, {useEffect, useState} from "react";
import {SendingStatus} from "../../../const/Events";
import {ScheduleTable} from "./ScheduleTable";
import {Key} from "antd/es/table/interface";
import {Query} from "../../functions/Query";
import {useNavigate} from "react-router";
import {useLocation} from "react-router-dom";
import {ListQueueAction} from "../../../recoil/list_queue/ListQueueAction";
import ScrollToTop from "react-scroll-to-top";
import {SchedulesCard} from "./ScheduleCard";
import {useTranslation} from "react-i18next";


const Schedule = () => {
    const {onGetListQueue, vm1} = ListQueueAction()
    const {onGetListJob, vm} = ScheduleAction()
    const location = useLocation()
    const {t} = useTranslation()
    useEffect(() => {
        onGetListJob(obj)
        setLoading(true)
        onGetListQueue()

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location]);

    useEffect(() => {
        if (vm.status === SendingStatus.success) {
            setLoading(false)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.status])
    const [loading, setLoading] = useState(false);
    const [, setSelected] = useState<Key[]>([])
    const {Option, OptGroup} = Select;
    const navigate = useNavigate()
    const {queryToObject, dataToQuery} = Query()
    const obj = queryToObject()
    const handleSelectRows = (key: []) => {
        setSelected(key)
    }
    const onSubmit = (value: any) => {
        if (value === "all") {
            navigate(`${location.pathname}`)
        }
        else
            navigate(`${location.pathname}?queue=${value}`)
    }
    const onPickTime = (value: any) => {
        if (value === null) {
            navigate(`${location.pathname}`)
        }
        else navigate(`${location.pathname}?started_at=${value?._d.toISOString()}`)
    }
    const onFinish = (value: any) => {
        if (value === null) {
            navigate(`/schedule`)
        }
        else
            navigate(`${location.pathname}?type=${value}`)
    }

    return (
        <div>
            <div className="font-semibold text-2xl text-blue-900 uppercase">{t("schedule.title_queue")}</div>
            {
                vm.schedule?.length === 0 && vm.status === SendingStatus.success
                    ? <Empty/>
                    : <Spin
                        spinning={loading}
                        size="large"
                    >
                        <Row gutter={16} className="pt-8">
                            <Col span={22}>
                                <div className={'hidden sm:block'}>
                                    <Row gutter={16} className="mb-12">
                                        <Col span={8}>
                                            <Card
                                                title={`${t("schedule.total_job")}`}
                                                className="font-bold  shadow-lg shadow-cyan-500/50 h-full"
                                                bordered={false}>
                                                <div className="text-3xl flex items-center mt-8 justify-center">
                                                    {vm.meta?.totalCount}
                                                    <div className="text-gray-400 ml-4 uppercase text-3xl">Jobs</div>
                                                </div>
                                            </Card>
                                        </Col>
                                        <Col span={16}>
                                            <Card
                                                title={`${t("schedule.filter")}`}
                                                className="!font-bold shadow-lg shadow-cyan-500/50 "
                                                bordered={false}>
                                                <div className="pb-4">
                                                    <div className="uppercase text-gray-400">show Jobs</div>
                                                    <Select
                                                        defaultValue={`${t("schedule.all")}`}
                                                        style={{width: 200}}
                                                        onSelect={onFinish}
                                                        filterOption={(input, option) => (
                                                            option!.children as unknown as string).includes(input)
                                                        }
                                                    >
                                                        <OptGroup label="Trạng thái">
                                                            <Option value="all">{`${t("schedule.all")}`}</Option>
                                                            <Option value="succeeded">{`${t("schedule.success")}`}</Option>
                                                            <Option value="running">{`${t("schedule.running")}`}</Option>
                                                            <Option value="failed">{`${t("schedule.failed")}`}</Option>
                                                        </OptGroup>
                                                    </Select>
                                                </div>
                                                <div className="pb-4">
                                                    <div className="uppercase text-gray-400">Queue</div>
                                                    <Select
                                                        defaultValue={`${t("schedule.all")}`}
                                                        style={{width: 200}}
                                                        onSelect={onSubmit}
                                                        filterOption={true}
                                                    >
                                                        <Option value="all">{`${t("schedule.all")}`}</Option>
                                                        {
                                                            vm1.queue?.map((item, index) =>
                                                                <Option label="queue" value={item?.name} key={index}>{item?.name}</Option>
                                                            )
                                                        }
                                                    </Select>

                                                </div>
                                                <div className="pb-4">
                                                    <div className="uppercase text-gray-400">Started AT</div>
                                                    <Space direction="vertical" size={12}>
                                                        <DatePicker name={"started_at"} onSelect={onPickTime}/>
                                                    </Space>
                                                </div>
                                                <Button
                                                    type="primary"
                                                    htmlType={"submit"}
                                                    onClick={() => navigate('/schedule')}
                                                    disabled={vm.meta?.totalCount === 119}
                                                >{`${t("schedule.reset_button")}`}</Button>
                                            </Card>
                                        </Col>
                                    </Row>
                                    <ScheduleTable selectRows={handleSelectRows}/>
                                </div>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <div className={'sm:hidden'}>
                                    <Space
                                        direction={"vertical"}
                                        size="middle"
                                        style={{display: 'flex'}}
                                    >
                                        <Card
                                            title={`${t("schedule.total_job")}`}
                                            className="font-bold  shadow-lg shadow-cyan-500/50 h-[140px]"
                                            bordered={false}>
                                            <div className="flex justify-center text-center items-center">
                                                <div className="text-4xl">{vm.meta?.totalCount} </div>
                                                <div className="text-gray-400 uppercase text-4xl ml-4">Jobs</div>
                                            </div>
                                        </Card>
                                        {
                                            vm.schedule?.map((item, index) => {
                                                return <SchedulesCard item={item} key={index}/>
                                            })
                                        }
                                    </Space>
                                </div>
                            </Col>
                        </Row>
                    </Spin>
            }

            <div className={'flex justify-center my-3'}>
                <Pagination
                    onChange={page => {
                        setLoading(true)
                        navigate(`${location.pathname}?${dataToQuery(page)}`)
                    }}
                    responsive
                    pageSize={20}
                    current={vm.meta?.currentPage}
                    total={vm.meta?.totalCount}
                    hideOnSinglePage
                />
            </div>
            <ScrollToTop smooth/>
        </div>
    )
}
export default Schedule
