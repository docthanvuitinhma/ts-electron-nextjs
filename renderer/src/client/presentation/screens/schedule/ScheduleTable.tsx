import {Button, Table, Tag, Tooltip} from "antd";
import {ColumnsType} from "antd/es/table";
import {TableRowSelection} from "antd/es/table/interface";
import {ScheduleAction} from "../../../recoil/schedule/ScheduleAction";
import {useState} from "react";
import {FiMoreHorizontal} from "react-icons/fi";
import {BsClockHistory} from "react-icons/bs";
import {VscError} from "react-icons/vsc";
import {GiDuration} from "react-icons/gi";
import {AiOutlineCheckCircle} from "react-icons/ai";
import {CheckCircleOutlined, CloseCircleOutlined, SyncOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";


type TProps = {
    selectRows: Function
}

interface DataType {
    key: number
    name: string
    status: string
    detail: {
        queue: string
        attempt: string
    }
    duration: string
    started: string
    error: string
}

export const ScheduleTable = (props: TProps) => {
    const [showErr, setShowErr] = useState(false);
    const {vm} = ScheduleAction()
    const {t} = useTranslation()
    const columns: ColumnsType<DataType> = [
        {
            title: `${t("schedule.job_name")}`,
            dataIndex: 'name',
            key: 'name',
            render: (name) => {
                return (
                    <div className="font-bold">
                        {name}
                    </div>
                )
            }
        },
        {
            title: `${t("schedule.status")}`,
            dataIndex: 'status',
            key: 'status',
            filters: [
                {
                    text: `${t("schedule.success")}`,
                    value: 'Success',
                },
                {
                    text: `${t("schedule.running")}`,
                    value: 'Running',
                },
                {
                    text: `${t("schedule.error")}`,
                    value: 'Failed',
                },
            ],
            render: (status) => {
                let color: any = []
                let icons: any = ""
                if (status === "Running") {
                    color = "blue"
                    icons = <SyncOutlined spin/>
                }
                if (status === "Success") {
                    color = "green"
                    icons = <CheckCircleOutlined/>
                }
                if (status === "Failed") {
                    color = "red"
                    icons = <CloseCircleOutlined/>
                }
                return (
                    <Tag color={color} key={status} icon={icons} className="flex items-center">
                        {status.toUpperCase()}
                    </Tag>
                );
            },
            onFilter: (value, record) => record.status.includes(value.toString()),
        },
        {
            title: `${t("schedule.details")}`,
            dataIndex: 'detail',
            key: 'detail',
            render: (detail) => {
                return (
                    <div>
                        <div className="flex"><span className="text-gray-400 mr-2">QUEUE:</span> {detail.queue}</div>
                        <div className="flex"><span className="text-gray-400 mr-2">Attempt:</span> {detail.attempt}</div>
                    </div>
                );
            },
            onFilter: (value, record) => record.detail.queue.includes(value.toString()),
        },
        {
            title: "Duration",
            dataIndex: 'duration',
            key: 'duration',
            render: (duration) => {
                return (
                    <div className="flex text-center items-center">
                        <GiDuration className="mr-2"/>
                        {duration}
                    </div>
                )
            }
        },
        {
            title: `${t("schedule.started_at")}`,
            dataIndex: 'started',
            key: 'started',
            render: (started) => {
                return (
                    <div className="italic flex items-center">
                        <BsClockHistory className="mr-2"/> {started}
                    </div>
                )
            },
            sorter: (a: any, b: any) => a.started - b.started,
        },
        {
            title: `${t("schedule.error")}`,
            dataIndex: 'error',
            key: 'error',
            className: "max-w-[200px]",
            render: error => {
                return (
                    error === ""
                        ? <div className="text-blue-400 flex items-center"><AiOutlineCheckCircle className="mr-2"/> Không có lỗi</div>
                        : <div className="flex">
                            {
                                !showErr &&
                                <div className="w-[40px] truncate mr-6 text-red-400">
                                    <VscError/> {error}
                                </div>
                            }

                            {
                                showErr &&
                                <div className="w-[80px] mr-6 text-red-400 ">
                                    <VscError/> {error}
                                </div>
                            }
                            <Tooltip title={"Xem chi tiết"}>
                                <Button
                                    shape={"circle"}
                                    onClick={() => setShowErr(!showErr)}
                                >
                                    <FiMoreHorizontal/>
                                </Button>
                            </Tooltip>
                        </div>
                )
            }

        }
    ]

    const data: DataType[] = vm.schedule?.map((item, index) => (
        {
            key: index,
            name: item.name,
            status: item.status,
            detail: {
                queue: item.queue,
                attempt: item.attempt
            },
            duration: item.duration,
            started: item.started,
            error: item.error

        }
    )) ?? []

    const rowSelection: TableRowSelection<DataType> = {
        onChange: (selectedRowKeys, selectedRows) => {
            props.selectRows(selectedRowKeys)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
    }

    return (
        <Table
            dataSource={data}
            columns={columns}
            rowSelection={{...rowSelection}}
            pagination={{pageSize: 20, position: []}}
        />
    )
}
