import {Avatar, Button, Card, Drawer, Space, Tag, Tooltip, Typography} from "antd";
import {CheckCircleOutlined, CloseCircleOutlined, FilterOutlined,SyncOutlined} from "@ant-design/icons";
import {FiMoreHorizontal} from "react-icons/fi";
import {BsClockHistory} from "react-icons/bs";
import {Divider} from "antd/es";
import {AiOutlineCheckCircle} from "react-icons/ai";
import {VscError} from "react-icons/vsc";
import {MasterLayoutCtxWrapper} from "../../layouts/MasterLayout";
import {FormFilter} from "./FormFilter";
import {useState} from "react";


export const SchedulesCard = ({...props}) => {
    const [showError, setShowError] = useState(false)
    const [showDrawer, setShowDrawer] = useState(false);
    const handleFilter = (isVisible: boolean) => {
        setShowDrawer(isVisible)
    }
    let colors: any = ""
    let icons: any = ""
    if (props.item.status === "Running") {
        colors = "blue"
        icons = <SyncOutlined spin/>
    }
    if (props.item.status === "Success") {
        colors = "green"
        icons = <CheckCircleOutlined/>
    }
    if (props.item.status === "Failed") {
        colors = "red"
        icons = <CloseCircleOutlined/>
    }
    const tool = () => (
        <div className={'block sm:hidden'}>
            <Space>
                <Button
                    type={"primary"}
                    shape={"circle"}
                    onClick={() => setShowDrawer(!showDrawer)}
                    icon={<FilterOutlined/>}
                />
            </Space>
        </div>
    )

    return (
        <MasterLayoutCtxWrapper
            tool={tool()}
        >
            <Card>
                <div className={'flex justify-between'}>
                    <Card.Meta
                        title={
                            <div className={'flex items-center gap-2'}>
                                <Tooltip title={'Tên công việc'}>
                                    <Typography.Text className="!text-[#009b90] text-xl">{props.item.name}</Typography.Text>
                                </Tooltip>
                            </div>
                        }
                        description={
                            <div className={'flex items-center gap-2'}>
                                <Tooltip title={"Ngày bắt đầu"}>
                                    <div className={'text-gray-400 flex justify-center items-center'}>
                                        <BsClockHistory className="mr-2"/> {props.item.started}
                                    </div>
                                </Tooltip>
                            </div>
                        }
                    />
                </div>
                <Divider/>
                <div>
                    <Tooltip title={"Trạng thái"}>
                        <div className="flex justify-between mx-auto mt-6 text-[15px]">
                            <div>Trạng thái :</div>
                            <Tag className=" h-[25px] flex items-center" color={colors} icon={icons}>
                                {props.item.status}
                            </Tag>

                        </div>
                    </Tooltip>
                    <Tooltip title={"Queue"}>
                        <div className="flex justify-between mx-auto my-2 text-[15px]">
                            <div>Queue :</div>
                            <div className=" font-bold ">
                                {props.item.queue}
                            </div>
                        </div>
                    </Tooltip>
                    <Tooltip title={"Queue"}>
                        <div className="flex justify-between mx-auto mb-2 text-[15px]">
                            <div>Duration:</div>
                            <div className=" italic">
                                {props.item.duration}
                            </div>
                        </div>
                    </Tooltip>
                    <div className="flex justify-between mx-auto text-[15px]">
                        <div>Error:</div>
                        {props.item.error == ""
                            ? <div className=" text-green-400"><AiOutlineCheckCircle className="mr-2"/> Không có lỗi</div>
                            : <div className="flex text-center">
                                {
                                    !showError &&
                                    <div className="truncate w-[80px] text-red-400">
                                        <VscError className="mr-2"/>
                                        {props.item.error}
                                    </div>
                                }
                                {
                                    showError &&
                                    <div className="mx-10 w-[80px] text-red-400">
                                        <VscError className="mr-2"/>
                                        {props.item.error}
                                    </div>
                                }
                                <Tooltip title={"Chi tiết"}>
                                    <Button
                                        shape={"circle"}
                                        className={"!text-[10px] ml-3"}
                                        onClick={() => setShowError(!showError)}
                                    >
                                        <FiMoreHorizontal/>
                                    </Button>
                                </Tooltip>
                            </div>
                        }
                    </div>
                </div>
            </Card>
            <Drawer
                className={'z-[1030]'}
                title={
                    <Typography.Title level={4}>
                        Nhập vào đây để bắt đầu lọc
                    </Typography.Title>
                }
                open={showDrawer}
                onClose={() => setShowDrawer(false)}
            >
                <FormFilter parentCallback={handleFilter}/>
            </Drawer>
        </MasterLayoutCtxWrapper>
    )
}