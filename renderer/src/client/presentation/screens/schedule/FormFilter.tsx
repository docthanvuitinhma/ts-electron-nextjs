import {Button,Form,Select} from "antd";
import React from "react";
import {useLocation, useNavigate} from "react-router";
import {Query} from "../../functions/Query";

type TProps = {
    parentCallback: Function
}

export const FormFilter = (props: TProps) => {
    const [form] = Form.useForm();
    const location = useLocation()
    const navigator = useNavigate()
    const {dataToQuery} = Query()

    const onFinish = (data: any) => {
        // onPickTime(data.started)
        props.parentCallback(false)
        navigator(`${location.pathname}?${dataToQuery(data)}`)
    }

    const onCancel = () => {
        form.resetFields()

        props.parentCallback(false)
        navigator(`${location.pathname}`)
    }
    // const onPickTime = (value:any) =>{
    //     if(value === null) {
    //         navigator(`${location.pathname}`)
    //     }
    //     else navigator(`${location.pathname}?started_at=${value?._d.toISOString()}`)
    //
    // }

    return (
        <div className={"flex flex-col"}>

            <Form
                form={form}
                labelCol={{span: 8}}
                wrapperCol={{span: 14}}
                layout="horizontal"
                onFinish={onFinish}
            >
                <Form.Item
                    name={'type'}
                >
                    <Select
                        options={[
                            {
                                label: 'Đã hoàn thành',
                                value: 'succeeded'
                            },
                            {
                                label: 'Đang hoạt động',
                                value: 'running'
                            },
                            {
                                label: 'Vô hiệu hoá',
                                value: 'failed'
                            },
                        ]}
                    />
                </Form.Item>
                <div className={'flex justify-between'}>
                    <Button
                        onClick={onCancel}
                        danger
                    >
                        Hủy
                    </Button>
                    <Button
                        type={"primary"}
                        htmlType={"submit"}
                    >
                        Lọc
                    </Button>
                </div>
            </Form>
        </div>
    )
}
