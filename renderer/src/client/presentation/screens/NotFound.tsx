import {useNavigate} from "react-router-dom";
import {Button, Result} from "antd";

const PageNotFound = () => {
    const navigate = useNavigate();
    return (
        <>
            <Result
                status="404"
                title="404 | Page not found"
                subTitle="Oops, có gì đó sai sai .Chúng tôi rất tiếc nhưng trang web bạn truy cập không tồn tại."
                extra={<Button
                    type="default"
                    onClick={() => navigate("/")}
                    shape={"round"}
                    className="cursor-pointer hover:text-blue-400 text-[1rem] items-center"
                >Quay lại trang chủ</Button>}
            />
        </>
    );
};

export default PageNotFound;
