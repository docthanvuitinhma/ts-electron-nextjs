import {ColumnsType} from "antd/es/table";
import {RoleAction} from "../../../recoil/role/RoleAction";
import React, {useEffect, useState} from "react";
import {Button, Empty, Modal, Space, Table} from "antd";
import {Key, TableRowSelection} from "antd/es/table/interface";
import {MasterLayoutCtxWrapper} from "../../layouts/MasterLayout";
import {useLocation, useNavigate} from "react-router";
import {RouteConfig} from "../../../config/RouteConfig";
import {DeleteOutlined, ExclamationCircleOutlined, PlusOutlined} from "@ant-design/icons";
import {Query} from "../../functions/Query";
import {Color} from "../../../const/Color";
import {SendingStatus} from "../../../const/Events";

interface DataType {
    key: string
    name: string
    displayName: string
}

const RoleScreen = () => {
    const navigator = useNavigate()
    const location = useLocation()
    const {queryToObject} = Query()
    const obj = queryToObject()
    const [selected, setSelected] = useState<Key[]>([])
    const {
        vm,
        onGetRoles,
        onClearRoleState
    } = RoleAction()

    useEffect(() => {
        console.log('%cMount Screen: Permission screen', Color.ConsoleInfo)
        return () => {
            onClearRoleState()
            console.log('%cUnmount Screen: Permission screen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        onGetRoles(obj)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    const columns: ColumnsType<DataType> = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Display name',
            dataIndex: 'displayName',
            key: 'displayName'
        },
    ]

    const data: DataType[] | undefined = vm.res?.map((item, index) => (
        {
            key: index.toString(),
            name: item.name ?? '',
            displayName: item.displayName ?? ''
        }
    ))

    const rowSelection: TableRowSelection<DataType> = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelected(selectedRowKeys)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
    }

    const onAdd = () => {
        navigator(RouteConfig.ADD_ROLE)
    }

    const handleInactive = () => {
        Modal.confirm({
            title: 'Vô hiệu hóa vai trò?',
            icon: <ExclamationCircleOutlined/>,
            content: 'Bạn muốn vô hiệu hóa vai trò?',
            onOk() {
                return new Promise((resolve, reject) => {
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {
            },
        })
    }

    const tool = () => (
        <div className={'hidden sm:block'}>
            <Space>
                <Button
                    type={"primary"}
                    onClick={onAdd}
                >
                    Thêm mới
                </Button>
                <Button
                    danger
                    onClick={handleInactive}
                    disabled={selected.length <= 0}
                >
                    Vô hiệu hóa
                </Button>
            </Space>
        </div>
    )

    const changePage = (page: number) => {
        navigator(`${location.pathname}?page=${page}`)
    }

    return (
        <MasterLayoutCtxWrapper
            tool={tool()}
        >
            {
                vm.res?.length === 0 && vm.status === SendingStatus.success
                    ? <Empty/>
                    : <Table
                        columns={columns}
                        dataSource={data}
                        rowSelection={{...rowSelection}}
                        pagination={{
                            pageSize: 20,
                            position: ["bottomCenter"],
                            hideOnSinglePage: true,
                            onChange: page => changePage(page)
                        }}
                    />
            }

            <div className={'fixed bottom-5 right-5 sm:hidden'}>
                <Space direction={"vertical"}>
                    <Button shape={"circle"} type={"primary"} icon={<PlusOutlined/>} onClick={onAdd}/>
                    <Button shape={"circle"} danger onClick={handleInactive} disabled={selected.length <= 0} icon={<DeleteOutlined/>}/>
                </Space>
            </div>
        </MasterLayoutCtxWrapper>
    )
}

export default RoleScreen