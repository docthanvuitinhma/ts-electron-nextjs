import {Button, Card, Checkbox, Form, Input} from "antd";
import {PermissionAction} from "../../../recoil/permission/PermissionAction";
import {useEffect, useMemo, useState} from "react";
import {Validation} from "../../../core/Validation";
import {AddRoleAction} from "../../../recoil/role/AddRoleAction";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {SendingStatus} from "../../../const/Events";
import {TRoleV0} from "../../../models/RoleModel";
import {noti} from "../../functions/Notification";
import {RouteConfig} from "../../../config/RouteConfig";
import {useNavigate} from "react-router";

let timerName: NodeJS.Timeout
let timerDisplayName: NodeJS.Timeout

const AddRole = () => {
    const navigator = useNavigate()
    const [nameError, setNameError] = useState('')
    const [displayNameError, setDisplayNameError] = useState('')
    const {
        vm,
        onGetPermissions,
        onClearPermissionState
    } = PermissionAction()
    const {
        vmRole,
        onAddRole,
        onClearState
    } = AddRoleAction()

    useEffect(() => {
        onGetPermissions()
        return () => {
            onClearPermissionState()
            onClearState()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const options = useMemo(() => (
        vm.res?.map(item => (
            {
                label: item.displayName || '',
                value: item.id || ''
            }
        ))
    ), [vm])

    useEffect(() => {
        if (vmRole.status === SendingStatus.success) {
            noti('success', 'Thêm mới vai trò thành công')
            navigator(RouteConfig.ROLE)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vmRole.status])

    useEffect(() => {
        if (vmRole.status === SendingStatus.error && vmRole.error) {
            if (vmRole.error.hasOwnProperty('name')) {
                setNameError(vmRole.error.name)
            }
            if (vmRole.error.hasOwnProperty('display_name')) {
                setDisplayNameError(vmRole.error.display_name)
            }
        }
    }, [vmRole.status, vmRole.error])

    const debounceName = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerName)

            timerName = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceDisplayName = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerDisplayName)

            timerDisplayName = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach(e => {
            if (e.name === 'name') {
                const process = debounceName(() => {
                    if (e.errors) {
                        setNameError(e.errors.toString)
                    }
                })

                process()
            } else if (e.name === 'displayName') {
                const process = debounceDisplayName(() => {
                    if (e.errors) {
                        setDisplayNameError(e.errors.toString())
                    }
                })

                process()
            }
        })
    }

    const onFinish = (data: TRoleV0) => {
        onAddRole({
            name: data.name,
            display_name: data.displayName,
            permission_id: data.permission
        })
    }

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach(e => {
            switch (e.name[0]) {
                case 'name':
                    setNameError(e.errors[0])
                    break
                case 'displayName':
                    setDisplayNameError(e.errors[0])
                    break
            }
        })
    }

    return (
        <Card>
            <Form
                labelCol={{span: 8}}
                wrapperCol={{span: 14}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                onFieldsChange={onFieldsChange}
            >
                <Form.Item
                    name={'name'}
                    label={'Name'}
                    rules={[
                        {required: true},
                        {pattern: Validation.FORMAT_NAME}
                    ]}
                    validateStatus={nameError ? 'error' : ''}
                    help={nameError ? nameError : ''}
                >
                    <Input
                        placeholder={'Name'}
                        onKeyUp={() => setNameError('')}
                    />
                </Form.Item>

                <Form.Item
                    name={'displayName'}
                    label={'Display name'}
                    rules={[
                        {required: true},
                        {pattern: Validation.FORMAT_NAME}
                    ]}
                    validateStatus={displayNameError ? 'error' : ''}
                    help={displayNameError ? displayNameError : ''}
                >
                    <Input
                        // disabled
                        placeholder={'Display name'}
                        onKeyUp={() => setDisplayNameError('')}
                    />
                </Form.Item>

                <Form.Item
                    name={'permission'}
                    label={'Quyền'}
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng chọn quyền'
                        }
                    ]}
                >
                    <Checkbox.Group
                        className={'flex flex-col'}
                        options={
                            options
                                ? options
                                : []
                        }
                    />
                </Form.Item>

                <Form.Item className="flex justify-center">
                    <Button type={"primary"} htmlType={"submit"}>Lưu</Button>
                </Form.Item>
            </Form>
        </Card>
    )
}

export default AddRole