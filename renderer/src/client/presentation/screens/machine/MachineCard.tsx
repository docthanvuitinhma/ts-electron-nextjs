import React, {useState} from "react";
import {useNavigate} from "react-router";
import {Badge, Card, Descriptions, Dropdown, Menu, Modal, Space, Tag, Tooltip, Typography} from "antd";
import {ExclamationCircleOutlined, FileOutlined, MoreOutlined, PictureOutlined, VideoCameraOutlined} from "@ant-design/icons";
import {Utils} from "../../../core/Utils";
import {App} from "../../../const/App";
import {AiOutlineCloudUpload} from "react-icons/all";
import ScrollToTop from "react-scroll-to-top";

export const MachineCard = ({...props}) => {
    const [tab, setTab] = useState('tab1')
    const navigator = useNavigate()
    const tab1 = () => (

        <Descriptions
            labelStyle={{fontWeight: '500'}}
        >
            <Descriptions.Item label='Tên'>
                {props.item.name}
            </Descriptions.Item>

            <Descriptions.Item label='Drive'>
                <div className={'flex flex-wrap gap-2'}>
                    {
                        props.item.machineId
                            ? <Tag
                                className={'cursor-pointer m-0'}
                                color={"green"}
                            >
                                MID
                            </Tag>
                            : null
                    }
                    {
                        props.item.origId
                            ? <Tag
                                className={'cursor-pointer m-0'}
                                color={"cyan"}
                            >
                                OID
                            </Tag>
                            : null
                    }
                    {
                        props.item.fixId
                            ? <Tag
                                className={'cursor-pointer m-0'}
                                color={"magenta"}
                            >
                                FID
                            </Tag>
                            : null
                    }
                    {
                        props.item.videoId
                            ? <Tag
                                className={'cursor-pointer m-0'}
                                color={"blue"}
                            >
                                VID
                            </Tag>
                            : null
                    }
                </div>
            </Descriptions.Item>
            <Descriptions.Item label='HDD'>
                {props.item.hardDisk}
            </Descriptions.Item>
            {
                props.item.firstDateShot === "" && props.item.lastDateShot === ""
                    ? null
                    : <Descriptions.Item label='Last shot'>
                        {Utils.createdAtFormatted(props.item.firstDateShot, App.FormatToDate)}
                        {"=>"}
                        {Utils.createdAtFormatted(props.item.lastDateShot, App.FormatToDate)}
                    </Descriptions.Item>
            }

            <Descriptions.Item label='Chống rung'>
                <Badge
                    status={props.item.antiVibration === 1 ? 'processing' : 'default'}
                    text={props.item.antiVibration === 1 ? 'Có chống rung' : 'Không chống rung'}
                />
            </Descriptions.Item>

            <Descriptions.Item label='Trạng thái'>
                {
                    props.item.isRemove === 1
                        ? <Typography className={'text-red-500'}>Dữ liệu đã xoá</Typography>
                        : <Badge
                            status={props.item.status === 1 ? "processing" : "default"}
                            text={props.item.status === 1 ? "Hoạt động" : "Ngừng hoạt động"}
                        />
                }
            </Descriptions.Item>
            {
                props.item.isRemove === 1
                    ? null
                    : <Descriptions.Item label='Ngày tạo'>
                        <div className="italic">
                            {Utils.createdAtFormatted(props.item.createdAt, App.FormatToMoment)}
                        </div>
                    </Descriptions.Item>
            }
        </Descriptions>
    )

    const driveAction = (name: string, value: string, color: string, onlyCopy?: boolean) => (

        <div className={'flex flex-row gap-2'}>

            <Tag
                className={'cursor-pointer'}
                color={color}
            >
                {name}
            </Tag>

            <Typography.Paragraph
                className={'m-0'}
                ellipsis={true}
            >
                {value}
            </Typography.Paragraph>

            <Dropdown
                trigger={['click']}
                overlay={
                    <Menu
                        items={
                            onlyCopy
                                ? [
                                    {
                                        key: 'copy',
                                        label: (
                                            <Typography.Paragraph
                                                className={'m-0'}
                                                copyable={{
                                                    tooltips: ['', ''],
                                                    text: value,
                                                    icon: ['Sao chép', 'Sao chép']
                                                }}
                                            />
                                        )
                                    }
                                ]
                                : [
                                    {
                                        key: 'copy',
                                        label: (
                                            <Typography.Paragraph
                                                className={'m-0'}
                                                copyable={{
                                                    tooltips: ['', ''],
                                                    text: value,
                                                    icon: ['Sao chép', 'Sao chép']
                                                }}
                                            />
                                        ),
                                    },
                                    {
                                        key: 'open',
                                        label: <a
                                            className={'text-black no-underline'}
                                            href={`https://drive.google.com/drive/folders/${value}`}
                                            target={'_blank'}
                                            rel="noreferrer"
                                        >
                                            Mở
                                        </a>
                                    },
                                ]
                        }
                    />
                }

            >
                <MoreOutlined/>
            </Dropdown>
        </div>
    )

    const tab2 = () => (
        <Space direction={"vertical"} size="middle" style={{display: 'flex'}}>
            {
                props.item.machineId && parseInt(props.item.machineId) !== 0
                    ? driveAction('MID', props.item.machineId, 'green', true)
                    : null
            }
            {
                props.item.origId && parseInt(props.item.origId) !== 0
                    ? driveAction('OID', props.item.origId, 'cyan')
                    : null
            }
            {
                props.item.fixId && parseInt(props.item.fixId) !== 0
                    ? driveAction('FID', props.item.fixId, 'magenta')
                    : null
            }
            {
                props.item.videoId && parseInt(props.item.videoId) !== 0
                    ? driveAction('VID', props.item.videoId, 'blue')
                    : null
            }
        </Space>
    )

    const handleDelete = () => {
        Modal.confirm({
            title: 'Xóa dữ liệu máy timelapse?',
            icon: <ExclamationCircleOutlined/>,
            content: 'Bạn muốn xóa dữ liệu máy timelapse?',
            onOk() {
                return new Promise((resolve, reject) => {
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {
            },
        })
    }

    const handleDropdown = (key: string) => {
        switch (key) {
            case 'delete':
                console.log('detail')
                handleDelete()
                break
            case 'edit':
                console.log('edit')
                navigator(`/machine/edit/${props.item.id}`)
                break
        }
    }

    return (
        <Card
            onTabChange={key => setTab(key)}
            title={(
                <div className={'flex justify-between'}>

                    <Typography.Title
                        className={'w-4/5 !text-[#1a8983]'}
                        level={5}
                        ellipsis={true}
                    >
                        {props.item.name}
                    </Typography.Title>
                    {
                        props.item.isRemove === 0
                            ? <Dropdown
                                trigger={['click']}
                                overlay={
                                    <Menu
                                        onClick={e => handleDropdown(e.key)}
                                        items={[
                                            {
                                                key: 'edit',
                                                label: 'Chỉnh sửa',
                                            },
                                            {
                                                key: 'delete',
                                                label: <Typography className={'text-red-500'}>Xóa</Typography>,
                                            },
                                        ]}
                                    />
                                }
                            >
                                <MoreOutlined/>
                            </Dropdown>
                            : ''
                    }
                </div>
            )}
            tabList={[
                {
                    key: 'tab1',
                    tab: 'Thông số',
                },
                {
                    key: 'tab2',
                    tab: 'Drive',
                },
            ]}
            actions={
                props.item.isRemove === 0
                    ? [
                        <Tooltip title='Hình ảnh'>
                            <>
                                <PictureOutlined
                                    onClick={() => navigator(`/machine/image/${props.item.id}`)}
                                />
                            </>
                        </Tooltip>,

                        <Tooltip key={'video'} title='Video'>
                            <VideoCameraOutlined
                                key='video'
                                onClick={() => navigator(`/machine/video/${props.item.id}`)}
                            />
                        </Tooltip>,

                        <Tooltip key={'ftp'} title='FTP'>
                            <a href="http://222.252.10.203:6789/disks/hs01/files/1f2UW0HNrwWdz6uHvxoWex1kDH5C-IJk0/"><FileOutlined key='ftp'/></a>

                        </Tooltip>,
                        <Tooltip key={'upload'} title='Upload media'>
                            <AiOutlineCloudUpload
                                className="text-2xl"
                                key='upload'
                                onClick={() => navigator(`/machine/upload-media/${props.item.id}`)}/>
                        </Tooltip>,
                    ]
                    : []
            }
        >
            <div className={'flex flex-col'}>
                {
                    tab === 'tab1'
                        ? tab1()
                        : tab === 'tab2'
                            ? tab2()
                            : null
                }
            </div>
        </Card>
    )
}
