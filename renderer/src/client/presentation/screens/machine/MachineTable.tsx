import {useNavigate} from "react-router";
import {Button, Dropdown, Menu, Switch, Table, Tag, Tooltip, Typography} from "antd";
import {ColumnsType} from "antd/es/table";
import {EditOutlined, FileOutlined, MoreOutlined, PictureOutlined, VideoCameraOutlined} from "@ant-design/icons";
import {TableRowSelection} from "antd/es/table/interface";
import {Utils} from "../../../core/Utils";
import {App} from "../../../const/App";
import {MachineAction} from "../../../recoil/machine/MachineAction";
import CIcon from "@coreui/icons-react";
import {cilCloudUpload} from "@duonghainbs/coreui-icons/ts/free/cil-cloud-upload";
import {useTranslation} from "react-i18next";

type TProps = {
    selectRows: Function
}

interface DataType {
    key: string
    name: string
    drive: {
        MID?: string,
        OID?: string,
        FID?: string,
        VID?: string
    }
    hdd: string
    lastShot: string[]
    antiVibration: string
    status: string[]
    createdAt: string[]
    action: number[]
}

export const MachineTable = (props: TProps) => {
    const {t} = useTranslation();
    const navigator = useNavigate()
    const {
        vm
    } = MachineAction()
    const checkTime = (lastShot: string, timeRender: number) => {
        const current = new Date().getTime()
        const [day, month, year] = lastShot.split('-')
        const last = new Date(+year, parseInt(month) - 1, +day).getTime()

        return (current - last) > (timeRender * 60 * 24) ? 'red' : 'green'
    }

    const columns: ColumnsType<DataType> = [
        {
            title: `${t("machine.name")}`,
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Drive',
            dataIndex: 'drive',
            key: 'drive',
            render: (obj: { MID: string, OID: string, FID: string, VID: string }) => (
                <div className={'flex flex-wrap gap-2'}>
                    {
                        Object.entries(obj).map(([key, value]) => {

                            let color: string = ""
                            if (key === 'MID') {
                                color = 'green'
                            } else if (key === 'OID') {
                                color = 'cyan'
                            } else if (key === 'FID') {
                                color = 'blue'
                            } else if (key === 'VID') {
                                color = 'magenta'
                            }


                            if (value !== null && value !== undefined && parseInt(value) !== 0)

                                return (
                                    <Dropdown
                                        key={key}
                                        overlay={
                                            <Menu
                                                items={[
                                                    {
                                                        key: 1,
                                                        label: (
                                                            <Typography.Paragraph
                                                                className={'m-0'}
                                                                copyable={{
                                                                    tooltips: ['', ''],
                                                                    text: value,
                                                                    icon: ['Sao chép', 'Sao chép']
                                                                }}
                                                            />
                                                        )
                                                    },
                                                    key === 'MID'
                                                        ? null
                                                        : {
                                                            key: 2,
                                                            label: <a
                                                                className={'text-black no-underline'}
                                                                href={`https://drive.google.com/drive/folders/${value}`}
                                                                target={'_blank'} rel="noreferrer"
                                                            >
                                                                Mở
                                                            </a>
                                                        },

                                                ]}
                                            />}>

                                        <Tag className={'cursor-pointer m-0'} color={color}>{key}</Tag>

                                    </Dropdown>
                                )
                        })
                    }
                </div>
            )
        },
        {
            title: 'HDD',
            dataIndex: 'hdd',
            key: 'hdd',
            filters: [
                {text: 'hs01', value: 'hs01'},
                {text: 'hs02', value: 'hs02'},
                {text: 'hs03', value: 'hs03'},
                {text: 'hs04', value: 'hs04'},
            ],
            onFilter: (value, record) => record.hdd.includes(value.toString()),
        },
        {
            title: `${t("machine.last_shot")}`,
            dataIndex: 'lastShot',
            key: 'lastShot',
            render: value => {
                const timeRender = value[0] === 'week' ? 7 : 10
                const firstShot = Utils.createdAtFormatted(value[1], App.FormatToDate)
                const lastShot = Utils.createdAtFormatted(value[2], App.FormatToDate)

                const color = checkTime(lastShot, timeRender)

                return (
                    value[1] === "" || value[2] === ""
                        ? <div className="text-red-400">Không có dữ liệu</div>
                        : <Typography style={{color: `${color}`}}>
                            {firstShot} -{">"} {lastShot}
                        </Typography>
                )
            }
        },
        {
            title: `${t("machine.anti-vibration")}`,
            dataIndex: 'antiVibration',
            key: 'antiVibration',
            filters: [
                {text: 'Không chống rung', value: '0'},
                {text: 'Có chống rung', value: '1'}
            ],
            onFilter: (value, record) => record.antiVibration.includes(value.toString()),
            render: (value) => (
                <Switch defaultChecked={value === '1'}/>
            )
        },
        {
            title: `${t("machine.status")}`,
            dataIndex: 'status',
            key: 'status',
            filters: [
                {text: 'Hoạt động', value: '1'},
                {text: 'Ngừng hoạt động', value: '0'}
            ],
            onFilter: (value, record) => record.status.includes(value.toString()),
            render: (value) => (
                value[0] === '1'
                    ? <Typography className={'text-red-500'}>Dữ liệu đã xóa</Typography>
                    : <Switch defaultChecked={value[1] === '1'}/>
            )
        },
        {
            title: `${t("machine.created_at")}`,
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: value => (
                value[0] === '1'
                    ? ''
                    : Utils.createdAtFormatted(value[1], App.FormatToMoment)
            )
        },
        {
            title: `${t("machine.action")}`,
            dataIndex: 'action',
            key: 'action',
            render: (value) => (
                value[0] === 0
                    ? <div className={"flex flex-row"}>
                        <Tooltip title={`${t("machine.edit")}`}>
                            <Button
                                type={"primary"}
                                icon={<EditOutlined/>}
                                onClick={() => navigator(`/machine/edit/${value[1]}`)}
                            />
                        </Tooltip>

                        <Tooltip title={`${t("machine.more")}`}>
                            <Dropdown
                                trigger={['click']}
                                overlay={
                                    <Menu
                                        items={[
                                            {
                                                key: 'image',
                                                icon: <PictureOutlined/>,
                                                label: <a onClick={() => navigator(`/machine/image/${value[1]}`)}>{t("machine.picture")}</a>
                                            },
                                            {
                                                key: 'video',
                                                icon: <VideoCameraOutlined/>,
                                                label: <a onClick={() => navigator(`/machine/video/${value[1]}`)}>{t("machine.video")}</a>
                                            },
                                            {
                                                key: 'FTP',
                                                icon: <FileOutlined/>,
                                                label: <a href={'http://222.252.10.203:6789/disks/hs00/files/1hbV2Zr5XJhyTuhjFZ-SnpYHCw1l2tJp1/'}>FTP</a>
                                            },
                                            {
                                                key: 'upload',
                                                icon: <CIcon icon={cilCloudUpload}/>,
                                                label: <a onClick={() => navigator(`/machine/upload-media/${value[1]}`)}>{t("machine.upload_media")}</a>
                                            },
                                        ]}
                                    />
                                }
                            >
                                <Button type={"default"} icon={<MoreOutlined/>} className=" flex justify-center items-center mx-2 bg-white text-black"/>
                            </Dropdown>
                        </Tooltip>
                    </div>
                    : ""
            )
        },
    ]

    const data: DataType[] | undefined = vm.machines?.map(item => (
        {
            key: item.id ? item.id.toString() : '',
            name: item.name ?? '',
            drive: {
                MID: item.machineId,
                OID: item.origId,
                FID: item.fixId,
                VID: item.videoId
            },
            hdd: item.hardDisk ?? '',
            lastShot: [
                item.renderSchedule ?? '',
                item.firstDateShot ?? '',
                item.lastDateShot ?? ''
            ],
            antiVibration: item.antiVibration ? item.antiVibration.toString() : '',
            status: [
                item.isRemove ? item.isRemove.toString() : '',
                item.status ? item.status.toString() : ''
            ],
            createdAt: [
                item.isRemove ? item.isRemove.toString() : '',
                item.createdAt ?? ''
            ],
            action: [
                item.isRemove ?? 0,
                item.id ?? 0
            ]
        }
    ))

    const rowSelection: TableRowSelection<DataType> = {
        onChange: (selectedRowKeys, selectedRows) => {
            props.selectRows(selectedRowKeys)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
        getCheckboxProps: (record: DataType) => ({
            disabled: record.action[0] === 1,
            name: record.name,
        }),
    }

    return (
        <Table
            dataSource={data}
            columns={columns}
            rowSelection={{...rowSelection}}
            pagination={{pageSize: 20, position: []}}
        />
    )
}