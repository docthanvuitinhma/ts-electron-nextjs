import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router";
import {Button, Card, Divider, Form, Input, Radio, Select} from "antd";
import {Color} from "../../../const/Color";
import {AddMachineAction} from "../../../recoil/machine/AddMachineAction";
import {Validation} from "../../../core/Validation";
import {FieldData, ValidateErrorEntity} from "rc-field-form/lib/interface";
import {SendingStatus} from "../../../const/Events";
import {noti} from "../../functions/Notification";
import {RouteConfig} from "../../../config/RouteConfig";
import {useTranslation} from "react-i18next";


export type TMachine = {
    name: string
    machineId: string
    origId: string
    fixId: string
    videoId: string
    hdd?: string
    renderSchedule?: string
    api?: string
    resolution?: string
    sort: number
    antiVibration?: number
    status?: number
}

let timerName: NodeJS.Timeout
let timerMId: NodeJS.Timeout
let timerOId: NodeJS.Timeout
let timerFId: NodeJS.Timeout
let timerVId: NodeJS.Timeout
let timerSort: NodeJS.Timeout

const AddMachine = () => {
    const navigator = useNavigate()
    const {id} = useParams()
    const {
        vm,
        onGetMachine,
        onUpdateMachine,
        onAddMachine,
        onClearState
    } = AddMachineAction()
    const [nameError, setNameError] = useState('')
    const [machineIdError, setMachineIdError] = useState('')
    const [origIdError, setOrigIdError] = useState('')
    const [fixIdError, setFixIdError] = useState('')
    const [videoIdError, setVideoIdError] = useState('')
    const [sortError, setSortError] = useState('')
    const {t} = useTranslation();

    useEffect(() => {
        if (vm.addStatus === SendingStatus.success) {
            noti('success', 'Thêm mới máy timelapse thành công')
            navigator(RouteConfig.MACHINE)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.addStatus])

    useEffect(() => {
        if (vm.updateStatus === SendingStatus.success) {
            noti('success', 'Chỉnh sửa máy timelapse thành công')
            navigator(RouteConfig.MACHINE)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.updateStatus])
    const hddOptions = [
        {
            label: 'hs00',
            value: 'hs00'
        },
        {
            label: 'hs01',
            value: 'hs01'
        },
        {
            label: 'hs02',
            value: 'hs02'
        },
        {
            label: 'hs03',
            value: 'hs03'
        },
    ]

    const renderScheduleOptions = [
        {
            label: `${t("machine.week")}`,
            value: 'week'
        },
        {
            label: `${t("machine.month")}`,
            value: 'month'
        }
    ]

    const resolutionOptions = [
        {
            label: '---không---',
            value: ''
        },
        {
            label: '6000x4000',
            value: '6000x4000'
        },
        {
            label: '4000x6000',
            value: '4000x6000'
        },
    ]

    const antiVibrationOptions = [
        {
            label: `${t("machine.yes")}`,
            value: 1
        },
        {
            label: `${t("machine.no")}`,
            value: 0
        },
    ]

    const statusOptions = [
        {
            label: `${t("machine.active")}`,
            value: 1
        },
        {
            label: `${t("machine.disable")}`,
            value: 0
        },
    ]

    const debounceName = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerName)

            timerName = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceMId = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerMId)

            timerMId = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceOId = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerOId)

            timerOId = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceFId = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerFId)

            timerFId = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceVId = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerVId)

            timerVId = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    const debounceSort = (func: Function, timeOut = 1000) => {

        return (...arg: string[]) => {

            clearTimeout(timerSort)

            timerSort = setTimeout(() => {
                func.apply(this, arg)
            }, timeOut)
        }
    }

    useEffect(() => {
        id
            ? console.log('%cMount Screen: Edit Machine', Color.ConsoleInfo)
            : console.log('%cMount Screen: Add Machine', Color.ConsoleInfo)

        return () => {
            onClearState()
            id
                ? console.log('%cUnmount Screen: Edit Machine', Color.ConsoleInfo)
                : console.log('%cUnmount Screen: Add Machine', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        if (id) {
            console.log('edit', id)
            onGetMachine(id)

        } else {
            console.log('add')
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id])

    const onFinish = (data: TMachine) => {
        console.log(data)
        if (id) {
            onUpdateMachine(id, data)
        } else {
            onAddMachine(data)
        }
    }

    const onFieldsChange = (data: FieldData[]) => {
        data.forEach((e: any) => {
            if (e.name[0] === 'name') {
                const process = debounceName(() => {
                    if (e.errors[0]) {
                        setNameError(e.errors[0] ? e.errors[0] : '')
                    }
                })

                process()
            } else if (e.name[0] === 'machineId') {
                const process = debounceMId(() => {
                    if (e.errors[0]) {
                        setMachineIdError(e.errors[0] ? e.errors[0] : '')
                    }
                })

                process()
            } else if (e.name[0] === 'origId') {
                const process = debounceOId(() => {
                    if (e.errors[0]) {
                        setOrigIdError(e.errors[0] ? e.errors[0] : '')
                    }
                })

                process()
            } else if (e.name[0] === 'fixId') {
                const process = debounceFId(() => {
                    if (e.errors[0]) {
                        setFixIdError(e.errors[0] ? e.errors[0] : '')
                    }
                })

                process()
            } else if (e.name[0] === 'videoId') {
                const process = debounceVId(() => {
                    if (e.errors[0]) {
                        setVideoIdError(e.errors[0] ? e.errors[0] : '')
                    }
                })

                process()
            } else if (e.name[0] === 'sort') {
                const process = debounceSort(() => {
                    if (e.errors[0]) {
                        setSortError(e.errors[0] ? e.errors[0] : '')
                    }
                })

                process()
            }
        })
    }

    const onFinishFailed = (errorInfo: ValidateErrorEntity) => {
        errorInfo.errorFields.forEach(e => {
            switch (e.name[0]) {
                case 'name':
                    setNameError(e.errors[0])
                    break
                case 'machineId':
                    setMachineIdError(e.errors[0])
                    break
                case 'origId':
                    setOrigIdError(e.errors[0])
                    break
                case 'fixId':
                    setFixIdError(e.errors[0])
                    break
                case 'videoId':
                    setVideoIdError(e.errors[0])
                    break
                case 'sort':
                    setSortError(e.errors[0])
                    break
            }
        })
    }

    return (
        <Card>
            <Form
                labelCol={{span: 8}}
                wrapperCol={{span: 14}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                onFieldsChange={onFieldsChange}
                fields={
                    id
                        ? [
                            {
                                name: 'name',
                                value: vm.res?.name
                            },
                            {
                                name: 'machineId',
                                value: vm.res?.machineId
                            },
                            {
                                name: 'origId',
                                value: vm.res?.origId
                            },
                            {
                                name: 'fixId',
                                value: vm.res?.fixId
                            },
                            {
                                name: 'videoId',
                                value: vm.res?.videoId
                            },
                            {
                                name: 'hdd',
                                value: vm.res?.hardDisk
                            },
                            {
                                name: `${t("machine.video_quality")}`,
                                value: vm.res?.resolution
                            },
                            {
                                name: `${t("machine.schedule_render")}`,
                                value: vm.res?.renderSchedule
                            },
                            {
                                name: `${t("machine.sort")}`,
                                value: vm.res?.sortOrder
                            },
                            {
                                name: `${t("machine.anti-vibration")}`,
                                value: vm.res?.antiVibration
                            },
                            {
                                name: `${t("machine.status")}`,
                                value: vm.res?.status
                            },
                        ]
                        : []
                }
            >

                <Form.Item
                    label={"name"}
                    name='name'
                    rules={[
                        {
                            required: true,
                        },
                        {
                            min: 3,
                        },
                        {
                            max: 100,
                        }
                    ]}
                    validateStatus={nameError ? 'error' : ''}
                    help={nameError ? nameError : ''}
                >
                    <Input
                        placeholder='Tên máy'
                        onKeyUp={() => setNameError('')}
                    />
                </Form.Item>

                <Form.Item
                    label='Machine ID'
                    name='machineId'
                    rules={[
                        {required: true},
                        {pattern: Validation.FORMAT_ID}
                    ]}
                    validateStatus={machineIdError ? 'error' : ''}
                    help={machineIdError ? machineIdError : ''}
                >
                    <Input
                        placeholder='Machine ID'
                        onKeyUp={() => setMachineIdError('')}
                    />
                </Form.Item>

                <Form.Item
                    label='Orig ID'
                    name='origId'
                    rules={[
                        {required: true},
                        {pattern: Validation.FORMAT_ID}
                    ]}
                    validateStatus={origIdError ? 'error' : ''}
                    help={origIdError ? origIdError : ''}
                >
                    <Input
                        placeholder='Orig ID'
                        onKeyUp={() => setOrigIdError('')}
                    />
                </Form.Item>

                <Form.Item
                    label='Fix ID'
                    name='fixId'
                    rules={[
                        {pattern: Validation.FORMAT_ID}
                    ]}
                    validateStatus={fixIdError ? 'error' : ''}
                    help={fixIdError ? fixIdError : ''}
                >
                    <Input
                        placeholder='Fix ID'
                        onKeyUp={() => setFixIdError('')}
                    />
                </Form.Item>

                <Form.Item
                    label='Video ID'
                    name='videoId'
                    rules={[
                        {pattern: Validation.FORMAT_ID}
                    ]}
                    validateStatus={videoIdError ? 'error' : ''}
                    help={videoIdError ? videoIdError : ''}
                >
                    <Input
                        placeholder='Video ID'
                        onKeyUp={() => setVideoIdError('')}
                    />
                </Form.Item>


                <Form.Item
                    label={'HDD'}
                    name={'hdd'}
                    rules={[
                        {required: true}
                    ]}
                >
                    <Select
                        options={hddOptions}
                    />
                </Form.Item>

                <Divider/>

                <Form.Item
                    label={`${t("machine.render_schedule")}`}
                    name={'renderSchedule'}
                    rules={[
                        {required: true}
                    ]}
                    initialValue={'week'}
                >
                    <Select
                        options={renderScheduleOptions}
                    />
                </Form.Item>

                <Form.Item
                    label={`${t("machine.video_quality")}`}
                    name={'resolution'}
                >
                    <Select
                        options={resolutionOptions}
                    />
                </Form.Item>

                <Divider/>

                {/*<Form.Item*/}
                {/*    label={`${t("machine.sort")}`}*/}
                {/*    name={'sort'}*/}
                {/*    rules={[*/}
                {/*        {type: "number"}*/}
                {/*    ]}*/}
                {/*    validateStatus={sortError ? 'error' : ''}*/}
                {/*    help={sortError ? sortError : ''}*/}
                {/*>*/}
                {/*    <Input*/}
                {/*        onKeyUp={() => setSortError('')}*/}
                {/*    />*/}
                {/*</Form.Item>*/}

                <Form.Item
                    label={`${t("machine.anti-vibration")}`}
                    name={'antiVibration'}
                    initialValue={1}
                >
                    <Radio.Group
                        className={'flex flex-col'}
                        options={antiVibrationOptions}
                    />
                </Form.Item>

                <Form.Item
                    label={`${t("machine.status")}`}
                    name={'status'}
                    initialValue={1}
                >
                    <Radio.Group
                        className={'flex flex-col'}
                        options={statusOptions}
                    />
                </Form.Item>

                <Form.Item className="flex justify-center">
                    <Button type={"primary"} htmlType={"submit"}>Submit</Button>
                </Form.Item>
            </Form>
        </Card>
    )
}

export default AddMachine