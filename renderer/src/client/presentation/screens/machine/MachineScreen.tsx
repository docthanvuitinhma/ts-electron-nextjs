import React, {useEffect, useState} from "react";
import {MasterLayoutCtxWrapper} from "../../layouts/MasterLayout";
import {MachineTable} from "./MachineTable";
import {MachineCard} from "./MachineCard";
import {MachineAction} from "../../../recoil/machine/MachineAction";
import {Button, Drawer, Empty, Modal, Pagination, Space, Spin, Tooltip, Typography} from "antd";
import {SendingStatus} from "../../../const/Events";
import {useLocation} from "react-router-dom";
import {useNavigate} from "react-router";
import {Query} from "../../functions/Query";
import {Filter} from "../../widgets/Filter";
import {RouteConfig} from "../../../config/RouteConfig";
import {Key} from "antd/es/table/interface";
import {ExclamationCircleOutlined, FilterOutlined, PlusOutlined ,UpOutlined} from "@ant-design/icons";
import {cilRecycle} from "@duonghainbs/coreui-icons/ts/free/cil-recycle";
import CIcon from "@coreui/icons-react";
import {useTranslation} from "react-i18next";
import ScrollToTop from "react-scroll-to-top";

const MachineScreen = () => {
    const [loading, setLoading] = useState(false)
    const location = useLocation();
    const {
        vm,
        onGetListMachine
    } = MachineAction()
    useEffect(() => {
        setLoading(true)
        onGetListMachine(obj)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])
    const onScroll = () => {

    }
    useEffect(() => {
        if (vm.status === SendingStatus.success) {
            setLoading(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [vm.status])


    const [drawer, setDrawer] = useState(false)
    const [selected, setSelected] = useState<Key[]>([])
    const {t} = useTranslation()
    const navigator = useNavigate()
    const {queryToObject, dataToQuery} = Query()
    const obj = queryToObject()
    const changePaginate = (page?: number) => {
        return obj !== undefined
            ? {
                ...obj,
                page: page ? page : 1
            }
            : {page: page ? page : 1}
    }

    const handleFilter = (isVisible: boolean) => {
        setDrawer(isVisible)
    }

    const onAdd = () => {
        navigator(RouteConfig.ADD_MACHINE)
    }

    const onFilter = () => {
        setDrawer(!drawer)
    }

    const onDelete = () => {
        Modal.confirm({
            title: 'Xóa dữ liệu máy timelapse?',
            icon: <ExclamationCircleOutlined/>,
            content: 'Bạn muốn xóa dữ liệu máy timelapse?',
            onOk() {
                return new Promise((resolve, reject) => {
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {
            },
        })
    }

    const tool = () => (
        <div className={'block'}>
            <Space>
                <Tooltip title={`${t("machine.new")}`}>
                    <Button
                        shape={"circle"}
                        onClick={onAdd}
                        className="!bg-green-500 !text-white ml-4 mr-2 hover:bg-green-300"
                        icon={<PlusOutlined/>}
                    >

                    </Button>
                </Tooltip>
                <Tooltip title={`${t("machine.filter")}`}>
                    <Button
                        shape={"circle"}
                        onClick={onFilter}
                        icon={<FilterOutlined/>}
                        className="bg-cyan-500 text-white mr-2 hover:bg-cyan-200 "
                    />
                </Tooltip>
                <Tooltip title={`${t("machine.delete")}`}>
                    <Button
                        shape={"circle"}
                        className={'block hover:cursor-not-allowed !outline-none text-white'}
                        disabled={selected.length <= 0}
                        onClick={onDelete}
                        icon={<CIcon icon={cilRecycle}/>}
                    />
                </Tooltip>
            </Space>
        </div>
    )

    const handleSelectRows = (key: []) => {
        setSelected(key)
    }

    return (
        <MasterLayoutCtxWrapper
            tool={tool()}
        >
            {
                vm.machines?.length === 0 && vm.status === SendingStatus.success
                    ? <Empty/>
                    : <Spin
                        spinning={loading}
                        size="large"
                    >
                        <div className={'hidden lg:block'}>
                            <MachineTable selectRows={handleSelectRows}/>
                        </div>

                        <div className={'lg:hidden'}>
                            <Space
                                direction={"vertical"}
                                size="middle"
                                style={{display: 'flex'}}
                            >
                                {
                                    vm.machines?.map(item => {

                                        return <MachineCard item={item} key={item.id}/>
                                    })
                                }
                            </Space>
                        </div>
                    </Spin>
            }

            <Drawer
                className={'z-[1030]'}
                title={
                    <Typography.Title level={4}>
                        Nhập vào đây để bắt đầu lọc
                    </Typography.Title>
                }
                open={drawer}
                onClose={() => setDrawer(false)}
            >
                <Filter parentCallback={handleFilter}/>
            </Drawer>

            <div className={'flex justify-center my-3'}>
                <Pagination
                    onChange={page => {
                        setLoading(true)
                        const q = changePaginate(page)
                        navigator(`${location.pathname}?${dataToQuery(q)}`)
                    }}
                    responsive
                    pageSize={20}
                    current={vm.meta?.currentPage}
                    total={vm.meta?.totalCount}
                    hideOnSinglePage
                />
            </div>
            <ScrollToTop smooth />
        </MasterLayoutCtxWrapper>

    )
}

export default MachineScreen
