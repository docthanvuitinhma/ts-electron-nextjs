import React from 'react';
import {setLng} from "../../locales/i18n";
import {useTranslation} from "react-i18next";
import {Select, message} from 'antd';
import Image from "next/image";
import {useConfigContext} from "../contexts/ConfigContext";


const ChangLanguage: React.FC = () => {
    const [lang, setLang] = useConfigContext()
    const {t} = useTranslation()
    const handleChangeLanguage = (value: number) => {
        setLang({
            lang: value
        })
        if (value === 0) {
            setLng('vi')
            message.success(t("message.success")).then()
        } else if (value === 1) {
            setLng('en')
            message.success(t("message.success")).then()
        } else if (value === 2) {
            setLng('zh')
            message.success(t("message.success")).then()
        }
    };
    return (
        <Select
            defaultValue={0}
            style={{
                width: 150,
                float: "right",
            }}
            onChange={handleChangeLanguage}
        >
            <Select.Option value={0}>
                <div className={"flex flex-direction-row"}>
                    <Image
                        src={"/_next/static/media/vn.9ec4ca4d.svg"}
                        alt={"flagVi"}
                        width={24}
                        height={24}
                    />
                    <span className={"ml-3"}>Tiếng Việt </span>
                </div>
            </Select.Option>

            <Select.Option value={1}>
                <div className={"flex flex-direction-row"}>
                    <Image
                        src={"/_next/static/media/gb-eng.513dcf1b.svg"}
                        alt={"flagEn"}
                        width={24}
                        height={24}
                    />
                    <span className={"ml-3"}>English </span>
                </div>
            </Select.Option>

            <Select.Option value={2}>
                <div className={"flex flex-direction-row"}>
                    <Image
                        src={"/_next/static/media/cn.e1b166eb.svg"}
                        alt={"flagZh"}
                        width={24}
                        height={24}
                    />
                    <span className={"ml-3"}>China</span>
                </div>
            </Select.Option>
        </Select>
    )
};

export default ChangLanguage
