import React from 'react'
import {CFooter} from '@coreui/react'

const AppFooter = () => {
    return (
        <CFooter>
            <div>
                <a href="https://selfie.autotimelapse.com/" target="_blank" rel="noopener noreferrer">
                    ATL Selfie
                </a>
                <span className="ms-1">&copy; 2022</span>
            </div>
            <div className="ms-auto">
                <span className="me-1">Powered by</span>
                <a href="https://iig.vn" target="_blank" rel="noopener noreferrer">
                    I&I Group
                </a>
            </div>
        </CFooter>
    )
}

export default React.memo(AppFooter)
