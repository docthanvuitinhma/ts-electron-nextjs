import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Uploader from "./Uploader";
import {useParams} from "react-router";
import {noti} from "../../../functions/Notification";
import { SyncOutlined} from "@ant-design/icons";
import {Button} from "antd"
import {StoreConfig} from "../../../../config/StoreConfig";
import {AxiosRequestConfig} from "axios";

const useStyles = makeStyles(theme => ({
    root: {
        "& > *": {
            margin: theme.spacing(1)
        }
    },
    input: {
        display: "none"
    }
}));


export default function UploadButton() {
    const classes = useStyles();
    const {id} = useParams()
    const [showProgress, setShowProgress] = useState(false)
    const uploadButton = <Button type ="primary" className="bg-[#009b90]" disabled={showProgress === true}/>;
    const [value, setValue] = useState(0)
    const config: AxiosRequestConfig = {
        headers: {
            'Authorization': '*'
        },
        withCredentials: false,
    }
    config.headers!.Accept = "application/json";
    const storeConfig = StoreConfig.getInstance()
    if (storeConfig.token && storeConfig.token.length > 0) {
        config.headers!.Authorization = `Bearer ${storeConfig.token}`;
    }

    return (
        <div className={classes.root}>
            <Uploader
                dropTargetID="myDropTarget"
                maxFileSize={102400000000}
                chunkSize={1024 * 1024}
                headerObject={config.headers}
                fileAddedMessage="Started!"
                completedMessage="Complete!"
                service={`http://222.252.10.203:8000/api/v1/timelapse/machine/${id}/upload-video`}
                textLabel="Upload files"
                previousText="Drop to upload your media:"
                disableDragAndDrop={false}
                onFileSuccess={() => {
                    noti("success", 'Tải lên video thành công')
                    setShowProgress(false)
                }}
                onFileAdded={(file, resumable) => {
                    setShowProgress(true)
                    console.log(file)
                    resumable.upload()
                    resumable.isUploading()
                    resumable.progress()
                }}
                maxFiles={undefined}
                startButton={false}
                pauseButton={true}
                cancelButton={false}
                uploadButton={uploadButton}
                onProgress={(file, resumable) => {
                    resumable.upload()
                    resumable.isUploading()
                    resumable.progress()
                    const progress = Math.floor((resumable.progress() * 100))
                    setValue(progress)
                }}
            />

            {
                showProgress &&
                <div>
                    <Button  className="flex justify-between items-center text-white bg-orange-800">
                        <SyncOutlined spin/>
                        <div className="ml-3">Chờ tý bạn eii, Hà Lội không vội đc đâu</div>
                    </Button>
                        <div className="progress mt-8 h-[25px]">
                            <div className="progress-bar progress-bar-striped bg-red-700 progress-bar-animated h-full" style={{width: value + "%"}}>{value}%</div>
                        </div>
                </div>

            }

        </div>
    );
}
