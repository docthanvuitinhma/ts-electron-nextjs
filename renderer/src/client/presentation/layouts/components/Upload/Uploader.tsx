import React, {useState, useEffect} from "react";
import Resumablejs from "resumablejs";
import IUploaderProps from "./IUploaderProps";
import Button from "@material-ui/core/Button";
import {StoreConfig} from "../../../../config/StoreConfig";
import {EDFile} from "../../../../core/encrypt/EDFile";
import moment from "moment/moment";
import {AxiosRequestConfig} from "axios";
import {TiUploadOutline} from "react-icons/ti";

interface IFileList {
    files: Resumable.ResumableFile[];
}

interface ILocalState {
    fileList: IFileList;
    progressBar: number;
    messageStatus: string;
    isPaused: boolean;
    isUploading: boolean;
    resumable?: Resumable.Resumable;
}

const MAX_FILE_SIZE = 10240000;
const CHUNK_SIZE = 1024 * 1024;
const config: AxiosRequestConfig = {
    headers: {
        'Authorization': '*'
    },
    withCredentials: false,
}
const storeConfig = StoreConfig.getInstance()

    const et = EDFile.encrypt({
        t: storeConfig.token,
        e: moment().add(30, 'seconds').unix()
    })

    config.headers!.Authorization = `Bearer ${et}`;

const defaultProps = {
    maxFiles: undefined,
    uploaderID: "default-resumable-uploader",
    dropTargetID: "drop-target",
    // filetypes: ["jpg", "JPG", "png", "PNG","m4v", "mp4", "MP4", "mkv","avi","wmv","mpg","flv","3gp"],
    // fileAccept: ["*/"],
    maxFileSize: MAX_FILE_SIZE,
    showFileList: true,
    onUploadErrorCallback: (file: Resumable.ResumableFile, message: string) => {
        console.log("error", file, message);
    },
    onFileRemoved: function (file: Resumable.ResumableFile) {
        return file;
    },
    onCancelUpload: function () {
        return true;
    },
    onPauseUpload: function () {
        return true;
    },
    onResumeUpload: function () {
        return true;
    },
    onStartUpload: function () {
        return true;
    },
    onProgress: (file: Resumable.ResumableFile, resumable: Resumablejs) => {
    },
    disableDragAndDrop: false,
    chunkSize: CHUNK_SIZE,
    simultaneousUploads: 1,
    fileParameterName: "file",
    maxFilesErrorCallback: null,
    cancelButton: false,
    pause: false,
    startButton: null,
    pauseButton: null,
    previousText: "",
    headers :"",
    headerObject: "",
    withCredentials: false,
    forceChunkSize: false
};

export const Uploader = (props: IUploaderProps) => {
    const [localState, setLocalState] = useState<ILocalState>({
        progressBar: 0,
        messageStatus: "",
        fileList: {files: []},
        isPaused: false,
        isUploading: true,
        resumable: undefined
    });

    let dropZone: null | HTMLElement = null;
    let uploader: null | HTMLElement = null;

    const mergedProps = {
        ...defaultProps,
        ...props
    };

    useEffect(() => {
        const config: Resumable.ConfigurationHash = {
            target: mergedProps.service,
            query: mergedProps.query || {},
            fileType: mergedProps.filetypes,
            maxFiles: mergedProps.maxFiles,
            maxFileSize: (mergedProps.maxFileSize as unknown) as boolean, // yay for hacks!
            fileTypeErrorCallback: (file: Resumable.ResumableFile, errorCount) => {
                if (typeof mergedProps.onFileAddedError === "function") {
                    mergedProps.onFileAddedError(file, errorCount);
                }
            },
            maxFileSizeErrorCallback: (file: Resumable.ResumableFile, errorCount) => {
                if (typeof mergedProps.onMaxFileSizeErrorCallback === "function") {
                    mergedProps.onMaxFileSizeErrorCallback(file, errorCount);
                }
            },
            testMethod: mergedProps.testMethod || "POST",
            testChunks: mergedProps.testChunks || false,
            headers: mergedProps.headerObject || {},
            withCredentials: mergedProps.withCredentials || false,
            chunkSize: mergedProps.chunkSize,
            simultaneousUploads: mergedProps.simultaneousUploads,
            fileParameterName: mergedProps.fileParameterName,
            generateUniqueIdentifier: mergedProps.generateUniqueIdentifier,
            forceChunkSize: mergedProps.forceChunkSize
        };

        const resumable = new Resumablejs(config);

        if (typeof mergedProps.maxFilesErrorCallback === "function") {
            resumable.opts.maxFilesErrorCallback = mergedProps.maxFilesErrorCallback;
        }

        // @ts-ignore
        resumable.assignBrowse(uploader, false);

        //Enable or Disable DragAnd Drop
        if (mergedProps.disableDragAndDrop === false) {
            // @ts-ignore
            resumable.assignDrop(dropZone);
        }

        resumable.on("fileAdded", (file: Resumable.ResumableFile, event: Event) => {
            setLocalState({
                ...localState,
                messageStatus: mergedProps.fileAddedMessage || " Starting upload! "
            });

            if (typeof mergedProps.onFileAdded === "function") {
                mergedProps.onFileAdded(file, resumable);
            } else {
                resumable.upload();
            }
        });

        resumable.on(
            "fileSuccess",
            (file: Resumable.ResumableFile, fileServer: any) => {
                if (mergedProps.fileNameServer) {
                    let objectServer = JSON.parse(fileServer);
                    file.fileName = objectServer[mergedProps.fileNameServer];
                } else {
                    file.fileName = fileServer;
                }

                let currentFiles = localState.fileList.files;
                currentFiles.push(file);

                setLocalState({
                    ...localState,
                    fileList: {files: currentFiles},
                    messageStatus:
                        mergedProps.completedMessage + file.fileName || fileServer
                });

                if (typeof mergedProps.onFileSuccess === "function") {
                    mergedProps.onFileSuccess(file, fileServer);
                }
            }
        );

        resumable.on("progress", (file: Resumable.ResumableFile) => {
            setLocalState({
                ...localState,
                isUploading: resumable?.isUploading()
            });
            if (typeof mergedProps.onProgress === "function") {
                mergedProps.onProgress(file, resumable);
            } else {
                resumable.progress();
            }
            const progress = resumable.progress() * 100;
            if (progress < 100) {
                setLocalState({
                    ...localState,
                    messageStatus: progress + "%",
                    progressBar: progress
                });
            } else {
                setTimeout(() => {
                    setLocalState({
                        ...localState,
                        progressBar: 0
                    });
                }, 1000);
            }
        });

        resumable.on(
            "fileError",
            (file: Resumable.ResumableFile, message: string) => {
                mergedProps.onUploadErrorCallback(file, message);
            }
        );

        setLocalState({
            ...localState,
            resumable
        });
    }, [props]);

    return (
        <div id={mergedProps.dropTargetID} ref={node => (dropZone = node)}>
            <input
                ref={node => (uploader = node)}
                type="file"
                id={mergedProps.uploaderID}
                className="btn"
                name={mergedProps.uploaderID + "-upload"}
                // accept={mergedProps.fileAccept || "*"}
                disabled={mergedProps.disableInput || false}
                style={{display: "none"}}
            />

            <label
                className={
                    mergedProps.disableInput
                        ? "btn file-upload disabled"
                        : "btn file-upload"
                }
                htmlFor={mergedProps.uploaderID}
            >
                <Button
                    variant="contained"
                    className="!bg-[#009b90] text-white"
                    component="span"
                >
                  <TiUploadOutline className="mr-2"/>Upload Media
                </Button>
            </label>
        </div>

    );
};

export default Uploader;
