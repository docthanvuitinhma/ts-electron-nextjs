import React from 'react'
import CIcon from '@coreui/icons-react'
import {cilAvTimer, cilCalendar, cilListRich, cilShieldAlt, cilSpeedometer, cilUser, cilUserPlus} from '@duonghainbs/coreui-icons'
import {CNavGroupItems, CNavItem, CNavTitle} from '@coreui/react'
import {RouteConfig} from "../../../config/RouteConfig";


type _TNavChild = {
    component: any
    name: string
    to?: string
    badge?: {
        color: string,
        text: string
    }
    href?: string,
    items?: _TNavChild[]
}

export type TNav = _TNavChild & {
    icon?: any
}

const _nav: TNav[] = [

    {
        component: CNavItem,
        name: 'Dashboard',
        to: RouteConfig.DASHBOARD,
        icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon"/>,
        badge: {
            color: 'info',
            text: 'NEW',
        },
    },
    {
        component: CNavItem,
        name: "Tiến trình",
        to: RouteConfig.SCHEDULE,
        icon: <CIcon icon={cilCalendar} customClassName="nav-icon"/>,
        badge: {
            color: 'info',
            text: 'NEW',
        },
    },
    {
        component: CNavTitle,
        name: 'Machine',
    },
    {
        component: CNavItem,
        name: 'Máy Timelapse',
        to: RouteConfig.MACHINE,
        icon: <CIcon icon={cilAvTimer} customClassName="nav-icon"/>,
    },
    {
        component: CNavTitle,
        name: 'Quản trị',
    },
    {
        component: CNavItem,
        name: 'Danh sách người dùng',
        to: RouteConfig.USER,
        icon: <CIcon icon={cilListRich} customClassName="nav-icon"/>
    },
    {
        component: CNavItem,
        name: 'Thêm người dùng',
        to: '/add-user',
        icon: <CIcon icon={cilUserPlus} customClassName="nav-icon"/>
    },
    {
        component: CNavItem,
        name: 'Vai trò',
        to: '/role',
        icon: <CIcon icon={cilShieldAlt} customClassName="nav-icon"/>
    },

]

export default _nav
