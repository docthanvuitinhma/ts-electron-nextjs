import UploadButton from "./Upload/UploaderButton";

const UploadMedia = () => {
    return(
        <fieldset>
            <p className="text-xl">
                You can upload any video or multimedia content you want.</p>
            <UploadButton/>
           </fieldset>
    )
}
export default UploadMedia