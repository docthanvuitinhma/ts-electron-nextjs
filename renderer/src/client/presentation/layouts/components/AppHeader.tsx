import React, {ReactNode} from 'react'
import {NavLink} from 'react-router-dom'
import {CContainer, CHeader, CHeaderBrand, CHeaderDivider, CHeaderNav, CHeaderToggler, CNavItem, CNavLink} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {cilBell, cilEnvelopeOpen, cilMenu} from '@duonghainbs/coreui-icons'
import {logoHeader} from '../../../assets/brand/brand'
import {ThemeAction} from "../../../recoil/theme/ThemeAction";
import AppBreadcrumb from "./AppBreadcrumb";
import AppHeaderDropdown from "./AppHeaderDropdown";
import ChangeLanguage from "../../language/ChangeLanguage";

type _Props = {
    tool?: ReactNode
}

const AppHeader = (props: _Props) => {
    const {
        vm,
        onSetState
    } = ThemeAction()

    return (
        <CHeader position="sticky" className="mb-4 !z-[99]">
            <CContainer fluid>
                <CHeaderToggler
                    className="ps-1"
                    onClick={() => onSetState({sidebarShow: !vm.sidebarShow})}
                >
                    <CIcon icon={cilMenu} size="lg"/>
                </CHeaderToggler>
                {/*@ts-ignore*/}
                <CHeaderBrand className="mx-auto d-md-none" to="/">
                    <CIcon icon={logoHeader} height={28}/>
                </CHeaderBrand>
                <CHeaderNav className="d-none d-md-flex me-auto">
                    <CNavItem>
                        <CNavLink to="/dashboard" component={NavLink}>
                            Dashboard
                        </CNavLink>
                    </CNavItem>
                    <CNavItem>
                        <CNavLink href="/user">Users</CNavLink>
                    </CNavItem>
                    <CNavItem>
                        <CNavLink href="#">Settings</CNavLink>
                    </CNavItem>
                </CHeaderNav>
                <CHeaderNav className="flex justify-center items-center">
                    <CNavItem className="mx-4">
                        <ChangeLanguage/>
                    </CNavItem>
                    <CNavItem>
                        <CNavLink href="#">
                            <CIcon icon={cilBell} size="lg"/>
                        </CNavLink>
                    </CNavItem>
                    <CNavItem>
                        <CNavLink href="#">
                            <CIcon icon={cilEnvelopeOpen} size="lg"/>
                        </CNavLink>
                    </CNavItem>
                </CHeaderNav>
                <CHeaderNav className="ml-3">
                    <AppHeaderDropdown/>
                </CHeaderNav>
            </CContainer>
            <CHeaderDivider/>
            <CContainer fluid>
                <AppBreadcrumb/>
                {
                    !!props.tool && (
                        <div>
                            {props.tool}
                        </div>
                    )
                }
            </CContainer>
        </CHeader>
    )
}

export default AppHeader
