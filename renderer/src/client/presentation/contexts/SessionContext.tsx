import React, {createContext, useContext, useEffect, useRef, useState} from "react";
import {initialSession, SessionModel} from "../../models/SessionModel";
import {UserModel} from "../../models/UserModel";
import {TNextAppData} from "../../const/Types";
import {useInjection} from "inversify-react";
import {StoreConfig} from "../../config/StoreConfig";
import {EDLocal} from "../../core/encrypt/EDLocal";
import {Utils} from "../../core/Utils";
import {MeAction} from "../../recoil/account/me/MeAction";

export const SessionContext = createContext<[SessionModel, (session: SessionModel) => void]>([initialSession, () => {
    //
}])

export const useSessionContext = () => useContext(SessionContext)

export const SessionContextProvider: React.FC<{ data?: TNextAppData, children?: any }> = (props) => {
    const [sessionState, setSessionState] = useState(initialSession);
    const defaultSessionContext: [SessionModel, typeof setSessionState] = [sessionState, setSessionState];
    const storeConfig = useInjection(StoreConfig)

    const {
        onSetMe
    } = MeAction()

    const devR18Ref = useRef<boolean>()

    useEffect(() => {
        if (devR18Ref.current) {
            return;
        }
        devR18Ref.current = true;

        const cls = EDLocal.getLocalStore('user');
        console.log(cls)

        if (cls) {
            const user = new UserModel(cls)
            initialSession.user = user;
            initialSession.isAuthenticated = true;
            initialSession.redirectPath = "/";

            storeConfig.token = initialSession.user.token;

            // StoreConfig.token = initialSession.user.token ?? '';
            if (!EDLocal.getCookie('user') && user.token) {
                EDLocal.setCookie('user', Utils.rmObjectByValue({token: user.token}))
            }

            onSetMe(user)
        }

        if (props.data && props.data.header?.token) {
            initialSession.user = new UserModel({
                token: props.data.header.token
            })
            initialSession.isAuthenticated = true
            initialSession.redirectPath = "/"

            storeConfig.token = props.data.header.token
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <SessionContext.Provider value={defaultSessionContext}>
            {props.children}
        </SessionContext.Provider>
    )
}
