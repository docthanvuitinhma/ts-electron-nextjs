import {useLocation} from "react-router";

export const Query = () => {

    const location = useLocation()

    const queryToObject = () => {
        const convert = location.search.replace('?', "").split('&').map(item => item.split('='))
        const obj = {page: 1}

        convert.forEach((item) => {
            if (item[0] !== '') {
                // @ts-ignore
                obj[item[0]] = item[1]
            }
        })

        return obj
    }

    const dataToQuery = (data: any) => {
        let query = ''
        console.log(data)

        Object.entries(data).forEach((value) => {
            if (value[1] !== undefined && value[1] !== "") {
                if (query !== '') {
                    query += '&'
                }
                query += `${value[0]}=${value[1]}`
            }
        })

        console.log('query', query)

        return query
    }
    const SelectFilter = (data: any) => {
        let query = ''
        console.log("Query data",data)

            if (query !== undefined ) {
                if (query !== '') {
                    query += data
                }
                else return ''
            }
        console.log("Query Queue",query)
        return query
    }

    return {
        queryToObject,
        dataToQuery,
        SelectFilter
    }
}