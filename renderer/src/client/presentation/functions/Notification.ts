import {notification} from "antd";

export type NotificationType = 'success' | 'info' | 'warning' | 'error'

export const noti = (data: NotificationType, description: string) => {
    notification[data]({
        message: 'Thông báo',
        description: description,
        placement: "bottomRight",
    });
}