import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {Alert, Modal, Result, Spin} from "antd";
import {Color} from "../../const/Color";
import {LogoutAction} from "../../recoil/account/logout/LogoutAction";
import {SendingStatus} from "../../const/Events";
import ErrorItemFC from "./ErrorItemFC";

export function LogoutWidget(props: {
    onClose: Function
}) {
    const {t} = useTranslation()

    const {
        vm,
        onLogout,
        onClearState
    } = LogoutAction()

    useEffect(() => {
        console.log('%cMount Screen: LogoutScreen', Color.ConsoleInfo);

        onLogout()

        return () => {
            onClearState()

            console.log('%cUnmount Screen: LogoutScreen', Color.ConsoleInfo)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [isModalVisible, setIsModalVisible] = useState(true)

    const handleClose = () => {
        setIsModalVisible(false)
        props.onClose()
    }
    console.log(vm.status)

    const isError = vm.status === SendingStatus.error

    return (
        <Modal
            open={isModalVisible}
            onCancel={handleClose}
            cancelText={t('button.close')}
            footer={null}
            closable={isError}
            destroyOnClose={true}
            keyboard={false}
            maskClosable={false}
        >
            <ErrorItemFC status={vm.status} onClose={handleClose}>
                {
                    (isError && vm.error && vm.error.hasOwnProperty('warning')) && (
                        <Alert className={"mt-6"} message={vm.error['warning']} type="error" showIcon/>
                    )
                }
                {
                    vm.status === SendingStatus.loading
                        ? <div className={"text-center"}>
                            <Spin tip={t('text.signingOut')}/>
                        </div>
                        :
                        vm.status === SendingStatus.success && (
                            <Result
                                status="success"
                                title={t('text.logoutSuccess')}
                            />
                        )
                }
            </ErrorItemFC>
        </Modal>
    )
}
