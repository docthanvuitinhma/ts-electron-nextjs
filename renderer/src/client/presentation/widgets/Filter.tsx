import {Button, DatePicker, Form, Input, Select} from "antd";
import React from "react";
import {useLocation, useNavigate} from "react-router";
import {Query} from "../functions/Query";

type TProps = {
    parentCallback: Function
}

export const Filter = (props: TProps) => {
    const [form] = Form.useForm();
    const location = useLocation()
    const navigator = useNavigate()
    const {dataToQuery} = Query()

    const onFinish = (data: any) => {
        props.parentCallback(false)
        navigator(`${location.pathname}?${dataToQuery(data)}`)
    }

    const onCancel = () => {
        form.resetFields()

        props.parentCallback(false)
        navigator(`${location.pathname}`)
    }

    return (
        <div className={"flex flex-col"}>

            <Form
                form={form}
                labelCol={{span: 8}}
                wrapperCol={{span: 14}}
                layout="horizontal"
                onFinish={onFinish}
            >

                <Form.Item
                    name={'name'}
                    label={'Tên máy'}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    name={'machine_id'}
                    label={'Machine ID'}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    name={'orig_id'}
                    label={'Orig ID'}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    name={'hdd'}
                    label={'HDD'}
                >
                    <Select
                        options={[
                            {
                                label: '',
                                value: ''
                            },
                            {
                                label: 'hs00',
                                value: 'hs00'
                            },
                            {
                                label: 'hs01',
                                value: 'hs01'
                            },
                            {
                                label: 'hs02',
                                value: 'hs02'
                            },
                            {
                                label: 'hs03',
                                value: 'hs03'
                            },
                        ]}
                    />
                </Form.Item>

                <Form.Item
                    name={'anti_vibration'}
                    label={'Chống rung'}
                >
                    <Select
                        options={[
                            {
                                label: '',
                                value: ''
                            },
                            {
                                label: 'Vô hiệu hóa',
                                value: '0'
                            },
                            {
                                label: 'Kích hoạt',
                                value: '1'
                            },
                        ]}
                    />
                </Form.Item>

                <Form.Item
                    name={'status'}
                    label={'Trạng thái'}
                >
                    <Select
                        options={[
                            {
                                label: '',
                                value: ''
                            },
                            {
                                label: 'Vô hiệu hóa',
                                value: '0'
                            },
                            {
                                label: 'Kích hoạt',
                                value: '1'
                            },
                        ]}
                    />
                </Form.Item>

                <Form.Item name={'is_remove'} label={'Xóa lưu'}>
                    <Select
                        options={[
                            {
                                label: '',
                                value: ''
                            },
                            {
                                label: 'Chưa xóa',
                                value: '0'
                            },
                            {
                                label: 'Đã xóa',
                                value: '1'
                            },
                        ]}
                    />
                </Form.Item>

                <Form.Item name={'created_at'} label={'Ngày thêm'}>
                    <DatePicker/>
                </Form.Item>

                <div className={'flex justify-between'}>

                    <Button
                        onClick={onCancel}
                        danger
                    >
                        Hủy
                    </Button>
                    <Button
                        type={"primary"}
                        htmlType={"submit"}
                    >
                        Lọc
                    </Button>
                </div>
            </Form>
        </div>
    )
}