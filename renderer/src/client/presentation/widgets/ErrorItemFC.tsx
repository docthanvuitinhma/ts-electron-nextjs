import {useTranslation} from "react-i18next";
import {SendingStatus} from "../../const/Events";
import {Button, Col, Modal, notification, Result, Row, Space, Typography} from "antd";
import {RouteConfig} from "../../config/RouteConfig";
import {CloseCircleOutlined, FrownOutlined} from "@ant-design/icons";
import {includes} from "lodash";
import {useNavigate} from "react-router";
import {memo, useEffect, useState} from "react";
import {useLocation} from "react-router-dom";

const ErrorItemFC = (props: {
    onClose?: Function,
    children: any;
    status?: SendingStatus,
    typeView?: 'default' | 'modal' | 'notification',
    onRefresh?: Function | null
}) => {
    const {t} = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const [render, setRender] = useState(props.children)

    useEffect(() => {
        if (!includes([SendingStatus.disConnect, SendingStatus.serverError, SendingStatus.unauthorized, SendingStatus.unauthorized], props.status)) {
            if (props.status === SendingStatus.maintenance) {
                Modal.error({
                    title: t('text.maintenance'),
                    content: (
                        <div className={"text-justify"}>
                            {t('message.maintenance')}
                        </div>
                    ),
                    okText: (
                        <>
                            <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                        </>
                    ),
                    okType: "default"
                })
            }
        }

        if (props.typeView === 'modal') {
            switch (props.status) {
                case SendingStatus.disConnect:
                    Modal.error({
                        title: t('text.noInternet'),
                        content: (
                            <>
                                <p className={"mb-0"}>
                                    <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.checkModem')}
                                </p>
                                <p className={"mb-0"}>
                                    <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.reconnectWifi')}
                                </p>
                            </>
                        ),
                        okText: (
                            <>
                                <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                            </>
                        ),
                        okType: "default"
                    })

                    break;
                case SendingStatus.serverError:
                    Modal.error({
                        title: t('text.error'),
                        content: (
                            <div className={"text-justify"}>
                                {t('message.confirmError')}
                            </div>
                        ),
                        okText: (
                            <>
                                <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                            </>
                        ),
                        okType: "default"
                    })

                    break;
                case SendingStatus.unauthorized:
                    Modal.error({
                        title: t('text.loginAgain'),
                        content: (
                            <div className={"text-justify"}>
                                {t('text.contentUnAuthorized')}
                            </div>
                        ),
                        okText: (
                            <>
                                <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('button.close')}
                            </>
                        ),
                        okType: "default"
                    })

                    break;
            }
        } else if (props.typeView === 'notification') {
            switch (props.status) {
                case SendingStatus.disConnect:
                    notification.error({
                        message: t('text.noInternet'),
                        description: (
                            <>
                                <p className={"mb-0"}>
                                    <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.checkModem')}
                                </p>
                                <p className={"mb-0"}>
                                    <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.reconnectWifi')}
                                </p>
                            </>
                        )
                    })
                    break
                case SendingStatus.serverError:
                    notification.error({
                        message: t('text.error'),
                        description: (
                            <div className={"text-justify"}>{t('message.confirmError')}</div>
                        ),
                    })
                    break
                case SendingStatus.unauthorized:
                    notification.error({
                        message: t('text.loginAgain'),
                        description: (
                            <div className={"text-justify"}>{t('text.contentUnAuthorized')}</div>
                        ),
                    })
                    break
            }
        } else {

            setRender(<Row justify={"center"}>
                <Col xs={24} sm={24} md={24} lg={18} xl={12} xxl={12}>
                    {
                        props.status === SendingStatus.disConnect
                            ? <Result
                                status="error"
                                title={t('text.noInternet')}
                            >
                                <div className="desc">
                                    <Typography.Paragraph>
                                        <Typography.Text
                                            strong
                                            style={{
                                                fontSize: 16,
                                            }}
                                        >
                                            {t('text.tryStepToConnect')}
                                        </Typography.Text>
                                    </Typography.Paragraph>
                                    <Typography.Paragraph>
                                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.checkModem')}
                                    </Typography.Paragraph>
                                    <Typography.Paragraph>
                                        <CloseCircleOutlined className="site-result-demo-error-icon mr-2"/>{t('text.reconnectWifi')}
                                    </Typography.Paragraph>
                                </div>
                            </Result>
                            : props.status === SendingStatus.serverError
                                ? <div>
                                    <Result
                                        status="500"
                                        title={t('text.error')}
                                        subTitle={t('message.confirmError')}
                                        extra={
                                            <Space>
                                                <Button
                                                    type="primary"
                                                    onClick={() => {
                                                        if (location.pathname === RouteConfig.DASHBOARD && props.onClose) {
                                                            props.onClose()
                                                        } else {
                                                            navigate(RouteConfig.DASHBOARD)
                                                        }
                                                    }
                                                    }
                                                >
                                                    {t('text.home')}
                                                </Button>
                                                <Button
                                                    type="primary"
                                                    onClick={() => props.onRefresh != null ? props.onRefresh() : null}
                                                >
                                                    {t('button.tryAgain')}
                                                </Button>
                                            </Space>
                                        }
                                    />
                                </div>
                                :
                                props.status === SendingStatus.unauthorized && (
                                    <Result
                                        status={"error"}
                                        icon={<FrownOutlined/>}
                                        title={t('text.noData')}
                                    />
                                )
                    }
                </Col>
            </Row>)
        }

    }, [location.pathname, navigate, props, props.status, t])

    return render;
}

export default memo(ErrorItemFC)
