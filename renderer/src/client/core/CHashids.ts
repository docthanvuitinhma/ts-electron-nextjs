import {App} from "../const/App";
import Hashids from 'hashids';
import {Utils} from "./Utils";

type _TConnect = {
    salt: string;
    length: number;
}

type NumberLike = number

export class CHashids {
    protected static C: _TConnect;

    static _init() {
        if (typeof this.C === "undefined") {
            this.C = (App.Hashids.connections as any)[App.Hashids.default];
        }
    }

    static connection(name: string) {
        const hc: any = App.Hashids.connections;

        if (hc.hasOwnProperty(name)) {
            this.C = hc[name];
        } else {
            this.C = hc[App.Hashids.default];
        }

        return this;
    }

    static encode(number: any): string {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.encode(number);
    }

    static decode(id: string): NumberLike[] {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.decode(id) as NumberLike[];
    }

    static decodeGetFirst(id: string): number | null {
        const hashids = new Hashids(this.C.salt, this.C.length);
        const result = hashids.decode(id);

        return result.length > 0 ? result[0] as number : null;
    }

    static encodeHex(value: string | bigint): string {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.encodeHex(value);
    }

    static decodeHex(id: string): string {
        const hashids = new Hashids(this.C.salt, this.C.length);

        return hashids.decodeHex(id);
    }

    static encodeObjectID(...value: string[]): string {
        return this.encodeHex(value.join(''))
    }

    static decodeObjectID(id: string): string[] | string {
        const decode = this.decodeHex(id);
        const size = 24;

        if (decode.length === 0) {
            return "";
        }

        if (decode.length === size) {
            return decode;
        }

        const value = [];

        for (let i = 1; i <= decode.length / size; i++) {
            value.push(decode.slice((i - 1) * size, size * i))
        }

        return value;
    }

    static encodeUUID(...value: string[]): string {
        value = value.map(item => Utils.UUIDToString(item))

        return this.encodeHex(value.join(''))
    }

    static decodeUUID(id: string): string[] | string {
        try {
            const decode = this.decodeHex(id);
            const size = 32;

            if (decode.length === 0) {
                return "";
            }

            if (decode.length === size) {
                return Utils.stringToUUID(decode);
            }

            const value = [];

            for (let i = 1; i <= decode.length / size; i++) {
                value.push(Utils.stringToUUID(decode.slice((i - 1) * size, size * i)))
            }

            return value;
        } catch (_) {
            return '';
        }
    }
}

CHashids._init();
