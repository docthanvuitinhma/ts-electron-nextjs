export  class Validation {
    private constructor() {
    }

    static readonly FORMAT_USERNAME = /^(?=[a-zA-Z0-9._]{3,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/
    static readonly FORMAT_PASSWORD = /^(?=.*\S).{6,}$/
    static readonly FORMAT_NAME = /^[a-zA-Z]{3,}(?: [a-zA-Z]+){0,2}$/;
    // static readonly FORMAT_NAME = /[^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;
    // static readonly FORMAT_NAME = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]+$/;
    static readonly FORMAT_EMAIL = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    static readonly FORMAT_PHONE = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
    static readonly FORMAT_ADDRESS = /^(?=.*\w,-_ ).{2,}$/


    static readonly FORMAT_ID = /^\S*$/
}