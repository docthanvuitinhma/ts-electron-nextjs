import {AES, enc} from "crypto-js";

export class EDFile {
    protected static key = process.env.NEXT_PUBLIC_ED_FILE_SECRET ?? '13245678913245678913245678913245'
    protected static iv = process.env.NEXT_PUBLIC_ED_FILE_IV ?? '13245678913245678913245678913245'

    public static encrypt(value: any, key?: string): string {
        if (typeof value === "object") {
            value = JSON.stringify(value);
        }

        const encrypted = AES.encrypt(value, enc.Utf8.parse(key ?? this.key), {
            iv: enc.Hex.parse(this.iv)
        })

        let data = encrypted.toString();

        if (data.indexOf('/') === 0) {
            data = '!' + data.substring(1, data.length)
        }

        return data;
    }

    public static decrypt(value: any, key?: string): any {
        if (value.indexOf('!') === 0) {
            value = '/' + value.substring(1, value.length)
        }

        const decrypted = AES.decrypt(value, enc.Utf8.parse(key ?? this.key), {
            iv: enc.Hex.parse(this.iv)
        })

        let data = decrypted.toString(enc.Utf8);

        try {
            data = JSON.parse(data);
        } catch (_) {
            //
        }
        return data;
    }
}
