import {CryptoJSAesJson, TOptions} from "./CryptoJSAesJson";

export class EDData {
    protected static opts: TOptions = {
        secret: process.env.NEXT_PUBLIC_ED_DATA_SECRET,
    }

    public static encrypt(value: any) {
        const driver = new CryptoJSAesJson(this.opts)

        return driver.encrypt(value)
    }

    public static decrypt(value: string | object): any {
        if (typeof value == "object") {
            value = JSON.stringify(value)
        }

        const driver = new CryptoJSAesJson(this.opts)

        return driver.decrypt(value)
    }
}
