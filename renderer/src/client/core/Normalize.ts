export class Normalize {
    static initJsonObject<T>(json: Record<string, any>, key: string | string[], fn?: (obj: Record<string, any>) => T): T | undefined {
        key = this._key(json, key);

        if (key instanceof Array) {
            return undefined;
        }

        return json.hasOwnProperty(key) && json[key] instanceof Object && Object.keys(json[key]).length > 0
            ? fn === undefined
                ? json[key]
                : fn(json[key])
            : undefined
    }

    static initJsonArray<T>(json: Record<string, any>, key: string | string[], fn?: (arr: any[]) => T): T | undefined {
        key = this._key(json, key);

        if (key instanceof Array) {
            return undefined;
        }

        return json.hasOwnProperty(key) && json[key] instanceof Array && json[key].length > 0
            ? fn === undefined
                ? json[key]
                : fn(json[key])
            : undefined
    }

    static initJsonString(json: Record<string, any>, key: string | string[], fn?: Function): string | undefined {
        key = this._key(json, key);

        if (key instanceof Array) {
            return undefined;
        }

        return json.hasOwnProperty(key) && (typeof json[key] === "string" || typeof json[key] === "number") && json[key].toString().length > 0
            ? fn === undefined
                ? typeof json[key] === "number"
                    ? json[key].toString()
                    : json[key]
                : fn(json[key])
            : undefined
    }

    static initJsonNumber(json: Record<string, any>, key: string | string[], fn?: Function): number | undefined {
        key = this._key(json, key);

        if (key instanceof Array) {
            return undefined;
        }

        return json.hasOwnProperty(key) && (typeof json[key] === "string" || typeof json[key] === "number") && json[key].toString().length > 0
            ? fn === undefined
                ? typeof json[key] === "string"
                    ? json[key].indexOf('.') !== -1 ? parseFloat(json[key]) : parseInt(json[key])
                    : json[key]
                : fn(json[key])
            : undefined
    }

    static initJsonBool(json: Record<string, any>, key: string | string[], fn?: Function): boolean | undefined {
        key = this._key(json, key);

        if (key instanceof Array) {
            return undefined;
        }

        return json.hasOwnProperty(key) && (typeof json[key] === "string" || typeof json[key] === "number" || typeof json[key] === "boolean") && json[key].toString().length > 0
            ? fn === undefined
                ? typeof json[key] === "string"
                    ? json[key] === '1'
                    : typeof json[key] === "number"
                        ? json[key] === 1
                        : json[key]
                : fn(json[key])
            : undefined
    }

    protected static _key(json: Record<string, any>, key: string | string[]) {
        if (key instanceof Array) {
            for (const item of key) {
                if (json.hasOwnProperty(item)) {
                    return item;
                }
            }
        }

        return key;
    }
}
