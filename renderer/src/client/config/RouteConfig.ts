import React, {lazy} from "react";





interface IRcc {
    path: string;
    name?: string;
    component?: React.ComponentType<any>;
    protect?: boolean;
    replace?: boolean;
    children?: IRcc[];
}

export interface IRco extends IRcc {
    routes?: IRcc[];
}

const MachineScreen = lazy(() => import("../presentation/screens/machine/MachineScreen"))
const ImageScreen = lazy(() => import("../presentation/screens/image/ImageScreen"))
const VideoScreen = lazy(() => import("../presentation/screens/video/VideoScreen"))
const RenderVideo = lazy(() => import("../presentation/screens/video/RenderVideo"))
const AddMachine = lazy(() => import( "../presentation/screens/machine/AddMachine"))
const AddUser = lazy(() => import( "../presentation/screens/user/AddUser"))
const EditUser = lazy(() => import( "../presentation/screens/user/EditUser"))
const UserScreen = lazy(() => import( "../presentation/screens/user/UserScreen"))
const PermissionScreen = lazy(() => import( "../presentation/screens/permission/PermissionScreen"))
const AddPermission = lazy(() => import( "../presentation/screens/permission/AddPermission"))
const RoleScreen = lazy(() => import( "../presentation/screens/role/RoleScreen"))
const AddRole = lazy(() => import( "../presentation/screens/role/AddRole"))
const Dashboard = lazy(() => import("../presentation/screens/dashboard/DashBoard"));
const Schedule = lazy(() => import("../presentation/screens/schedule/Schedule"));
const UploadMedia = lazy(() => import("../presentation/layouts/components/UploadMedia"));


export class RouteConfig {
    static readonly DASHBOARD: string = "/";
    static readonly MACHINE: string = "/machine";
    static readonly USER: string = "/user";
    static readonly ROLE: string = "/role";
    static readonly PERMISSION: string = "/permission";
    static readonly IMAGE: string = "/machine/image/:id";
    static readonly VIDEO: string = "/machine/video/:id";
    static readonly RENDER_VIDEO: string = "/machine/image/render-video";
    static readonly ADD_MACHINE: string = "/machine/add";
    static readonly EDIT_MACHINE: string = "/machine/edit/:id";
    static readonly ADD_USER: string = "/add-user";
    static readonly EDIT_USER: string = "/user/edit/:id";
    static readonly ADD_ROLE: string = "/role/add";
    static readonly ADD_PERMISSION: string = "/permission/add";
    static readonly SCHEDULE: string = "/schedule";
    static readonly UPLOAD_MEDIA:string = "/machine/upload-media/:id"
    static readonly LOGIN: string = "/login";

    static readonly NOT_FOUND: string = "*";

    static masterRoutes: IRco[] = [

        {
            name: 'machine',
            path: RouteConfig.MACHINE,
            component: MachineScreen,
            protect: true
        },
        {
            name: 'image',
            path: RouteConfig.IMAGE,
            component: ImageScreen,
            protect: true
        },
        {
            name: 'video',
            path: RouteConfig.VIDEO,
            component: VideoScreen,
            protect: true
        },
        {
            name: 'add machine',
            path: RouteConfig.ADD_MACHINE,
            component: AddMachine,
            protect: true
        },
        {
            name: 'edit machine',
            path: RouteConfig.EDIT_MACHINE,
            component: AddMachine,
            protect: true
        },
        {
            name: 'user',
            path: RouteConfig.USER,
            component: UserScreen,
            protect: true
        },
        {
            name: 'Add user',
            path: RouteConfig.ADD_USER,
            component: AddUser,
            protect: true
        },
        {
            name: 'Edit user',
            path: RouteConfig.EDIT_USER,
            component: EditUser,
            protect: true
        },
        {
            name: 'permission',
            path: RouteConfig.PERMISSION,
            component: PermissionScreen,
            protect: true
        },
        {
            name: 'add permission',
            path: RouteConfig.ADD_PERMISSION,
            component: AddPermission,
            protect: true
        },
        {
            name: 'role',
            path: RouteConfig.ROLE,
            component: RoleScreen,
            protect: true
        },
        {
            name: 'add role',
            path: RouteConfig.ADD_ROLE,
            component: AddRole,
            protect: true
        },
        {
            name: 'DashBoard',
            path: RouteConfig.DASHBOARD,
            component: Dashboard,
            protect: true
        },
        {
            name: 'Schedule',
            path: RouteConfig.SCHEDULE,
            component: Schedule,
            protect: true
        },
        {
            name: 'Upload Media',
            path: RouteConfig.UPLOAD_MEDIA,
            component: UploadMedia,
            protect: true
        },
        {
            name: 'Render Video',
            path: RouteConfig.RENDER_VIDEO,
            component: RenderVideo,
            protect: true
        },

    ]
}
