import {injectable} from "inversify";
import {container} from "./InversifyConfig";

@injectable()
export class StoreConfig {
    public static getInstance(): StoreConfig {
        return container.get(StoreConfig)
    }

    token?: string
}
