import {Normalize} from "../core/Normalize";

export class UserDetail {
    id: string
    name: string
    email:string
    role: role[]
    constructor(data:any) {
        this.id = Normalize.initJsonString(data, 'id') ?? '';
        this.name = Normalize.initJsonString(data, 'name') ?? "" ;
        this.email = Normalize.initJsonString(data,'email') ?? "";
        this.role = Normalize.initJsonArray(data, 'role') ?? []
    }
}
export class role {
    id: number
    name:string
    constructor(data:any) {
        this.id = Normalize.initJsonNumber(data, 'id') ?? 0
        this.name = Normalize.initJsonString(data, 'name') ?? ""
    }
}