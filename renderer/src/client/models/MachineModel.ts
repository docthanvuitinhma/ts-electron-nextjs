import {Normalize} from "../core/Normalize";
import {App} from "../const/App";
import moment from "moment/moment";

export class MachineModel {
    id?: number
    name?: string
    machineId?: string
    origId?: string
    fixId?: string
    videoId?: string
    hardDisk?: string
    renderSchedule?: string
    resolution?: string
    antiVibration?: number
    sortOrder?: number
    status?: number
    firstDateShot: string
    lastDateShot: string
    isRemove?: number
    createdAt?: string

    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatISOFromMoment).format(format)

    constructor(data: any) {
        this.id = Normalize.initJsonNumber(data, 'id')
        this.name = Normalize.initJsonString(data, 'name')
        this.machineId = Normalize.initJsonString(data, 'machine_id')
        this.origId = Normalize.initJsonString(data, 'orig_id')
        this.fixId = Normalize.initJsonString(data, 'fix_id')
        this.videoId = Normalize.initJsonString(data, 'video_id')
        this.hardDisk = Normalize.initJsonString(data, 'hard_disk')
        this.renderSchedule = Normalize.initJsonString(data, 'render_schedule')
        this.resolution = Normalize.initJsonString(data, 'resolution')
        this.antiVibration = Normalize.initJsonNumber(data, 'anti_vibration')
        this.sortOrder = Normalize.initJsonNumber(data, 'sort_order')
        this.status = Normalize.initJsonNumber(data, 'status')
        this.firstDateShot = Normalize.initJsonString(data, 'first_date_shot') ?? ""
        this.lastDateShot = Normalize.initJsonString(data, 'last_date_shot') ?? ""
        this.isRemove = Normalize.initJsonNumber(data, 'is_remove')
        this.createdAt = Normalize.initJsonString(data, 'created_at')
    }
}