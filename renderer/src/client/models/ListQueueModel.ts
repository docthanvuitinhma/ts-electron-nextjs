import {Normalize} from "../core/Normalize";

export class ListQueueModel {
    name : string
    total:string
    constructor(data:any) {
        this.name = Normalize.initJsonString(data,'name') ?? "";
        this.total = Normalize.initJsonString(data, 'total') ?? ""
    }
}