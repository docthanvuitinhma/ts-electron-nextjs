import {Normalize} from "../core/Normalize";

export class DiskUsageModel {
    name: string
    total: number
    used: number
    available: number
    percent: number
    unit: string

    constructor(data: any) {
        this.name = Normalize.initJsonString(data,'name') ?? "";
        this.total = Normalize.initJsonNumber(data,'total') ?? 0;
        this.used = Normalize.initJsonNumber(data,'used') ?? 0;
        this.available = Normalize.initJsonNumber(data,'available') ?? 0;
        this.percent = Normalize.initJsonNumber(data, 'percent') ?? 0;
        this.unit = Normalize.initJsonString(data,'unit') ?? "";
    }
}