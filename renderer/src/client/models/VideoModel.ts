import {Normalize} from "../core/Normalize";
import {App} from "../const/App";
import moment from "moment";

export type TVideo = {
    name: string
    size: string
    url: string
}
export class VideoModel {
    name: string
    size: string
    url: string
    createdAt: string

    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatISOFromMoment).format(format)

    constructor(data: any) {
        this.name = Normalize.initJsonString(data, 'name') ?? ""
        this.size = Normalize.initJsonString(data, 'size') ?? ""
        this.url = Normalize.initJsonString(data, 'url') ??""
        this.createdAt = Normalize.initJsonString(data, 'created_at') ?? ""
    }
}