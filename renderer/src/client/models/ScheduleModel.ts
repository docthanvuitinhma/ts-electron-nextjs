import {Normalize} from "../core/Normalize";

export class ScheduleModel {
    status: string
    name :string
    queue : string
    attempt:string
    duration: string
    started: string
    error :string
constructor(data:any) {
        this.status = Normalize.initJsonString(data,'status') ?? "";
        this.name = Normalize.initJsonString(data,'name') ?? "" ;
        this.queue = Normalize.initJsonString(data,'queue') ?? "";
        this.attempt = Normalize.initJsonString(data,'attempt') ?? "";
        this.duration = Normalize.initJsonString(data,'duration') ?? "";
        this.started = Normalize.initJsonString(data,'started')?? "";
        this.error = Normalize.initJsonString(data,'error') ?? "";
}
}