import {Normalize} from "../core/Normalize";

export type TPermission = {
    key: string
    name: string
}

export type TRoleV0 = {
    name: string
    displayName: string
    permission: number[]
}

export class RoleModel {
    id?: number
    name?: string
    displayName?: string
    permission?: TPermission[]

    constructor(data: any) {
        this.id = Normalize.initJsonNumber(data, 'id')
        this.name = Normalize.initJsonString(data, 'name')
        this.displayName = Normalize.initJsonString(data, 'display_name')
        this.permission = Normalize.initJsonArray(data, 'permission')
    }
}