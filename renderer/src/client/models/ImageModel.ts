import {Normalize} from "../core/Normalize";
import {App} from "../const/App";
import moment from "moment";

type TInfo = {
    air_temp: number
    air_humid: number
}

export class ImageModel {
    name?: string
    order?: number
    info?: TInfo
    link?: string
    size?: string
    dateShot?: string

    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.dateShot, App.FormatISOFromMoment).format(format)

    constructor(data: any) {
        this.name = Normalize.initJsonString(data, 'name')
        this.order = Normalize.initJsonNumber(data, 'order')
        this.info = Normalize.initJsonObject(data, 'info')
        this.link = Normalize.initJsonString(data, 'link')
        this.size = Normalize.initJsonString(data, 'size')
        this.dateShot = Normalize.initJsonString(data, 'date_shot')
    }
}