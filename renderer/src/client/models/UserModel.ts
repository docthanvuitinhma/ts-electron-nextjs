import {App} from "../const/App";
import moment from "moment";
import {Normalize} from "../core/Normalize";
import {Model} from "./Model";

export type TLoginVO = {
    email: string
    password: string
}

export type TUserV0 = {
    name?: string
    email?: string
    password?: string
    role?: []
}

export type TMeInfoV0 = {
    name?: string
    email?: string
    telephone?: string
    address?: string
}

export type TMePasswordVO = {
    password: string
    current: string
}

export type TMeImageV0 = {
    image: File
    type: string
}

// export class UserModel extends Model {
export class UserModel {
    userId: number
    username: string
    email: string
    name: string
    role: Role[]
    image?: string
    coverPhoto?: string
    phone?: string
    address?: string
    createdAt: string
    token?: string
    accessToken?: AccessTokenModel

    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatISOFromMoment).format(format)

    constructor(data: Record<string, any>) {
        // super()

        this.userId = Normalize.initJsonNumber(data, 'id') ?? 0
        this.username = Normalize.initJsonString(data, 'username') ?? ""
        this.email = Normalize.initJsonString(data, 'email') ?? ""
        this.name = Normalize.initJsonString(data, 'name') ?? ""
        this.image = Normalize.initJsonString(data, 'image') ?? ""
        this.coverPhoto = Normalize.initJsonString(data, 'coverPhoto') ?? ""
        this.phone = Normalize.initJsonString(data, 'phone')?? ""
        this.address = Normalize.initJsonString(data, 'address') ?? ""
        this.createdAt = Normalize.initJsonString(data, 'created_at') ?? ""
        this.token = Normalize.initJsonString(data, 'token')?? ""
        // this.role = Normalize.initJsonArray(data, 'role', (arr:any[])=>arr.map(item => new Role(item))) ?? []
        this.role = Normalize.initJsonArray(data, 'role') ?? []
        this.accessToken = Normalize.initJsonObject(data, 'accessToken', v => new AccessTokenModel(v))
    }
}
export class Role {
    id: number
    name: string
    constructor(data: any) {
        this.id = Normalize.initJsonNumber(data, 'id') ?? 0
        this.name = Normalize.initJsonString(data, 'name') ?? ""
    }
}
export class AccessTokenModel extends Model {
    token?: string
    abilities?: string[]
    expiresAt?: string
    updatedAt?: string
    createdAt?: string

    expiresAtFormatted = (format: string = App.FormatToMoment): string => moment(this.expiresAt, App.FormatISOFromMoment).format(format)
    updatedAtFormatted = (format: string = App.FormatToMoment): string => moment(this.updatedAt, App.FormatISOFromMoment).format(format)
    createdAtFormatted = (format: string = App.FormatToMoment): string => moment(this.createdAt, App.FormatISOFromMoment).format(format)

    constructor(data: Record<string, any>) {
        super()

        this.token = Normalize.initJsonString(data, 'token')
        this.abilities = Normalize.initJsonArray(data, 'abilities')
        this.expiresAt = Normalize.initJsonString(data, 'expiresAt')
        this.updatedAt = Normalize.initJsonString(data, 'updatedAt')
        this.createdAt = Normalize.initJsonString(data, 'createdAt')
    }
}

