import {Normalize} from "../core/Normalize";

export class ApiResModel {
    success: boolean;
    data?: any
    error?: any;
    items: any[];
    code: number;
    meta?: PaginateMetaModel;

    constructor(data: Record<string, any>) {
        this.success = data['success'];
        this.data = Normalize.initJsonObject(data, 'data')
        this.error = Normalize.initJsonObject(data, 'error')
        this.items = Normalize.initJsonArray(data, 'items') ?? []
        this.code = Normalize.initJsonNumber(data, 'code') ?? 1
        if (data.hasOwnProperty('_meta') && data["_meta"] !== null) this.meta = new PaginateMetaModel(data['_meta']);
    }
}

// export class AesApiResModel {
//     success: boolean;
//     data?: any;
//     error?: any;
//     items?: any[];
//     code: number;
//
//     constructor(data: Record<string, any>) {
//         this.success = data['success'];
//         this.data = Normalize.initJsonObject(data, 'data')
//         this.error = Normalize.initJsonObject(data, 'error')
//         this.items = Normalize.initJsonArray(data, 'items')
//         this.code = Normalize.initJsonNumber(data, 'code') ?? 1
//     }
// }

export class PaginateMetaModel {
    totalCount: number;
    pageCount: number;
    currentPage: number;
    nextPage: number;
    perPage: number;

    constructor(data: any) {
        this.totalCount = data['total_count'];
        this.pageCount = data['page_count'];
        this.currentPage = data['current_page'] | data['page'];
        this.nextPage = data['next_page'];
        this.perPage = data['per_page'];
    }
}