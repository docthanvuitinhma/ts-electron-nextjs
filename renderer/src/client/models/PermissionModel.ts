import {Normalize} from "../core/Normalize";

export type TPermissionV0 = {
    key: string
    displayName: string
}

export class PermissionModel {
    id?: number
    key?: string
    displayName?: string

    constructor(data: any) {
        this.id = Normalize.initJsonNumber(data, 'id')
        this.key = Normalize.initJsonString(data, 'key')
        this.displayName = Normalize.initJsonString(data, 'display_name')
    }
}