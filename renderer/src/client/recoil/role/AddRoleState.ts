import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {KeyAddRole} from "../KeyRecoil";
import {RoleModel} from "../../models/RoleModel";

type TAddRoleState = {
    status: SendingStatus
    res?: RoleModel
    error?: Record<string, any>
}

export const initialState: TAddRoleState = {
    status: SendingStatus.idle
}

export const AddRoleState = atom({
    key: KeyAddRole,
    default: initialState
})