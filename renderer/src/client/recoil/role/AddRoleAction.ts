import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {AddRoleState} from "./AddRoleState";
import {SendingStatus} from "../../const/Events";
import {setErrorHandled} from "../CmAction";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";

export const AddRoleAction = () => {
    const vm = useRecoilValue(AddRoleState)
    const [state, setState] = useRecoilState(AddRoleState)
    const resetState = useResetRecoilState(AddRoleState)
    const apiService = useInjection(ApiService)

    const onAddRole = (data: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .createRole(data)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        status: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vmRole: vm,
        onAddRole,
        onClearState: resetState
    }
}