import {atom} from "recoil";
import {RoleModel} from "../../models/RoleModel";
import {KeyRole} from "../KeyRecoil";
import {SendingStatus} from "../../const/Events";

type TRoleState = {
    status: SendingStatus
    error?: Record<string, any>
    res?: RoleModel[]
}

export const initialState: TRoleState = {
    status: SendingStatus.idle
}

export const RoleState = atom({
    key: KeyRole,
    default: initialState
})