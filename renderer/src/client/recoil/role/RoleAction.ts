import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {RoleState} from "./RoleState";
import {ApiService} from "../../repositories/ApiService";
import {setErrorHandled} from "../CmAction";
import {RoleModel} from "../../models/RoleModel";
import {SendingStatus} from "../../const/Events";
import {useInjection} from "inversify-react";

export const RoleAction = () => {
    const vm = useRecoilValue(RoleState)
    const [state, setState] = useRecoilState(RoleState)
    const resetState = useResetRecoilState(RoleState)
    const apiService = useInjection(ApiService)

    const onGetRoles = (page?: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .getAllRole(page ?? {})
            .then(r => {

                if (r.success) {

                    if (r.items) {

                        const roles = r.items.map(item => (
                            new RoleModel(item)
                        ))

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: roles
                        })

                    } else {

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: []
                        })

                    }
                } else {

                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })

                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onGetRoles,
        onClearRoleState: resetState
    }
}