import {atom} from "recoil";
import {KeyTheme} from "../KeyRecoil";

export type TThemeState = {
    sidebarShow?: boolean,
    sidebarUnfoldable?: boolean
}

export const initialState: TThemeState = {
    sidebarShow: true
}

export const ThemeState = atom<TThemeState>({
    key: KeyTheme,
    default: initialState
})
