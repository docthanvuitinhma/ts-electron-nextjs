import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {ThemeState, TThemeState} from "./ThemeState";

export const ThemeAction = () => {
    const [state, setState] = useRecoilState(ThemeState)
    const resetState = useResetRecoilState(ThemeState)
    const vm = useRecoilValue(ThemeState)

    const onSetState = (item: TThemeState) => {
        setState({
            ...state,
            ...item
        })
    }

    return {
        vm,
        onSetState,
        onClearState: resetState
    }
}
