import {SendingStatus} from "../../../const/Events";
import {atom} from "recoil";
import {KeyLogout} from "../../KeyRecoil";

type TLogoutState = {
    status: SendingStatus
    error?: Record<string, any>
}

export const initialState: TLogoutState = {
    status: SendingStatus.idle,
}

export const LogoutState = atom<TLogoutState>({
    key: KeyLogout,
    default: initialState
})
