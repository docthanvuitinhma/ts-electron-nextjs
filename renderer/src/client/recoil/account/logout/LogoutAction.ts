import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {LogoutState} from "./LogoutState";
import {SendingStatus} from "../../../const/Events";
import {ApiService} from "../../../repositories/ApiService";
import {setErrorHandled} from "../../CmAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";
import {MeAction} from "../me/MeAction";
import {useInjection} from "inversify-react";

export const LogoutAction = () => {
    const [state, setState] = useRecoilState(LogoutState)
    const resetState = useResetRecoilState(LogoutState)
    const vm = useRecoilValue(LogoutState)
    const apiService = useInjection(ApiService)

    const [session, setSession] = useSessionContext()

    const {
        onClearUser
    } = MeAction()

    const onLogout = () => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .logout()
            .then(r => {
                if (r.success) {
                    // remove store user
                    onClearUser()

                    setSession({
                        ...session,
                        isAuthenticated: false,
                        user: undefined,
                        redirectPath: '/login'
                    })

                    setState({
                        ...state,
                        status: SendingStatus.success,
                    })
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onLogout,
        onClearState: resetState
    }
}
