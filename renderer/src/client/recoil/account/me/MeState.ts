import {SendingStatus} from "../../../const/Events";
import {UserModel} from "../../../models/UserModel";
import {atom} from "recoil";
import {KeyMe} from "../../KeyRecoil";

type TMeState = {
    user?: UserModel
    isLoading: SendingStatus
    error?: Record<string, any>
}

export const initialState: TMeState = {
    isLoading: SendingStatus.idle
}

export const MeState = atom<TMeState>({
    key: KeyMe,
    default: initialState
})
