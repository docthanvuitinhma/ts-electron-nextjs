import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {SendingStatus} from "../../const/Events";
import {ApiService} from "../../repositories/ApiService";
import {setErrorHandled} from "../CmAction";
import {useInjection} from "inversify-react";
import {ScheduleState} from "./ScheduleState";
import {ScheduleModel} from "../../models/ScheduleModel";

export const ScheduleAction = () => {
    const [state, setState] = useRecoilState(ScheduleState)
    const vm = useRecoilValue(ScheduleState)
    const resetState = useResetRecoilState(ScheduleState)
    const apiService = useInjection(ApiService)

    const onGetListJob = (query?: any) => {

        setState({
            ...state,
            status: SendingStatus.loading
        })
        const totalQuery = {...query, sort: 'created_at', order: 'desc'}

        apiService
            .getAllJob(totalQuery)
            .then(res => {
                if (res.success) {
                    if (res.items) {
                        const listQueue = res.items.map(item => (
                            new ScheduleModel(item)
                        ))
                        setState({
                            ...state,
                            status: SendingStatus.success,
                            schedule: listQueue,
                            meta: res.meta
                        })
                    } else {

                        setState({
                            ...state,
                            status: SendingStatus.error,
                            error: res.error,
                            meta: res.meta
                        })
                    }
                } else {

                    setState({
                        ...state,
                        status: SendingStatus.success,
                        schedule: [],
                        meta: res.meta
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }
    return {
        vm,
        onClearMachineState: resetState,
        onGetListJob
    }
}