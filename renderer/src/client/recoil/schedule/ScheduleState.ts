import {SendingStatus} from "../../const/Events";
import {atom} from "recoil";
import { KeySchedule} from "../KeyRecoil";
import {PaginateMetaModel} from "../../models/ApiResModel";
import {ScheduleModel} from "../../models/ScheduleModel";


type TScheduleState = {
    status: SendingStatus
    schedule?: ScheduleModel[]
    error?: Record<string, any>
    meta?: PaginateMetaModel
}

export const initialState: TScheduleState = {
    status: SendingStatus.idle
}

export const ScheduleState = atom<TScheduleState>({
    key: KeySchedule,
    default: initialState
})