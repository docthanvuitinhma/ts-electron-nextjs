import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {AddUserState} from "./AddUserState";
import {SendingStatus} from "../../const/Events";
import {RoleModel} from "../../models/RoleModel";
import {setErrorHandled} from "../CmAction";
import {TUserV0} from "../../models/UserModel";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";



export const AddUserAction = () => {
    const [state, setState] = useRecoilState(AddUserState)
    const vm = useRecoilValue(AddUserState)
    const resetState = useResetRecoilState(AddUserState)
    const apiService = useInjection(ApiService)

    const onGetRoles = () => {
        setState({
            ...state,
            roleStatus: SendingStatus.loading
        })

        apiService
            .getAllRole()
            .then(r => {

                if (r.success) {

                    const roles = r.items?.map(item => (
                        new RoleModel(item)
                    ))

                    setState({
                        ...state,
                        roleStatus: SendingStatus.success,
                        res: roles
                    })

                } else {
                    setState({
                        ...state,
                        roleStatus: SendingStatus.error,
                        error: r.error
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'addStatus', err))
    }

    const onAddUser = (data: TUserV0) => {

        setState({
            ...state,
            addStatus: SendingStatus.loading
        })

        apiService
            .createUser(data)
            .then(r => {
                console.log(r)
                if (r.success) {
                    setState({
                        ...state,
                        addStatus: SendingStatus.success,
                    })

                } else {
                    setState({
                        ...state,
                        addStatus: SendingStatus.error,
                        error: r.error
                    })

                }
            })
            .catch(err => setErrorHandled(state, setState, 'addStatus', err))
    }

    return {
        vm,
        onGetRoles,
        onAddUser,
        onClearUserState: resetState,
    }
}