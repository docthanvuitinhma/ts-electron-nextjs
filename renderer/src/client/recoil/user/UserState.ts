import {atom} from "recoil";
import {PaginateMetaModel} from "../../models/ApiResModel";
import {UserModel} from "../../models/UserModel";
import {SendingStatus} from "../../const/Events";
import {KeyUser} from "../KeyRecoil";

type TUser = {
    meta?: PaginateMetaModel
    res?: UserModel[]
    status: SendingStatus
    deleteStatus: SendingStatus
    updateStatus: SendingStatus
    error?: Record<string, any>
}

export const initialState: TUser = {
    status: SendingStatus.idle,
    deleteStatus: SendingStatus.idle,
    updateStatus: SendingStatus.idle
}

export const UserState = atom<TUser>({
    key: KeyUser,
    default: initialState
})