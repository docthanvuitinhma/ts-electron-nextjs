import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {AddUserState} from "./AddUserState";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";
import {UpdateUserState} from "./UpdateUserState";
import {SendingStatus} from "../../const/Events";
import {TUserV0, UserModel} from "../../models/UserModel";
import {setErrorHandled} from "../CmAction";
import {RoleModel} from "../../models/RoleModel";

export const UpdateUserAction = () => {
    const [state, setState] = useRecoilState(UpdateUserState)
    const vm = useRecoilValue(UpdateUserState)
    const resetState = useResetRecoilState(UpdateUserState)
    const apiService = useInjection(ApiService)
    const onUpdateUser = (id: any, data: TUserV0) => {
        setState({
            ...state,
            updateStatus: SendingStatus.loading
        })
        apiService.updateUser(id, data)
            .then(res => {
                if (res.success) {
                    setState({
                        ...state,
                        updateStatus: SendingStatus.success
                    })

                } else {
                    setState({
                        ...state,
                        updateStatus: SendingStatus.error,
                        error: res.error
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
        console.log(vm)
    }

    const onGetRoles = () => {
        setState({
            ...state,
            roleStatus: SendingStatus.loading
        })

        apiService
            .getAllRole()
            .then(r => {

                if (r.success) {

                    const roles = r.items?.map(item => (
                        new RoleModel(item)
                    ))

                    setState({
                        ...state,
                        roleStatus: SendingStatus.success,
                        res: roles
                    })

                } else {
                    setState({
                        ...state,
                        roleStatus: SendingStatus.error,
                        error: r.error
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'addStatus', err))
    }

    return {
        onUpdateUser,
        onGetRoles,
        vm,
        onClearUpdateState: resetState
    }
}