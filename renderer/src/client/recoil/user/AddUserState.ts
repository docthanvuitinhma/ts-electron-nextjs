import {atom} from "recoil";
import {RoleModel} from "../../models/RoleModel";
import {SendingStatus} from "../../const/Events";
import {KeyAddUser} from "../KeyRecoil";

type TUser = {
    res?: RoleModel[]
    roleStatus: SendingStatus
    addStatus: SendingStatus
    error?: Record<string, any>
}

export const initialState: TUser = {
    roleStatus: SendingStatus.idle,
    addStatus: SendingStatus.idle
}

export const AddUserState = atom<TUser>({
    key: KeyAddUser,
    default: initialState
})