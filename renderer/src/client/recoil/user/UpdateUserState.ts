import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {KeyUpdateUser, KeyUserInfo} from "../KeyRecoil";
import {RoleModel} from "../../models/RoleModel";

type TUser = {
    res?: RoleModel[]
    status: SendingStatus
    roleStatus: SendingStatus
    updateStatus: SendingStatus
    error?: Record<string, any>
}
export const initInfoState: TInfoUser = {
    id: 0,
    name: "",
    email: "",
    createdAt: "",
    role: []
}
export const initialState: TUser = {
    status: SendingStatus.idle,
    roleStatus: SendingStatus.idle,
    updateStatus: SendingStatus.idle
}
export type TInfoUser = {
    id: number
    name?: string
    email?: string
    createdAt?: string
    role?: any[]

}


export const InfoState = atom<TInfoUser>({
    key: KeyUserInfo,
    default: initInfoState
})

export const UpdateUserState = atom<TUser>({
    key: KeyUpdateUser,
    default: initialState
})