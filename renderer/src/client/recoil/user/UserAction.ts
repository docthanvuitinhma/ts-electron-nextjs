import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {UserState} from "./UserState";
import {SendingStatus} from "../../const/Events";
import {UserModel} from "../../models/UserModel";
import {setErrorHandled} from "../CmAction";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";

export const UserAction = () => {
    const [state, setState] = useRecoilState(UserState)
    const vm = useRecoilValue(UserState)
    const resetState = useResetRecoilState(UserState)
    const apiService = useInjection(ApiService)
    const onGetAllUser = (query?: any) => {

        setState({
            ...state,
            status: SendingStatus.loading
        })

        const finalQuery = {sort: 'created_at', order: 'desc', ...query}

        apiService
            .getAllUser(finalQuery)
            .then(r => {

                if (r.success) {

                    if (r.items) {

                        const users = r.items.map(item => (
                            new UserModel(item)
                        ))

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: users,
                            meta: r.meta
                        })

                    } else {

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: []
                        })
                    }

                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }
    const onGetUser =  (id :any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })
        apiService.getUser(id)
            .then (res => {

                if (res.success) {

                    if (res.data) {
                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: res.data,
                            meta: res.meta
                        })

                    } else {

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: []
                        })
                    }

                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: res.error
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
        console.log("VM",vm)
    }


    // const onDeleteUser = (userId: string) => {
    //     setState({
    //         ...state,
    //         deleteStatus: SendingStatus.loading
    //     })
    //
    //     AxiosClient
    //         .delete(ApiService.deleteUser(userId))
    //         .then(r => {
    //
    //             if (r.success) {
    //                 setState({
    //                     ...state,
    //                     deleteStatus: SendingStatus.success
    //                 })
    //
    //             } else {
    //                 setState({
    //                     ...state,
    //                     deleteStatus: SendingStatus.error,
    //                     error: r.error
    //                 })
    //             }
    //
    //         })
    //         .catch(err => setErrorHandled(state, setState, 'deleteStatus', err))
    // }

    return {
        vm,
        onGetAllUser,
        onGetUser,
        onClearUserState: resetState,


    }
}