import {Utils} from "../core/Utils";
import {v4 as uuid} from "uuid";

const createKey = (key: string) => {
    if (Utils.isDev()) {
        return `${key}-${uuid()}`;
    }

    return key;
}

// Init Tracking
export const KeyIT = createKey("it")

// Config
export const KeyTheme = createKey("theme")

// Auth
export const KeyLogin = createKey("login")
export const KeyLogout = createKey("logout")

// Account
export const KeyMe = createKey("me")
// export const KeyMeEdit = createKey("me_edit")
// export const KeyMeImage = createKey("me_image")
// export const KeyMePassword = createKey("me_password")

// Machine
export const KeyMachine = createKey("machine")
export const KeyAddMachine = createKey("add_machine")

export const KeyImage = createKey("image")
export const KeyVideo = createKey("video")

// User
export const KeyUser = createKey("user")
export const KeyAddUser = createKey("add_user")
export const KeyUpdateUser = createKey("update-user")
export const KeyUserInfo= createKey("user-info")
// Permission
export const KeyPermission = createKey("permission")
export const KeyAddPermission = createKey("add_permission")

// Role
export const KeyRole = createKey("role")
export const KeyAddRole = createKey("add_role")

//Disk
export const KeyDiskUsage = createKey("disk_usage")

//Schedule
export const KeySchedule = createKey("schedule")

//Queue
export const KeyQueue = createKey("list_queue")