import {atom} from "recoil";
import {PaginateMetaModel} from "../../models/ApiResModel";
import {SendingStatus} from "../../const/Events";
import {ListQueueModel} from "../../models/ListQueueModel";
import {KeyQueue} from "../KeyRecoil";

type TListQueueState = {
    status: SendingStatus
    queue?: ListQueueModel[]
    error?: Record<string, any>
    meta?: PaginateMetaModel
}

export const initialState: TListQueueState = {
    status: SendingStatus.idle
}

export const ListQueueState = atom<TListQueueState>({
    key: KeyQueue,
    default: initialState
})