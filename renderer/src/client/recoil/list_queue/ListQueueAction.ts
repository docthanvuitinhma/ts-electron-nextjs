import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";
import {ListQueueState} from "./ListQueueState";
import {SendingStatus} from "../../const/Events";
import {ListQueueModel} from "../../models/ListQueueModel";
import {setErrorHandled} from "../CmAction";


export const ListQueueAction = () => {
    const vm1 = useRecoilValue(ListQueueState)
    const [state, setState] = useRecoilState(ListQueueState)
    const resetState = useResetRecoilState(ListQueueState)
    const apiService = useInjection(ApiService)

    const onGetListQueue = (query?: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })
        const totalQuery = {...query, sort: 'created_at', order: 'desc'}

        apiService
            .getQueue(totalQuery)
            .then(r => {
                if (r.success) {

                    if (r.items) {

                        const listQueue = r.items.map(item => (
                            new ListQueueModel(item)
                        ))

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            queue: listQueue,
                            meta: r.meta
                        })
                    } else {
                        setState({
                            ...state,
                            status: SendingStatus.success,
                            queue: [],
                            meta: r.meta
                        })

                    }
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))

    }

    return {
        vm1,
        onGetListQueue,
        onClearQueue: resetState
    }
}