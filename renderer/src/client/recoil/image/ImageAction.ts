import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {ImageState} from "./ImageState";
import {useInjection} from "inversify-react";
import {SendingStatus} from "../../const/Events";
import {ApiService} from "../../repositories/ApiService";
import {setErrorHandled} from "../CmAction";
import {ImageModel} from "../../models/ImageModel";

export const ImageAction = () => {
    const [state, setState] = useRecoilState(ImageState)
    const vm = useRecoilValue(ImageState)
    const resetState = useResetRecoilState(ImageState)
    const apiService = useInjection(ApiService)

    const onGetImages = (id: string, query?: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })
        console.log(query)

        const totalQuery = {...query, sort: 'date_shot', order: 'desc'}

        apiService
            .getImage(id, totalQuery)
            .then(r => {
                console.log(r)

                if (r.success) {

                    if (r.items) {

                        const images = r.items.map(item => {

                            return new ImageModel(item)
                        })

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: images,
                            meta: r.meta
                        })

                    } else {
                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: [],
                            meta: r.meta
                        })
                    }
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })

                }

            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onGetImages,
        onClearState: resetState
    }
}