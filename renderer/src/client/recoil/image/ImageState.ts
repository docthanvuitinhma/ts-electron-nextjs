import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {ImageModel} from "../../models/ImageModel";
import {PaginateMetaModel} from "../../models/ApiResModel";
import {KeyImage} from "../KeyRecoil";

type TImagesState = {
    status: SendingStatus
    error?: Record<string, any>
    res?: ImageModel[]
    meta?: PaginateMetaModel
}

export const initialState: TImagesState = {
    status: SendingStatus.idle
}
export const ImageState = atom<TImagesState>({
    key: KeyImage,
    default: initialState
})

