import {AxiosError, AxiosResponse} from "axios";
import {SetterOrUpdater} from "recoil";
import {ResCode, SendingStatus} from "../const/Events";

export const setErrorHandled = (
    state: any,
    setState: SetterOrUpdater<any>,
    key?: string | string[],
    err?: any
) => {
    console.error('ErrorHandled', err)

    let status: SendingStatus

    if (typeof (err as AxiosError).response === 'object') { // server error
        switch ((err.response as AxiosResponse).status) {
            case ResCode.HTTP_UNAUTHORIZED:
                status = SendingStatus.unauthorized

                break
            case ResCode.HTTP_SERVICE_UNAVAILABLE:
                status = SendingStatus.maintenance

                break
            case ResCode.HTTP_INTERNAL_SERVER_ERROR:
            default:
                status = SendingStatus.serverError

                break
        }
    } else { // internet disconnect
        if (navigator.onLine) {
            status = SendingStatus.serverError
        } else {
            status = SendingStatus.disConnect
        }

    }

    if (key) {
        if (typeof key === "string") {
            setState({
                ...state,
                [key]: status
            })
        } else if (key.length >= 2) {
            setState({
                ...state,
                [key[0]]: {
                    [key[1]]: status
                }
            })
        }
    }
}
