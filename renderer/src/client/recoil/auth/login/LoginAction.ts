import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {LoginState} from "./LoginState";
import {ApiService} from "../../../repositories/ApiService";
import {TLoginVO, UserModel} from "../../../models/UserModel";
import {setErrorHandled} from "../../CmAction";
import {MeAction} from "../../account/me/MeAction";
import {useSessionContext} from "../../../presentation/contexts/SessionContext";
import {useInjection} from "inversify-react";
import {SendingStatus} from "../../../const/Events";

export const LoginAction = () => {
    const [state, setState] = useRecoilState(LoginState)
    const resetState = useResetRecoilState(LoginState)
    const vm = useRecoilValue(LoginState)
    const [session, setSession] = useSessionContext()
    const apiService = useInjection(ApiService)

    const {
        onStoreUser
    } = MeAction()

    const onLogIn = (data: TLoginVO) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .login(data)
            .then(r => {
                if (r.success) {
                    const user = new UserModel(r.data)

                    onStoreUser(user)

                    setSession({
                        ...session,
                        isAuthenticated: true,
                        user: user
                    })

                    console.log(session)

                    setState({
                        ...state,
                        user: user,
                        status: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onLogIn,
        onClearState: resetState
    }
}
