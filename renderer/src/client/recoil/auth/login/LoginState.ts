import {atom} from "recoil";
import {UserModel} from "../../../models/UserModel";
import {SendingStatus} from "../../../const/Events";
import {KeyLogin} from "../../KeyRecoil";

type TLoginState = {
    user?: UserModel
    status: SendingStatus
    error?: Record<string, any>
}

export const initialState: TLoginState = {
    status: SendingStatus.idle,
}

export const LoginState = atom<TLoginState>({
    key: KeyLogin,
    default: initialState
})
