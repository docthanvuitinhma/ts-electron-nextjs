import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {AddPermissionState} from "./AddPermissionState";
import {SendingStatus} from "../../const/Events";
import {setErrorHandled} from "../CmAction";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";

export const AddPermissionAction = () => {
    const vm = useRecoilValue(AddPermissionState)
    const [state, setState] = useRecoilState(AddPermissionState)
    const resetState = useResetRecoilState(AddPermissionState)
    const apiService = useInjection(ApiService)

    const onAddPermission = (data: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .createPermission(data)
            .then(r => {
                if (r.success) {
                    setState({
                        ...state,
                        status: SendingStatus.success
                    })
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onAddPermission,
        onClearState: resetState
    }
}