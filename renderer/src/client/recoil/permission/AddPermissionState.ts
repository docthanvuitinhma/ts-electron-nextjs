import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {PermissionModel} from "../../models/PermissionModel";
import {KeyAddPermission} from "../KeyRecoil";

type TAddPermissionState = {
    status: SendingStatus
    error?: Record<string, any>
    res?: PermissionModel
}

export const initialState: TAddPermissionState = {
    status: SendingStatus.idle
}

export const AddPermissionState = atom({
    key: KeyAddPermission,
    default: initialState
})