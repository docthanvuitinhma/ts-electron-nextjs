import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {PermissionState} from "./PermissonState";
import {ApiService} from "../../repositories/ApiService";
import {PermissionModel} from "../../models/PermissionModel";
import {setErrorHandled} from "../CmAction";
import {SendingStatus} from "../../const/Events";
import {useInjection} from "inversify-react";

export const PermissionAction = () => {
    const vm = useRecoilValue(PermissionState)
    const [state, setState] = useRecoilState(PermissionState)
    const resetState = useResetRecoilState(PermissionState)
    const apiService = useInjection(ApiService)

    const onGetPermissions = (page?: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .getAllPermission(page ?? {})
            .then(r => {

                if (r.success) {

                    if (r.items) {

                        const permission = r.items.map(item => (
                            new PermissionModel(item)
                        ))

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: permission
                        })

                    } else {

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            res: []
                        })

                    }

                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    return {
        vm,
        onGetPermissions,
        onClearPermissionState: resetState
    }

}