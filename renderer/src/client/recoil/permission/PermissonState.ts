import {atom} from "recoil";
import {PermissionModel} from "../../models/PermissionModel";
import {SendingStatus} from "../../const/Events";
import {KeyPermission} from "../KeyRecoil";

type TPermission = {
    status: SendingStatus
    error?: Record<string, any>
    res?: PermissionModel[]
}

export const initialState: TPermission = {
    status: SendingStatus.idle
}

export const PermissionState = atom({
    key: KeyPermission,
    default: initialState
})