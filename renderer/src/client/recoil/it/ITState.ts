import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {KeyIT} from "../KeyRecoil";
import {InitModel, TrackingModel} from "../../models/ITModel";

export type TITState = {
    init?: InitModel
    tracking?: TrackingModel
    isLoading: SendingStatus
    error?: Record<string, any>
}

export const initialState: TITState = {
    isLoading: SendingStatus.idle,
}

export const ITState = atom<TITState>({
    key: KeyIT,
    default: initialState
})
