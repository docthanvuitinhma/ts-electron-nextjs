import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {VideoState} from "./VideoState";
import {SendingStatus} from "../../const/Events";
import {setErrorHandled} from "../CmAction";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";
import {TVideo} from "../../models/VideoModel";

export const VideoAction = () => {
    const vm = useRecoilValue(VideoState)
    const [state, setState] = useRecoilState(VideoState)
    const resetState = useResetRecoilState(VideoState)
    const apiService = useInjection(ApiService)
    const onGetVideos = (id: string) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .getVideo(id)
            .then(r => {

                if (r.success) {
                    setState({
                        ...state,
                        status: SendingStatus.success,
                        res: r.items
                    })
                    console.log("state :  ", state)
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    const onUpload = (id: string, data: TVideo) => {

        setState({
            ...state,
            status: SendingStatus.loading
        })

        // AxiosClient
        //     .post(ApiService.resMachine(), data)
        apiService
            .uploadVideo(id, data)
            .then(r => {
                console.log(r)

                if (r.success) {
                    setState({
                        ...state,
                        status: SendingStatus.success
                    })

                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'addStatus', err))

    }
    return {
        vm,
        onGetVideos,
        onClearVideoState: resetState,
        onUpload
    }
}