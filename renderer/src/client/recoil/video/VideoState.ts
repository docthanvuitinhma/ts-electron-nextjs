import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {VideoModel} from "../../models/VideoModel";
import {KeyVideo} from "../KeyRecoil";

type TVideosState = {
    status: SendingStatus
    error?: Record<string, any>
    res?: VideoModel[]
}

export const initialState: TVideosState = {
    status: SendingStatus.idle
}

export const VideoState = atom({
    key: KeyVideo,
    default: initialState
})