import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {SendingStatus} from "../../const/Events";
import {ApiService} from "../../repositories/ApiService";
import {setErrorHandled} from "../CmAction";
import {DiskUsageState} from "./DiskUsageState";
import {DiskUsageModel} from "../../models/DiskUsageModel";
import {useInjection} from "inversify-react";


export const DiskUsageAction = () => {
    const [state, setState] = useRecoilState(DiskUsageState)
    const vm = useRecoilValue(DiskUsageState)
    const resetState = useResetRecoilState(DiskUsageState)
    const apiService = useInjection(ApiService)

    const onGetListDisk = () => {

        setState({
            ...state,
            status: SendingStatus.loading
        })


        apiService
            .getDisk()
            .then(res => {
                console.log(res.items)
                if (res.items) {
                    const listDisk = res.items.map(item => (
                        new DiskUsageModel(item)
                    ))
                    setState({
                        ...state,
                        status: SendingStatus.success,
                        diskusage: listDisk
                    })
                } else {

                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: res.error
                    })
                }

            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }
    console.log(vm)
    return {
        vm,
        onClearMachineState: resetState,
        onGetListDisk
    }
}