import {SendingStatus} from "../../const/Events";
import {atom} from "recoil";
import {KeyDiskUsage} from "../KeyRecoil";
import {PaginateMetaModel} from "../../models/ApiResModel";
import {DiskUsageModel} from "../../models/DiskUsageModel";

type TDiskUsageState = {
    status: SendingStatus
    diskusage?: DiskUsageModel[]
    error?: Record<string, any>
    meta?: PaginateMetaModel
}

export const initialState: TDiskUsageState = {
    status: SendingStatus.idle
}

export const DiskUsageState = atom<TDiskUsageState>({
    key: KeyDiskUsage,
    default: initialState
})