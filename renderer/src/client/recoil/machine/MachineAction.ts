import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {MachineState} from "./MachineState";
import {SendingStatus} from "../../const/Events";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";
import {MachineModel} from "../../models/MachineModel";
import {setErrorHandled} from "../CmAction";

export const MachineAction = () => {
    const vm = useRecoilValue(MachineState)
    const [state, setState] = useRecoilState(MachineState)
    const resetState = useResetRecoilState(MachineState)
    const apiService = useInjection(ApiService)

    const onGetListMachine = (query?: any) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        const totalQuery = {...query, sort: 'created_at', order: 'desc'}

        apiService
            .getAllMachine(totalQuery)
            .then(r => {
                if (r.success) {
                    if (r.items) {

                        const machines = r.items.map(item => (
                            new MachineModel(item)
                        ))

                        setState({
                            ...state,
                            status: SendingStatus.success,
                            machines: machines,
                            meta: r.meta
                        })
                    } else {
                        setState({
                            ...state,
                            status: SendingStatus.success,
                            machines: [],
                            meta: r.meta
                        })

                    }
                } else {
                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))

    }

    return {
        vm,
        onGetListMachine,
        onClearMachineState: resetState
    }
}