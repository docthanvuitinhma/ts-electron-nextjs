import {useRecoilState, useRecoilValue, useResetRecoilState} from "recoil";
import {AddMachineState} from "./AddMachineState";
import {SendingStatus} from "../../const/Events";
import {setErrorHandled} from "../CmAction";
import {TMachine} from "../../presentation/screens/machine/AddMachine";
import {useInjection} from "inversify-react";
import {ApiService} from "../../repositories/ApiService";
import {MachineModel} from "../../models/MachineModel";

export const AddMachineAction = () => {
    const [state, setState] = useRecoilState(AddMachineState)
    const vm = useRecoilValue(AddMachineState)
    const resetState = useResetRecoilState(AddMachineState)
    const apiService = useInjection(ApiService)

    const onGetMachine = (machineId: string) => {
        setState({
            ...state,
            status: SendingStatus.loading
        })

        apiService
            .getMachine(machineId)
            .then(r => {

                console.log(r)

                if (r.success) {

                    const machine = new MachineModel(r.data)

                    setState({
                        ...state,
                        status: SendingStatus.success,
                        res: machine
                    })

                } else {

                    setState({
                        ...state,
                        status: SendingStatus.error,
                        error: r.error
                    })

                }
            })
            .catch(err => setErrorHandled(state, setState, 'status', err))
    }

    const onUpdateMachine = (machineId: string, data: TMachine) => {
        console.log('UPDATE MACHINE')

        setState({
            ...state,
            updateStatus: SendingStatus.loading
        })

        // AxiosClient
        //     .put(ApiService.resMachine(machineId), data)
        apiService
            .updateMachine(machineId, data)
            .then(r => {
                console.log(r)

                if (r.success) {
                    setState({
                        ...state,
                        updateStatus: SendingStatus.success
                    })

                } else {
                    setState({
                        ...state,
                        updateStatus: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'updateStatus', err))

    }

    const onAddMachine = (data: TMachine) => {
        console.log('CREATE MACHINE')

        setState({
            ...state,
            addStatus: SendingStatus.loading
        })

        // AxiosClient
        //     .post(ApiService.resMachine(), data)
        apiService
            .storeMachine(data)
            .then(r => {
                console.log(r)

                if (r.success) {
                    setState({
                        ...state,
                        addStatus: SendingStatus.success
                    })

                } else {
                    setState({
                        ...state,
                        addStatus: SendingStatus.error,
                        error: r.error
                    })
                }
            })
            .catch(err => setErrorHandled(state, setState, 'addStatus', err))

    }

    return {
        vm,
        onGetMachine,
        onUpdateMachine,
        onAddMachine,
        onClearState: resetState
    }
}