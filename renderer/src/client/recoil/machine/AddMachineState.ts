import {atom} from "recoil";
import {SendingStatus} from "../../const/Events";
import {MachineModel} from "../../models/MachineModel";
import {KeyAddMachine} from "../KeyRecoil";

type TMachineState = {
    status: SendingStatus
    addStatus: SendingStatus
    updateStatus: SendingStatus
    error?: Record<string, any>
    res?: MachineModel
}

export const initialState: TMachineState = {
    status: SendingStatus.idle,
    addStatus: SendingStatus.idle,
    updateStatus: SendingStatus.idle
}

export const AddMachineState = atom<TMachineState>({
    key: KeyAddMachine,
    default: initialState
})