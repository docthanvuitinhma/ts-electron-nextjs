import {atom} from "recoil";
import {MachineModel} from "../../models/MachineModel";
import {KeyMachine} from "../KeyRecoil";
import {PaginateMetaModel} from "../../models/ApiResModel";
import {SendingStatus} from "../../const/Events";

type TMachineState = {
    status: SendingStatus
    machines?: MachineModel[]
    error?: Record<string, any>
    meta?: PaginateMetaModel
}

export const initialState: TMachineState = {
    status: SendingStatus.idle
}

export const MachineState = atom<TMachineState>({
    key: KeyMachine,
    default: initialState
})