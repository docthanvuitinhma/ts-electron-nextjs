import axios, {AxiosRequestConfig, CancelToken} from "axios";
import {getLng} from "../locales/i18n";
import {App} from "../const/App";
import {ApiResModel} from "../models/ApiResModel";
import {EDFile} from "../core/encrypt/EDFile";
import moment from "moment";
import {Color} from "../const/Color";
import {EDData} from "../core/encrypt/EDData";
import {StoreConfig} from "../config/StoreConfig";
import {Utils} from "../core/Utils";

export class AxiosClient {

    static readonly Config = (isUp: boolean = false): AxiosRequestConfig => {
        const lng = getLng()

        const config: AxiosRequestConfig = {
            headers: {
                "Content-Type": isUp ? "multipart/form-data" : "application/json",
                "Lang-Code": lng ?? 'vi',
                "Platform": "web",
                'Access-Control-Allow-Origin': '*'
            },
            withCredentials: false,
        }

        if (!isUp) {
            config.headers!.Accept = "application/json";
        }

        const storeConfig = StoreConfig.getInstance()
        if (storeConfig.token && storeConfig.token.length > 0) {
            const et = EDFile.encrypt({
                t: storeConfig.token,
                e: moment().add(30, 'seconds').unix()
            })

            config.headers!.Authorization = `Bearer ${et}`;
        }

        return config;
    }

    public static get(path: string, query?: any, cancelToken?: CancelToken): Promise<ApiResModel> {
        const ep = EDFile.encrypt({
            p: path,
            q: AxiosClient.convertDataGet(query),
            e: moment().add(30, 'seconds').unix()
        })

        console.log('%c<-GET--------------------------------------------', Color.ConsoleInfo);
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`);
        console.log(`QUERY:`, query);

        return axios
            .get(`${App.ApiUrl}/${Utils.base64Encode(ep)}`, cancelToken
                ? {
                    ...AxiosClient.Config(),
                    ...{
                        cancelToken: cancelToken,
                    },
                }
                : AxiosClient.Config()
            )
            .then(r => {
                const data = r.data.hasOwnProperty('c') && r.data.hasOwnProperty('i') && r.data.hasOwnProperty('s')
                    ? EDData.decrypt(r.data)
                    : r.data

                console.log('RES:', data);
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo);

                return new ApiResModel(data);
            });
    }

    public static post(
        path: string,
        data?: any,
        isUp: boolean = false,
        config?: AxiosRequestConfig,
    ): Promise<ApiResModel> {
        const ep = EDFile.encrypt({
            p: path,
            e: moment().add(30, 'seconds').unix()
        });

        console.log('%c<-POST-------------------------------------------', Color.ConsoleInfo);
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`);
        console.log(`DATA:`, data);

        return axios
            .post(`${App.ApiUrl}/${Utils.base64Encode(ep)}`, data, config ?? AxiosClient.Config(isUp))
            .then(r => {
                console.log(r.data)
                const data = r.data.hasOwnProperty('c') && r.data.hasOwnProperty('i') && r.data.hasOwnProperty('s')
                    ? EDData.decrypt(r.data)
                    : r.data

                console.log('RES:', data);
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo);

                return new ApiResModel(data);
            });
    }

    public static put(
        path: string,
        data: any,
        config: AxiosRequestConfig = AxiosClient.Config()
    ): Promise<ApiResModel> {
        const ep = EDFile.encrypt({
            p: path,
            e: moment().add(30, 'seconds').unix()
        });

        console.log('%c<-GET--------------------------------------------', Color.ConsoleInfo);
        console.log(`[${moment().format(App.FormatTimeFull)}] PATH: ${path}`);
        console.log(`DATA:`, data);

        return axios
            .put(`${App.ApiUrl}/${Utils.base64Encode(ep)}}`, data, config)
            .then(r => {

                const data = r.data.hasOwnProperty('c') && r.data.hasOwnProperty('i') && r.data.hasOwnProperty('s')
                    ? EDData.decrypt(r.data)
                    : r.data

                console.log('RES:', data);
                console.log('%c--END------------------------------------------->', Color.ConsoleInfo);

                return new ApiResModel(data);
            });
    }

    public static convertDataGet(data: any, prefix: string = '') {
        const cv: any = {};

        if (typeof data === "object") {
            Object.entries(data as any).forEach(([key, value]) => {
                if (typeof value === "object") {
                    Object.assign(cv, AxiosClient.convertDataGet(value, prefix.length > 0 ? `${prefix}_${key}` : key));
                } else {
                    cv[prefix.length > 0 ? `${prefix}_${key}` : key] = value;
                }
            });
        }

        return cv;
    }
}
