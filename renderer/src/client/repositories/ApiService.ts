import {AxiosClient} from "./AxiosClient";
import {ApiResModel} from "../models/ApiResModel";
import {injectable} from "inversify";
import {TLoginVO, TUserV0} from "../models/UserModel";
import {TMachine} from "../presentation/screens/machine/AddMachine";
import {TVideo} from "../models/VideoModel";

@injectable()
export class ApiService {
    init(): Promise<ApiResModel> {
        return AxiosClient.get("init-web")
    }

    tracking(): Promise<ApiResModel> {
        return AxiosClient.get("tracking-web")
    }

    login(data: TLoginVO): Promise<ApiResModel> {
        return AxiosClient.post("auth/login", data);
    }

    logout(): Promise<ApiResModel> {
        return AxiosClient.post("auth/logout");
    }

    getMe(): Promise<ApiResModel> {
        return AxiosClient.get("account/me")
    }
    getDisk(): Promise<ApiResModel> {
        return AxiosClient.get("timelapse/disk-usage")
    }

    getMachine(id: string): Promise<ApiResModel> {
        return AxiosClient.get(`timelapse/machines/${id}`)
    }

    getAllMachine(query?: any): Promise<ApiResModel> {
        return AxiosClient.get("timelapse/machines", query)
    }

    getImage(id: string, query?: any): Promise<ApiResModel> {
        return AxiosClient.get(`timelapse/machine/${id}/images`, query ? query : {})
    }

    getVideo(id: string): Promise<ApiResModel> {
        return AxiosClient.get(`timelapse/machine/${id}/videos`)
    }

    getRole(roleId: string): Promise<ApiResModel> {
        return AxiosClient.get(`roles/${roleId}/detail`)
    }

    getAllRole(query?: any): Promise<ApiResModel> {
        return AxiosClient.get("roles/list", query)
    }

    getPermission(permissionId: string): Promise<ApiResModel> {
        return AxiosClient.get(`permissions/${permissionId}/detail`)
    }

    getAllPermission(query?: any): Promise<ApiResModel> {
        return AxiosClient.get("permissions/list", query)
    }

    getAllUser(query: any): Promise<ApiResModel> {
        return AxiosClient.get("users/list", query)
    }
    getUser(id: string):Promise<ApiResModel> {
        return AxiosClient.get(`users/${id}/detail`)
    }
    updateUser(id :string, data:TUserV0): Promise<ApiResModel> {
        return AxiosClient.put(`users/${id}`,data)
    }

    updateMachine(id: string, data: TMachine): Promise<ApiResModel> {
        return AxiosClient.put(`timelapse/machines/${id}`, data)
    }

    storeMachine(data: TMachine): Promise<ApiResModel> {
        return AxiosClient.post("timelapse/machines", data)
    }

    createUser(data: TUserV0): Promise<ApiResModel> {
        return AxiosClient.post("users/create", data)
    }
    uploadVideo(id:string, data: TVideo): Promise<ApiResModel> {
        return AxiosClient.post(`timelapse/machine/${id}/videos`, data)
    }
    createRole(data: any): Promise<ApiResModel> {
        return AxiosClient.post("roles/create", data)
    }

    createPermission(data: any): Promise<ApiResModel> {
        return AxiosClient.post("permissions/create", data)
    }
    getAllJob(query?: any): Promise<ApiResModel> {
        return AxiosClient.get("log/jobs", query)
    }
    getQueue(data:any): Promise<ApiResModel> {
        return AxiosClient.get("log/queues",data)
    }



}
