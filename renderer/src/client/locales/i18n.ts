import i18n from 'i18next';
import tranVI from './vi/translation.json';
import tranEN from './en/translation.json';
import tranZH from './zh/translation.json';
import {EDLocal} from "../core/encrypt/EDLocal";

export const resources = {
    vi: {
        translation: tranVI,
    },
    en: {
        translation: tranEN,
    },
    zh: {
        translation: tranZH,
    },
};

export function initLng(df = 'vi'): string {
    let lng = EDLocal.getLocalStore('lang');

    if (!lng) {
        lng = df;

        EDLocal.setLocalStore('lang', df)
    }

    return lng;
}

export function getLng(): string {
    return i18n.language;
}

type _TLangCode = 'vi' | 'en' | 'zh';

export function setLng(lang: _TLangCode): void {
    EDLocal.setLocalStore('lang', lang)

    i18n.changeLanguage(lang)
        .then(() => `Change language: ${lang}`);
}
