/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./renderer/src/client/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {},
    },
    plugins: [],
}
