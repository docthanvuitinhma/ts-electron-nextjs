import {app} from 'electron';
import serve from 'electron-serve';
import {createWindow} from './helpers';

const isProd: boolean = process.env.NODE_ENV === 'production';

if (isProd) {
    serve({directory: 'app'});
} else {
    app.setPath('userData', `${app.getPath('userData')} (development)`);
}

(async () => {
    await app.whenReady();

    const mainWindow = createWindow('main', {
        width: 1000,
        height: 600,
    });
    const sampleWindow = createWindow("sample", {
        width: 700,
        height: 1000,
        show: false,
    });

    if (isProd) {
        await mainWindow.loadURL('app://./home.html');
        await sampleWindow.loadURL("app://./sample.html");
    } else {
        const port = process.argv[2];
        await mainWindow.loadURL(`http://localhost:${port}/home`);
        await sampleWindow.loadURL(`http://localhost:${port}/sample`);
        mainWindow.webContents.openDevTools();
    }
})();

app.on('window-all-closed', () => {
    app.quit();
});

